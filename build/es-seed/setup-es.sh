#!/bin/bash
# setup-es.sh

curl -X PUT "elasticsearch:9200/metalheads"
elasticdump --input data/es-mappings.json --output=http://elasticsearch:9200 --type=mappings
elasticdump --input data/es-data.json --output=http://elasticsearch:9200 --type=data
