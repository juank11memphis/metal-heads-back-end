import { albums, bands, artists, songs } from './albums.etl.fixtures'

export const expectedAlbumTracks = [
  {
    _id: songs.speedKing._id,
    name: 'Speed King',
    length: '07:22',
  },
  {
    name: 'Child in Time',
    length: '11:07',
  },
  {
    name: 'Wring That Neck',
    length: '18:59',
  },
  {
    name: 'Mandrake Root',
    length: '17:38',
  },
  {
    name: 'Highway Star',
    length: '08:32',
  },
  {
    _id: songs.strangeWoman._id,
    name: 'Strange Kind of Woman',
    length: '09:17',
  },
  {
    name: 'Never Before',
    length: '04:34',
  },
  { name: 'Lazy', length: '10:22' },
  {
    name: "Space Truckin'",
    length: '21:46',
  },
  {
    name: 'Lucille (Little Richard cover)',
    length: '07:21',
  },
]

export const expectedAlbumLineUp = [
  {
    id: artists.ritchie._id,
    metalArchivesId: '8201',
    roles: 'Guitars',
    name: 'Ritchie Blackmore',
  },
  {
    metalArchivesId: '8929',
    roles: 'Drums',
    name: 'Ian Paice',
  },
  {
    metalArchivesId: '8957',
    roles: 'Organ',
    name: 'Jon Lord',
  },
  {
    metalArchivesId: '8247',
    roles: 'Vocals',
    name: 'Ian Gillan',
  },
  {
    metalArchivesId: '8926',
    roles: 'Bass, \t\t\t\t\t\t\t\t\t\t\tVocals',
    name: 'Roger Glover',
  },
]

export const expectedAlbum = {
  dataExtracted: true,
  _id: albums.deepPurpleConcert._id,
  name: 'Deep Purple In Concert',
  metalArchivesId: '3389',
  picture: `http://metalheads.imgix.net/albums/${
    albums.deepPurpleConcert._id
  }_picture.jpg`,
  reviews: 'None yet',
  format: '2 12" vinyls (33⅓ RPM)',
  label: 'Harvest',
  catalogId: 'SHDW 412',
  releaseDate: '1980',
}
