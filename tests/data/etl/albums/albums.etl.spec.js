import { expect } from 'chai'
import sinon from 'sinon'
import request from 'request'

import { testsSetup } from '../../../tests.util'
import albumETL from '../../../../app/data/etl/albums/album.etl'
import Album from '../../../../app/components/albums/album'
import Band from '../../../../app/components/bands/band'
import { albums } from './albums.etl.fixtures'
import {
  expectedAlbum,
  expectedAlbumTracks,
  expectedAlbumLineUp,
} from './albums.etl.snapshots'
import { MetalArchivesAlbumWebScraper } from '../../../../app/data/webscraping/metal-archives-album.webscraper'
import { expectedMultiSidesAlbum } from '../../webscraping/snapshots/metal-archives-album.webscraper.snapshots'
import elasticsearchApi, {
  ElasticSearchApi,
} from '../../../../app/data/api/elasticsearch.api'
import albumsDao from '../../../../app/components/albums/albums.dao'

const sandbox = sinon.createSandbox()

describe('AlbumETL', () => {
  beforeEach(done => {
    sandbox
      .stub(MetalArchivesAlbumWebScraper.prototype, 'scrapAlbumFullData')
      .returns(expectedMultiSidesAlbum)
    sandbox.stub(request, 'get').yields(
      null,
      {
        statusCode: 200,
      },
      'an image buffer',
    )
    testsSetup(__dirname + '/albums.etl.fixtures.js', done)
  })

  afterEach(() => {
    sandbox.restore()
  })

  it('should run album etl successfully', async () => {
    const dbAlbum = await albumsDao.getDetail(albums.deepPurpleConcert._id)
    const dbBand = await Band.findById(dbAlbum.band, {
      name: 1,
    })
    await albumETL.start(dbAlbum._id)
    const album = await albumsDao.getFullDetail(dbAlbum._id)
    expect(album.tracks).to.containSubset(expectedAlbumTracks)
    expect(album.lineUp).to.containSubset(expectedAlbumLineUp)
    album.tracks = undefined
    album.lineUp = undefined
    album.band = undefined
    expect(album.toObject()).to.containSubset(expectedAlbum)

    const esBands = await elasticsearchApi.searchByEntityType('band')
    expect(esBands.length).to.equal(1)
    const esAlbums = await elasticsearchApi.searchByEntityType('album', 999)
    expect(esAlbums.length).to.equal(1)
    const esArtists = await elasticsearchApi.searchByEntityType('artist')
    expect(esArtists.length).to.equal(5)
    const esSongs = await elasticsearchApi.searchByEntityType('song')
    expect(esSongs.length).to.equal(10)
  })
})
