const id = require('pow-mongodb-fixtures').createObjectId

export const songs = {
  speedKing: {
    _id: id(),
    name: 'Speed King',
    length: '7:11',
  },
  strangeWoman: {
    _id: id(),
    name: 'Strange Kind of Woman',
    length: '8:17',
  },
  weirdSong: {
    _id: id(),
    name: 'The Weirdest Song',
    length: '11:11',
  },
}

export const artists = {
  ritchie: {
    _id: id(),
    name: 'Ritchie Blackmore',
    metalArchivesId: '8201',
  },
}

export const bands = {
  deepPurple: {
    _id: id(),
    name: 'Deep Purple',
  },
}

export const albums = {
  deepPurpleConcert: {
    _id: id(),
    name: 'Deep Purple In Concert',
    metalArchivesId: '3389',
    band: bands.deepPurple._id,
    tracks: [songs.speedKing._id, songs.strangeWoman._id, songs.weirdSong._id],
  },
}
