import { expect } from 'chai'
import sinon from 'sinon'

import getNewAdditionsETL from '../../../../app/data/etl/new-additions/get-new-additions.etl'
import { MetalArchivesApi } from '../../../../app/data/api/metal-archives.api'
import {
  newAdditionsEtlStubData,
  expectedNewAdditionsEtlData,
} from './get-new-additions.etl.snapshots'
import { connectMongoose } from '../../../tests.util'
import Band from '../../../../app/components/bands/band'
import elasticsearchApi from '../../../../app/data/api/elasticsearch.api'

const sandbox = sinon.createSandbox()

describe('GetNewAdditionsETL', () => {
  before(() => {
    sandbox
      .stub(MetalArchivesApi.prototype, 'getNewAdditionsData')
      .returns(newAdditionsEtlStubData)
    return connectMongoose()
  })

  after(() => {
    sandbox.restore()
  })

  it('should run get new additions etl successfully', async () => {
    await getNewAdditionsETL.start()
    const newBands = await Band.find({})
    expect(newBands).to.containSubset(expectedNewAdditionsEtlData)

    const esBands = await elasticsearchApi.searchByEntityType('band')
    expect(esBands.length).to.equal(10)
  })
})
