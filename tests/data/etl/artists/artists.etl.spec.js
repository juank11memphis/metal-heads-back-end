import { expect } from 'chai'
import sinon from 'sinon'
import request from 'request'

import { testsSetup } from '../../../tests.util'
import artistETL from '../../../../app/data/etl/artists/artist.etl'
import Artist from '../../../../app/components/artists/artist'
import { artists } from './artists.etl.fixtures'
import { expectedArtistData } from './artists.etl.snapshots'
import { MetalArchivesWebScraper } from '../../../../app/data/webscraping/metal-archives.webscraper'
import { getExpectedArtistData } from '../../webscraping/snapshots/metal-archives.webscraper.snapshots'
import elasticsearchApi from '../../../../app/data/api/elasticsearch.api'
import artistsDao from '../../../../app/components/artists/artists.dao'

const sandbox = sinon.createSandbox()

describe('ArtistETL', () => {
  before(done => {
    sandbox
      .stub(MetalArchivesWebScraper.prototype, 'scrapArtistFullData')
      .returns(getExpectedArtistData())
    sandbox.stub(request, 'get').yields(
      null,
      {
        statusCode: 200,
      },
      'an image buffer',
    )
    testsSetup(__dirname + '/artists.etl.fixtures.js', done)
  })

  after(() => {
    sandbox.restore()
  })

  it('should run artist etl successfully', async () => {
    const dbArtist = await artistsDao.getDetail(artists.harris._id)
    const artist = await artistETL.start(dbArtist)
    expect(artist).to.containSubset(expectedArtistData)

    const esBands = await elasticsearchApi.searchByEntityType('band')
    expect(esBands.length).to.equal(1)
    const esAlbums = await elasticsearchApi.searchByEntityType('album', 999)
    expect(esAlbums.length).to.equal(0)
    const esArtists = await elasticsearchApi.searchByEntityType('artist')
    expect(esArtists.length).to.equal(1)
  })
})
