const id = require('pow-mongodb-fixtures').createObjectId

export const artists = {
  harris: {
    _id: id(),
    name: 'Steve Harris',
    metalArchivesId: '105',
  },
}

export const bands = {
  steveHarris: {
    _id: id(),
    name: 'Steve Harris',
    metalArchivesId: '3540352457',
  },
}
