import { expect } from 'chai'
import sinon from 'sinon'
import request from 'request'

import { testsSetup } from '../../../tests.util'
import artistSyncETL from '../../../../app/data/etl/artists/artist-sync.etl'
import Artist from '../../../../app/components/artists/artist'
import { artists } from './artists-sync.etl.fixtures'
import { artistForStub, expectedArtistData } from './artists-sync.etl.snapshots'
import { MetalArchivesWebScraper } from '../../../../app/data/webscraping/metal-archives.webscraper'
import artistsDao from '../../../../app/components/artists/artists.dao'

const sandbox = sinon.createSandbox()

describe('ArtistSyncETL', () => {
  before(done => {
    sandbox
      .stub(MetalArchivesWebScraper.prototype, 'scrapArtistFullData')
      .returns(artistForStub)
    sandbox.stub(request, 'get').yields(
      null,
      {
        statusCode: 200,
      },
      'an image buffer',
    )
    testsSetup(__dirname + '/artists-sync.etl.fixtures.js', done)
  })

  after(() => {
    sandbox.restore()
  })

  it('should run artist sync etl successfully', async () => {
    const dbArtist = await artistsDao.getDetail(artists.harris._id)
    const artist = await artistSyncETL.start(dbArtist)
    expect(artist).to.containSubset(expectedArtistData)
  })
})
