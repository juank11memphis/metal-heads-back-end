const id = require('pow-mongodb-fixtures').createObjectId
const harrisId = id()

export const bands = {
  steveHarris: {
    _id: id(),
    name: 'Steve Harris',
    metalArchivesId: '3540352457',
  },
  maiden: {
    _id: id(),
    name: 'Iron Maiden',
    metalArchivesId: '25',
  },
}

export const artists = {
  harris: {
    _id: harrisId,
    name: 'Steve Harris',
    metalArchivesId: '105',
    picture: `http://metalheads.imgix.net/artists/${harrisId}_picture.jpg`,
    gender: 'Male',
    placeOfOrigin: 'United Kingdom (Leytonstone, London)',
    age: '61 (born Mar 12th, 1956)',
    realName: 'Stephen Percy Harris',
    dataExtracted: true,
    pastBands: [
      {
        role: 'Bass\t\t\t\t\t\t\n\t\t\t\t\t\t(1972-1973)',
        name: "Gypsy's Kiss",
      },
      {
        role: 'Bass\t\t\t\t\t\t\n\t\t\t\t\t\t(1974-1975)',
        name: 'Smiler',
      },
    ],
    activeBands: [
      {
        role: 'Bass, Keyboards\t\t\t\t\t\t\n\t\t\t\t\t\t(1975-present)',
        metalArchivesId: '25',
        name: 'Iron Maiden',
      },
      {
        id: bands.steveHarris._id,
        role: 'Bass',
        metalArchivesId: '3540352457',
        name: 'Steve Harris',
      },
    ],
  },
}
