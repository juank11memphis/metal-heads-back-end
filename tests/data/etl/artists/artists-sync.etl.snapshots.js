import { artists, bands } from './artists-sync.etl.fixtures'

export const artistForStub = {
  realName: 'Stephen Percy Harris',
  age: '61 (born Mar 12th, 1956)',
  placeOfOrigin: 'United Kingdom (Leytonstone, London)',
  gender: 'Male',
  picture: './artist-detail_files/105_artist.jpg',
  activeBands: [
    {
      name: 'Steve Harris',
      metalArchivesId: '3540352457',
      role: 'Bass',
    },
  ],
  pastBands: [
    {
      name: 'Iron Maiden',
      metalArchivesId: '25',
      role: 'Bass, Keyboards\t\t\t\t\t\t\n\t\t\t\t\t\t(1975-present)',
    },
    {
      name: "Gypsy's Kiss",
      role: 'Bass\t\t\t\t\t\t\n\t\t\t\t\t\t(1972-1973)',
    },
    {
      name: 'Smiler',
      role: 'Bass\t\t\t\t\t\t\n\t\t\t\t\t\t(1974-1975)',
    },
  ],
}

export const expectedArtistData = {
  _id: artists.harris._id,
  name: 'Steve Harris',
  metalArchivesId: '105',
  picture: `http://metalheads.imgix.net/artists/${
    artists.harris._id
  }_picture.jpg`,
  gender: 'Male',
  placeOfOrigin: 'United Kingdom (Leytonstone, London)',
  age: '61 (born Mar 12th, 1956)',
  realName: 'Stephen Percy Harris',
  dataExtracted: true,
  pastBands: [
    {
      name: "Gypsy's Kiss",
      role: 'Bass\t\t\t\t\t\t\n\t\t\t\t\t\t(1972-1973)',
    },
    {
      name: 'Smiler',
      role: 'Bass\t\t\t\t\t\t\n\t\t\t\t\t\t(1974-1975)',
    },
    {
      id: bands.maiden._id,
      name: 'Iron Maiden',
      metalArchivesId: '25',
      role: 'Bass, Keyboards\t\t\t\t\t\t\n\t\t\t\t\t\t(1975-present)',
    },
  ],
  activeBands: [
    {
      name: 'Steve Harris',
      metalArchivesId: '3540352457',
      role: 'Bass',
      id: bands.steveHarris._id,
    },
  ],
}
