import { bands } from '../fixtures/band.etl.fixtures'

export const expectedBandEtlData = () => {
  return {
    _id: bands.maiden._id,
    name: 'Iron Maiden',
    metalArchivesId: '25',
    picture: './iron-maiden_files/25_photo.jpg',
    genre: 'Heavy Metal, NWOBHM',
    location: 'London, England',
    status: 'Active',
    lyricalThemes: 'History, Literature, War, Mythology, Society, Religion',
    yearsActive: '1975-present',
    formedIn: '1975',
    similarBandsExtracted: false,
    dataExtracted: true,
  }
}
