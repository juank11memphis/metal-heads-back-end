import { bands, artists, albums } from '../fixtures/band-sync.etl.fixtures'

export const expectedAllenLandeWebscraperData = {
  location: 'N/A',
  status: 'Broken',
  formedIn: '2005',
  genre: 'Melodic Heavy Metal/Hard Rock/Pop(later)',
  lyricalThemes: 'Fantasy',
  yearsActive: '2005-2017',
  picture: './allenLande_files/47043_photo.jpg',
  currentMembers: [
    {
      name: 'Jørn Lande',
      metalArchivesId: '456',
      roles: 'Vocals (2005-2017)',
    },
    {
      name: 'Russell Allen',
      metalArchivesId: '5838',
      roles: 'Vocals (2005-2017)',
    },
    {
      name: 'Juan Morales',
      metalArchivesId: '112233',
      roles: 'Drums (2016-2017)',
    },
    {
      name: 'Timo Tolkki',
      metalArchivesId: '3717',
      roles: 'Guitars, Bass, Keyboards (2013-2017)',
    },
  ],
  albums: [
    {
      name: 'The Battle',
      metalArchivesId: '92383',
      type: 'Full-length',
      year: '2005',
    },
    {
      name: 'The Revenge',
      metalArchivesId: '149481',
      type: 'Full-length',
      year: '2007',
    },
    {
      name: 'The Showdown',
      metalArchivesId: '285406',
      type: 'Full-length',
      year: '2010',
    },
    {
      name: 'The Great Divide',
      metalArchivesId: '436176',
      type: 'Full-length',
      year: '2014',
    },
    {
      name: 'The Great Whatever',
      metalArchivesId: '11221133',
      type: 'Full-length',
      year: '2017',
    },
  ],
}

export const expectedAllenLandeSyncEtlData = {
  _id: bands.allenLande._id,
  name: 'Allen - Lande',
  metalArchivesId: '47043',
  picture: './allenLande_files/47043_photo.jpg',
  genre: 'Melodic Heavy Metal/Hard Rock/Pop(later)',
  location: 'N/A',
  status: 'Broken',
  lyricalThemes: 'Fantasy',
  yearsActive: '2005-2017',
  formedIn: '2005',
  currentMembers: [artists.allen._id, artists.lande._id, artists.tolkki._id],
  albums: [
    albums.battle._id,
    albums.divide._id,
    albums.showdown._id,
    albums.revenge._id,
  ],
  similarBandsExtracted: false,
  dataExtracted: true,
}
