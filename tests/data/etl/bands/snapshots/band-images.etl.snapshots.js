export const expectedLogErrorData = {
  message: 'new-error-message',
  errorData: {
    name: 'Bruce Dickinson',
    type: 'artist',
    imageUrl: 'awesome-picture',
  },
}

export const expectedLogErrorData2 = {
  message: 'new-error-message',
  errorData: {
    name: 'Killers',
    type: 'album',
    imageUrl: 'awesome-picture',
  },
}

export const expectedLogErrorData3 = {
  message: 'new-error-message',
  errorData: {
    name: 'Iron Maiden',
    type: 'band',
    imageUrl: 'awesome-picture',
  },
}
