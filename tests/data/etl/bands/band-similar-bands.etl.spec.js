import { expect } from 'chai'
import sinon from 'sinon'

import { testsSetup } from '../../../tests.util'
import bandSimilarBandsETL from '../../../../app/data/etl/bands/band-similar-bands.etl'
import Band from '../../../../app/components/bands/band'
import { bands } from './fixtures/band-similar-bands.etl.fixtures'
import { MetalArchivesWebScraper } from '../../../../app/data/webscraping/metal-archives.webscraper'
import { expectedWebScraperSimilarBandsIds } from '../../webscraping/snapshots/metal-archives.webscraper.snapshots'
import { expectedSimilarBandsEtlData } from './snapshots/band-similar-bands.etl.snapshots'
import bandsDao from '../../../../app/components/bands/bands.dao'

const sandbox = sinon.createSandbox()

describe('BandSimilarBandsETL', () => {
  before(done => {
    sandbox
      .stub(MetalArchivesWebScraper.prototype, 'scrapBandSimilarBandsIds')
      .returns(expectedWebScraperSimilarBandsIds)
    testsSetup(__dirname + '/fixtures/band-similar-bands.etl.fixtures.js', done)
  })

  after(() => {
    sandbox.restore()
  })

  it('should run band similar bands etl successfully', async () => {
    const dbBand = await bandsDao.getDetail(bands.maiden._id)
    const similarBands = await bandSimilarBandsETL.start(dbBand)
    expect(similarBands).to.containSubset(expectedSimilarBandsEtlData)
  })
})
