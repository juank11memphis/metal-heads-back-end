import { expect } from 'chai'
import sinon from 'sinon'
import request from 'request'

import { testsSetup } from '../../../tests.util'
import bandETL from '../../../../app/data/etl/bands/band.etl'
import Band from '../../../../app/components/bands/band'
import Artist from '../../../../app/components/artists/artist'
import { bands } from './fixtures/band-sync.etl.fixtures'
import {
  expectedAllenLandeWebscraperData,
  expectedAllenLandeSyncEtlData,
} from './snapshots/band-sync.etl.snapshots'
import { MetalArchivesBandWebScraper } from '../../../../app/data/webscraping/metal-archives-band.webscraper'
import elasticSearchApi from '../../../../app/data/api/elasticsearch.api'
import bandsDao from '../../../../app/components/bands/bands.dao'
import artistsDao from '../../../../app/components/artists/artists.dao'

const sandbox = sinon.createSandbox()

describe('BandSyncETL', () => {
  beforeEach(done => {
    sandbox
      .stub(MetalArchivesBandWebScraper.prototype, 'scrapBandFullData')
      .returns(expectedAllenLandeWebscraperData)
    sandbox.stub(request, 'get').yields(
      null,
      {
        statusCode: 200,
      },
      'an image buffer',
    )
    testsSetup(__dirname + '/fixtures/band-sync.etl.fixtures.js', done)
  })

  afterEach(() => {
    sandbox.restore()
  })

  it('should run band sync etl successfully', async () => {
    const dbBand = await bandsDao.getFullDetail(bands.allenLande._id)
    const band = await bandETL.start(dbBand)
    expect(band).to.containSubset(expectedAllenLandeSyncEtlData)
    expect(band.currentMembers.length).to.equal(4)
    expect(band.albums.length).to.equal(5)
    // Now we make sure existing members were updated
    const landeId = band.currentMembers[0]
    const lande = await artistsDao.getDetail(landeId)
    expect(lande.roles[dbBand._id]).to.equal('Vocals (2005-2017)')

    const esBands = await elasticSearchApi.searchByEntityType('band')
    expect(esBands.length).to.equal(1)
    const esAlbums = await elasticSearchApi.searchByEntityType('album', 999)
    expect(esAlbums.length).to.equal(5)
    const allArtists = await Artist.find({})
    const esArtists = await elasticSearchApi.searchByEntityType('artist')
    expect(esArtists.length).to.equal(5)
  })
})
