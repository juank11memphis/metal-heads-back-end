import { expect } from 'chai'
import sinon from 'sinon'
import request from 'request'

import { testsSetup } from '../../../tests.util'
import bandETL from '../../../../app/data/etl/bands/band.etl'
import Band from '../../../../app/components/bands/band'
import Artist from '../../../../app/components/artists/artist'
import Album from '../../../../app/components/albums/album'
import { bands } from './fixtures/band.etl.fixtures'
import { expectedBandEtlData } from './snapshots/band.etl.snapshots'
import { MetalArchivesBandWebScraper } from '../../../../app/data/webscraping/metal-archives-band.webscraper'
import { expectedBandWebScraperData } from '../../webscraping/snapshots/metal-archives-band.webscraper.snapshots'
import elasticSearchApi from '../../../../app/data/api/elasticsearch.api'
import bandsDao from '../../../../app/components/bands/bands.dao'

const sandbox = sinon.createSandbox()

describe('BandETL', () => {
  before(done => {
    sandbox
      .stub(MetalArchivesBandWebScraper.prototype, 'scrapBandFullData')
      .returns(expectedBandWebScraperData())
    sandbox.stub(request, 'get').yields(
      null,
      {
        statusCode: 200,
      },
      'an image buffer',
    )
    testsSetup(__dirname + '/fixtures/band.etl.fixtures.js', done)
  })

  after(() => {
    sandbox.restore()
  })

  it('should run band etl successfully', async () => {
    const dbBand = await bandsDao.getDetail(bands.maiden._id)
    const band = await bandETL.start(dbBand)
    expect(band).to.containSubset(expectedBandEtlData())
    expect(band.currentMembers.length).to.equal(6)
    expect(band.albums.length).to.equal(118)

    // members asserts
    const firstMember = await Artist.findById(band.currentMembers[0])
    expect(firstMember.roles[dbBand._id]).to.equal(
      'Bass, Keyboards (1975-present)',
    )

    const esBands = await elasticSearchApi.searchByEntityType('band')
    expect(esBands.length).to.equal(1)
    const esAlbums = await elasticSearchApi.searchByEntityType('album', 999)
    expect(esAlbums.length).to.equal(118)
    const esArtists = await elasticSearchApi.searchByEntityType('artist')
    expect(esArtists.length).to.equal(6)
  })
})
