import { expect } from 'chai'
import request from 'request'
import sinon from 'sinon'
const id = require('pow-mongodb-fixtures').createObjectId

import { testsSetup } from '../../../tests.util'
import bandImagesETL from '../../../../app/data/etl/bands/band-images.etl'
import Band from '../../../../app/components/bands/band'
import Artist from '../../../../app/components/artists/artist'
import Album from '../../../../app/components/albums/album'
import {
  expectedLogErrorData,
  expectedLogErrorData2,
  expectedLogErrorData3,
} from './snapshots/band-images.etl.snapshots'
import { AWSS3Api } from '../../../../app/data/api/aws-s3.api'
import bandsDao from '../../../../app/components/bands/bands.dao'

describe('BandImagesETL', () => {
  beforeEach(done => {
    sinon.stub(request, 'get').yields(null, { statusCode: 200 }, true)
    testsSetup(__dirname + '/fixtures/band-images.etl.fixtures.js', done)
  })

  afterEach(() => {
    request.get.restore()
  })

  it('should log erros', () => {
    let item = new Artist({
      name: 'Bruce Dickinson',
      picture: 'awesome-picture',
    })
    let error = new Error('test-error')
    let result = bandImagesETL.logError(
      'new-error-message',
      error,
      item,
      'artist',
    )
    expect(result).to.containSubset(expectedLogErrorData)
    expect(result.errorData.error).to.equal(error)

    item = new Album({ name: 'Killers', picture: 'awesome-picture' })
    error = new Error('test-error')
    result = bandImagesETL.logError('new-error-message', error, item, 'album')
    expect(result).to.containSubset(expectedLogErrorData2)
    expect(result.errorData.error).to.equal(error)

    item = new Band({ name: 'Iron Maiden', picture: 'awesome-picture' })
    error = new Error('test-error')
    result = bandImagesETL.logError('new-error-message', error, item, 'band')
    expect(result).to.containSubset(expectedLogErrorData3)
    expect(result.errorData.error).to.equal(error)
  })

  it('should handle upload image errors on extractArtistsImages', async () => {
    const stub = sinon
      .stub(AWSS3Api.prototype, 'downloadAndUploadImage')
      .throws(new Error('Test Error'))
    let dbMaiden = await Band.findOne({ name: 'Iron Maiden' })
    dbMaiden = await bandsDao.getFullDetail(dbMaiden._id)
    const result = await bandImagesETL.extractArtistsImages(dbMaiden)
    expect(result._id).to.equal(dbMaiden._id)
    stub.restore()
  })

  it('should handle upload image errors on extractAlbumsImages', async () => {
    const stub = sinon
      .stub(AWSS3Api.prototype, 'downloadAndUploadImage')
      .throws(new Error('Test Error'))
    let dbMaiden = await Band.findOne({ name: 'Iron Maiden' })
    dbMaiden = await bandsDao.getFullDetail(dbMaiden._id)
    const result = await bandImagesETL.extractAlbumsImages(dbMaiden)
    expect(result._id).to.equal(dbMaiden._id)
    stub.restore()
  })

  it('should know if an image is not available for processing', () => {
    let result = bandImagesETL.isImageNotAvailable({
      dataExtracted: true,
      picture: null,
    })
    expect(result).to.equal(true)

    result = bandImagesETL.isImageNotAvailable({
      dataExtracted: true,
      picture: '',
    })
    expect(result).to.equal(true)
  })

  it('should not process band image if it is already in AWS S3', async () => {
    const band = new Band({ picture: 'http://metalheads.imgix.net/abc.png' })
    const result = await bandImagesETL.extractBandImages(band)
    expect(result._id).to.equal(band._id)
  })

  it('should not generate band picture url if not necessary', async () => {
    const band = new Band({
      picture: 'a-nice-picture.png',
      dataExtracted: true,
      metalArchivesId: '11',
    })
    const result = await bandImagesETL.extractBandImages(band)
    expect(result.picture).to.equal('a-nice-picture.png')
  })

  it('should not generate albums pictures urls if not necessary', async () => {
    const albumId = id()
    let album = new Album({
      _id: albumId,
      picture: 'a-very-nice-picture.png',
      dataExtracted: true,
      metalArchivesId: '33',
    })
    let band = new Band({
      picture: null,
      dataExtracted: false,
      metalArchivesId: '11',
      albums: [album],
    })
    band = await bandImagesETL.extractAlbumsImages(band)
    album = band.albums[0]
    expect(album.picture).to.equal(
      `http://metalheads.imgix.net/albums/${albumId}_picture.jpg`,
    )
  })

  it('should not generate artists pictures urls if not necessary', async () => {
    const artistId = id()
    let artist = new Artist({
      _id: artistId,
      picture: 'a-very-nice-picture.png',
      dataExtracted: true,
      metalArchivesId: '33',
    })
    let band = new Band({
      picture: null,
      dataExtracted: false,
      metalArchivesId: '11',
      currentMembers: [artist],
    })
    band = await bandImagesETL.extractArtistsImages(band)
    artist = band.currentMembers[0]
    expect(artist.picture).to.equal(
      `http://metalheads.imgix.net/artists/${artistId}_picture.jpg`,
    )
  })

  it('should handle upload image errors on extractBandImages', async () => {
    const stub = sinon
      .stub(AWSS3Api.prototype, 'downloadAndUploadImage')
      .throws(new Error('Test Error'))
    const dbMaiden = await Band.findOne({ name: 'Iron Maiden' })
    const result = await bandImagesETL.extractBandImages(dbMaiden)
    expect(result._id).to.equal(dbMaiden._id)
    stub.restore()
  })

  it('should know when is not neccessary to generate an image url', () => {
    const shouldGenerateImageUrl = bandImagesETL.needToGenerateImage({
      picture: 'something',
      dataExtracted: true,
    })
    expect(shouldGenerateImageUrl).to.equal(false)
  })
})
