const id = require('pow-mongodb-fixtures').createObjectId

export const artists = {
  lande: {
    _id: id(),
    name: 'Jørn Lande',
    metalArchivesId: '456',
  },
  allen: {
    _id: id(),
    name: 'Russell Allen',
    metalArchivesId: '5838',
  },
  jami: {
    _id: id(),
    name: 'Jami Huovinen',
    metalArchivesId: '161979',
  },
  tolkki: {
    _id: id(),
    name: 'Timo Tolkki',
    metalArchivesId: '3717',
  },
}

export const albums = {
  battle: {
    _id: id(),
    name: 'The Battle',
    metalArchivesId: '92383',
  },
  revenge: {
    _id: id(),
    name: 'The Revenge',
    metalArchivesId: '149481',
  },
  showdown: {
    _id: id(),
    name: 'The Showdown',
    metalArchivesId: '285406',
  },
  divide: {
    _id: id(),
    name: 'The Great Divide',
    metalArchivesId: '436176',
  },
}

export const bands = {
  allenLande: {
    _id: id(),
    name: 'Allen - Lande',
    metalArchivesId: '47043',
    picture: './allenLande_files/47043_photo.jpg',
    genre: 'Melodic Heavy Metal/Hard Rock',
    location: 'N/A',
    status: 'Active',
    lyricalThemes: 'Fantasy',
    yearsActive: '2005-present',
    formedIn: '2005',
    similarBandsExtracted: false,
    dataExtracted: true,
    currentMembers: [
      artists.lande._id,
      artists.allen._id,
      artists.jami._id,
      artists.tolkki._id,
    ],
    albums: [
      albums.battle._id,
      albums.revenge._id,
      albums.showdown._id,
      albums.divide._id,
    ],
  },
}
