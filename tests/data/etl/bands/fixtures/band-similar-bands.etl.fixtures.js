const id = require('pow-mongodb-fixtures').createObjectId

export const bands = {
  maiden: {
    _id: id(),
    name: 'Iron Maiden',
    metalArchivesId: '25',
  },
  judas: {
    _id: id(),
    name: 'Judas Priest',
    metalArchivesId: '97',
  },
  bruce: {
    _id: id(),
    name: 'Bruce Dickinson',
    metalArchivesId: '295',
  },
  dio: {
    _id: id(),
    name: 'Dio',
    metalArchivesId: '163',
  },
  saxon: {
    _id: id(),
    name: 'Saxon',
    metalArchivesId: '626',
  },
  angelWitch: {
    _id: id(),
    name: 'Angel Witch',
    metalArchivesId: '537',
  },
  apnr: {
    _id: id(),
    name: 'Ария',
    metalArchivesId: '272',
  },
  helloween: {
    _id: id(),
    name: 'Helloween',
    metalArchivesId: '159',
  },
  diamondHead: {
    _id: id(),
    name: 'Diamond Head',
    metalArchivesId: '401',
  },
  satan: {
    _id: id(),
    name: 'Satan',
    metalArchivesId: '774',
  },
  grimReaper: {
    _id: id(),
    name: 'Grim Reaper',
    metalArchivesId: '952',
  },
}
