const id = require('pow-mongodb-fixtures').createObjectId

export const albums = {
  powerslave: {
    _id: id(),
    name: 'Powerslave',
    metalArchivesId: '77',
  },
  bookOfSouls: {
    _id: id(),
    name: 'Book of Souls',
    metalArchivesId: '517686',
  },
}

export const artists = {
  harris: {
    _id: id(),
    name: 'Steve Harris',
    metalArchivesId: '105',
  },
  bruce: {
    _id: id(),
    name: 'Bruce Dickinson',
    metalArchivesId: '30',
  },
}

export const bands = {
  maiden: {
    _id: id(),
    name: 'Iron Maiden',
    metalArchivesId: '25',
    albums: [albums.powerslave._id, albums.bookOfSouls._id],
    currentMembers: [artists.harris._id, artists.bruce._id],
  },
}

export const imagestoprocess = [
  {
    band: bands.maiden._id,
  },
]
