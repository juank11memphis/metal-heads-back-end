const id = require('pow-mongodb-fixtures').createObjectId

export const bands = {
  maiden: {
    _id: id(),
    name: 'Iron Maiden',
    metalArchivesId: '25',
  },
}
