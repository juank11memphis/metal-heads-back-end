export const getExpectedArtistData = () => {
  return {
    realName: 'Stephen Percy Harris',
    age: '61 (born Mar 12th, 1956)',
    placeOfOrigin: 'United Kingdom (Leytonstone, London)',
    gender: 'Male',
    picture: './artist-detail_files/105_artist.jpg',
    activeBands: [
      {
        name: 'Iron Maiden',
        metalArchivesId: '25',
        role: 'Bass, Keyboards\t\t\t\t\t\t\n\t\t\t\t\t\t(1975-present)',
      },
      {
        name: 'Steve Harris',
        metalArchivesId: '3540352457',
        role: 'Bass',
      },
    ],
    pastBands: [
      {
        name: "Gypsy's Kiss",
        role: 'Bass\t\t\t\t\t\t\n\t\t\t\t\t\t(1972-1973)',
      },
      {
        name: 'Smiler',
        role: 'Bass\t\t\t\t\t\t\n\t\t\t\t\t\t(1974-1975)',
      },
    ],
  }
}

export const expectedWebScraperSimilarBandsIds = [
  '97',
  '295',
  '163',
  '626',
  '537',
  '272',
  '159',
  '401',
  '774',
  '952',
]
