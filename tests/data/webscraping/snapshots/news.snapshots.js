export const expectedBlabbermouthNews = [
  {
    title:
      "WHITFIELD CRANE Says UGLY KID JOE Reunion is 'Really Cathartic And Cool'",
    link:
      'http://www.blabbermouth.nethttp://www.blabbermouth.net/news/whitfield-crane-says-ugly-kid-joe-reunion-is-really-cathartic-and-cool/',
  },
  {
    title:
      "POWERMAN 5000 Frontman Says Making New Albums 'Never Seems To Be A Comfortable Experience'",
    link:
      'http://www.blabbermouth.nethttp://www.blabbermouth.net/news/powerman-5000-frontman-says-making-new-albums-never-seems-to-be-a-comfortable-experience/',
  },
  {
    title:
      "MICHAEL SWEET On New SWEET & LYNCH Album 'Unified': 'I Think It's Going To Be A Winner'",
    link:
      'http://www.blabbermouth.nethttp://www.blabbermouth.net/news/michael-sweet-on-new-sweet-lynch-album-unified-i-think-its-going-to-be-a-winner/',
  },
  {
    title:
      "AVENGED SEVENFOLD Wanted To Do 'Something Cool' With Cover Of PINK FLOYD's 'Wish You Were Here'",
    link:
      'http://www.blabbermouth.nethttp://www.blabbermouth.net/news/avenged-sevenfold-wanted-to-do-something-cool-with-cover-of-pink-floyds-wish-you-were-here/',
  },
  {
    title:
      "ANTHRAX Guitarist To Release 'Access All Areas: Stories From A Hard Rock Life' Book In December",
    link:
      'http://www.blabbermouth.nethttp://www.blabbermouth.net/news/anthrax-guitarist-to-release-access-all-areas-stories-from-a-hard-rock-life-book-in-december/',
  },
  {
    title:
      'DECAPITATED Singer, Bassist Appear In Spokane Court On Rape And Kidnapping Charges',
    link:
      'http://www.blabbermouth.nethttp://www.blabbermouth.net/news/decapitated-singer-bassist-appear-in-spokane-court-on-rape-and-kidnapping-charges/',
  },
  {
    title: "Former ACCEPT Singer DAVID REECE Working On 'Mavericks' Solo Album",
    link:
      'http://www.blabbermouth.nethttp://www.blabbermouth.net/news/former-accept-singer-david-reece-working-on-mavericks-solo-album/',
  },
  {
    title: 'LORDI: Video Of Entire Moscow Concert',
    link:
      'http://www.blabbermouth.nethttp://www.blabbermouth.net/news/lordi-video-of-entire-moscow-concert/',
  },
  {
    title: 'Watch SATYRICON Perform In Tilburg',
    link:
      'http://www.blabbermouth.nethttp://www.blabbermouth.net/news/watch-satyricon-perform-in-tilburg/',
  },
  {
    title:
      "MATT HEAFY: 'When We 'Nail' TRIVIUM, It's When We Do Music For Ourselves'",
    link:
      'http://www.blabbermouth.nethttp://www.blabbermouth.net/news/matt-heafy-when-we-nail-trivium-its-when-we-do-music-for-ourselves/',
  },
]

export const expectedLoudwireNews = [
  {
    title:
      'Anthrax’s Scott Ian to Release ‘Access All Areas: Stories From a Hard Rock Life’ Book in D…',
    link:
      'http://loudwire.com/anthrax-scott-ian-all-access-stories-from-a-hard-rock-life-book/',
  },
  {
    title:
      'Scorpions’ ‘Born to Touch Your Feelings’ Ballad Compilation Details Revealed, Will Include…',
    link:
      'http://loudwire.com/scorpions-born-to-touch-your-feelings-compilation-new-songs/',
  },
  {
    title:
      'Bring Me the Horizon Members Finish Mount Kilimanjaro Charity Climb, Share Photos',
    link: 'http://loudwire.com/bring-me-the-horizon-kilimanjaro-photos/',
  },
  {
    title: '10 Insane Moments in Awards Show History',
    link: 'http://loudwire.com/10-insane-moments-in-awards-show-history/',
  },
  {
    title:
      'Sons of Apollo Unleash ‘Lost in Oblivion’ Video, Plus News on Veil of Maya, Hoobastank + M…',
    link:
      'http://loudwire.com/sons-of-apollo-lost-in-oblivion-video-veil-of-maya-hoobastank-more/',
  },
  {
    title:
      'Lzzy Hale on Halestorm’s Next Album: ’Now is the Time to Double Down on Rock’',
    link:
      'http://loudwire.com/lzzy-hale-halestorm-next-album-double-down-on-rock/',
  },
  {
    title: 'Jason Newsted Lists Enormous Montana Ranch for Sale',
    link:
      'http://loudwire.com/jason-newsted-lists-enormous-montana-ranch-for-sale/',
  },
  {
    title: 'GWAR, ‘The Blood of Gods’ – Album Review',
    link: 'http://loudwire.com/gwar-the-blood-of-gods-album-review/',
  },
  {
    title:
      'Sons of Apollo’s Mike Portnoy + Derek Sherinian: We Transcend Dream Theater Comparison, Fi…',
    link:
      'http://loudwire.com/sons-of-apollo-mike-portnoy-derek-sherinian-transcend-dream-theater-comparison-find-beauty-art-strategic-wankery/',
  },
  {
    title: 'Trivium, ‘The Sin and the Sentence’ – Album Review',
    link: 'http://loudwire.com/trivium-the-sin-and-the-sentence-album-review/',
  },
]
