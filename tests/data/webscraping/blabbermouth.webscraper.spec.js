import { expect } from 'chai'
import fs from 'fs'
import path from 'path'

import '../../tests.util'
import blabbermouthWebScraper from '../../../app/data/webscraping/blabbermouth.webscraper'
import { expectedBlabbermouthNews } from './snapshots/news.snapshots'

describe('Blabbermouth webscraper', () => {
  it('should scrap blabbermouth news', async () => {
    const html = fs.readFileSync(
      path.join(__dirname, './htmls/blabbermouth-news.html'),
    )
    const news = await blabbermouthWebScraper.scrapNews(html)
    expect(news.length).to.equal(10)
    expect(news).to.containSubset(expectedBlabbermouthNews)
  })
})
