import { expect } from 'chai'
import fs from 'fs'
import path from 'path'

import '../../tests.util'
import metalArchivesAlbumWebscraper from '../../../app/data/webscraping/metal-archives-album.webscraper'
import {
  expectedTwoSidesAlbum,
  expectedSingleSideAlbum,
  expectedMultiSidesAlbum,
} from './snapshots/metal-archives-album.webscraper.snapshots'

describe('MetalArchivesAlbumWebscraper', () => {
  it('should scrap full data for a single side album from the metal archives', async () => {
    const html = fs.readFileSync(
      path.join(__dirname, './htmls/brave-new-world.html'),
    )
    const album = await metalArchivesAlbumWebscraper.scrapAlbumFullData(
      null,
      '',
      '',
      html,
    )
    expect(album).to.deep.equal(expectedSingleSideAlbum)
  })

  it('should scrap full data for a 2 sides album from the metal archives', async () => {
    const html = fs.readFileSync(
      path.join(__dirname, './htmls/piece-of-mind.html'),
    )
    const album = await metalArchivesAlbumWebscraper.scrapAlbumFullData(
      null,
      '',
      '',
      html,
    )
    expect(album).to.deep.equal(expectedTwoSidesAlbum)
  })

  it('should scrap full data for a multi sides album from the metal archives', async () => {
    const html = fs.readFileSync(
      path.join(__dirname, './htmls/deep-purple-in-concert.html'),
    )
    const album = await metalArchivesAlbumWebscraper.scrapAlbumFullData(
      null,
      '',
      '',
      html,
    )
    expect(album).to.deep.equal(expectedMultiSidesAlbum)
  })
})
