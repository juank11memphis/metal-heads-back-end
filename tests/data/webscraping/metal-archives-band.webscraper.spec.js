import { expect } from 'chai'
import fs from 'fs'
import path from 'path'

import '../../tests.util'
import metalArchivesBandWebScraper from '../../../app/data/webscraping/metal-archives-band.webscraper'
import {
  expectedBandWebScraperData,
  expectedBrainstormWebScraperData,
} from './snapshots/metal-archives-band.webscraper.snapshots'

describe('MetalArchivesBandWebScraper', () => {
  it('should scrap full data for a band from the metal archives', async () => {
    const html = fs.readFileSync(
      path.join(__dirname, './htmls/iron-maiden.html'),
    )
    const htmlDiscography = fs.readFileSync(
      path.join(__dirname, './htmls/iron-maiden-discography.html'),
    )
    const band = await metalArchivesBandWebScraper.scrapBandFullData(null, '', [
      html,
      htmlDiscography,
    ])
    expect(band).to.deep.equal(expectedBandWebScraperData())
  })

  it('should scrap full data for a band from the metal archives when data is not complete', async () => {
    const html = fs.readFileSync(
      path.join(__dirname, './htmls/brainstorm.html'),
    )
    const htmlDiscography = fs.readFileSync(
      path.join(__dirname, './htmls/brainstorm-discography.html'),
    )
    const band = await metalArchivesBandWebScraper.scrapBandFullData(null, '', [
      html,
      htmlDiscography,
    ])
    expect(band).to.deep.equal(expectedBrainstormWebScraperData())
  })
})
