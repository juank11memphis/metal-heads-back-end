import { expect } from 'chai'
import fs from 'fs'
import path from 'path'

import '../../tests.util'
import loudwireWebScraper from '../../../app/data/webscraping/loudwire.webscraper'
import { expectedLoudwireNews } from './snapshots/news.snapshots'

describe('Blabbermouth webscraper', () => {
  it('should scrap loudwire news', async () => {
    const html = fs.readFileSync(
      path.join(__dirname, './htmls/loudwire-news.html'),
    )
    const news = await loudwireWebScraper.scrapNews(html)
    expect(news.length).to.equal(10)
    expect(news).to.containSubset(expectedLoudwireNews)
  })
})
