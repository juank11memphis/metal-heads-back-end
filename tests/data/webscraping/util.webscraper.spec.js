import { expect } from 'chai'

import '../../tests.util'
import {
  getRequestOptions,
  USER_AGENT_NAME,
} from '../../../app/data/webscraping/util.webscraper'

describe('String util', () => {
  it('should return request options used in web scrapers', () => {
    const url = 'an-awesome-url'
    const requestOptions = getRequestOptions(url)
    expect(requestOptions).to.deep.equal({
      url,
      headers: {
        'User-Agent': USER_AGENT_NAME,
      },
    })
  })
})
