import { expect } from 'chai'
import fs from 'fs'
import path from 'path'

import '../../tests.util'
import metalArchivesWebScraper from '../../../app/data/webscraping/metal-archives.webscraper'
import {
  getExpectedArtistData,
  expectedWebScraperSimilarBandsIds,
} from './snapshots/metal-archives.webscraper.snapshots'

describe('MetalArchivesWebScraper', () => {
  it('should scrap similar bands ids', async () => {
    const html = fs.readFileSync(
      path.join(__dirname, './htmls/metal-archives-similar-bands.html'),
    )
    const similarBandsIds = await metalArchivesWebScraper.scrapBandSimilarBandsIds(
      null,
      html,
    )
    expect(similarBandsIds).to.deep.equal(expectedWebScraperSimilarBandsIds)
  })

  it('should scrap artits full data', async () => {
    const html = fs.readFileSync(
      path.join(__dirname, './htmls/metal-archives-artist-detail.html'),
    )
    const artist = await metalArchivesWebScraper.scrapArtistFullData(
      '',
      null,
      html,
    )
    expect(artist).to.deep.equal(getExpectedArtistData())
  })
})
