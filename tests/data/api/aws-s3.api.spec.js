import { expect } from 'chai'
import sinon from 'sinon'
import request from 'request'
import aws from 'aws-sdk'

import '../../tests.util'
import awsS3Api, { AWSS3Api } from '../../../app/data/api/aws-s3.api'
import Band from '../../../app/components/bands/band'
import Artist from '../../../app/components/artists/artist'
import Album from '../../../app/components/albums/album'
import Constants from '../../../app/core/constants'

const id = require('pow-mongodb-fixtures').createObjectId

const sandbox = sinon.createSandbox()

describe('AWSS3Api', () => {
  before(done => {
    sandbox.stub(request, 'get').yields(
      null,
      {
        statusCode: 200,
      },
      'an image buffer',
    )
    const putObject = (aws.S3.prototype.putObject = sandbox.stub())
    putObject.yields(null, {})
    done()
  })

  after(() => {
    sandbox.restore()
  })

  it('should download and upload images', async () => {
    const imageUrlResult = await awsS3Api.downloadAndUploadImage(
      'img-url',
      'img-path',
    )
    expect(imageUrlResult).to.equal('http://metalheads.imgix.net/img-path')
  })

  it('should download and upload images in production mode', async () => {
    const stub = sinon.stub(Constants, 'isProduction').returns(true)
    const stub2 = sinon
      .stub(awsS3Api, 'uploadImage')
      .returns('an-awesome-image-url.png')
    const imageUrlResult = await awsS3Api.downloadAndUploadImage(
      'img-url',
      'img-path',
    )
    expect(imageUrlResult).to.equal('an-awesome-image-url.png')
    stub.restore()
    stub2.restore()
  })

  it('should uploadImage images', async () => {
    const imageUrlResult = await awsS3Api.uploadImage('imageBuffer', 'img-path')
    expect(imageUrlResult).to.equal('http://metalheads.imgix.net/img-path')
  })

  it('should handle unexpected errors on upload images', async () => {
    const putObjectStub = (aws.S3.prototype.putObject = sinon.stub())
    putObjectStub.yields(new Error('Test Error'), null)
    try {
      await awsS3Api.uploadImage('imageBuffer', 'img-path')
    } catch (error) {
      expect(error.message).to.equal('Test Error')
    }
  })

  it('should generate s3 image paths based on model instance', () => {
    let newId = id()
    const newBand = new Band({
      _id: newId,
    })
    let imagePath = awsS3Api.generateS3ImagePath(newBand, 'band')
    expect(imagePath).to.equal(`bands/${newId}_picture.jpg`)

    newId = id()
    const newArtist = new Artist({
      _id: newId,
    })
    imagePath = awsS3Api.generateS3ImagePath(newArtist, 'artist')
    expect(imagePath).to.equal(`artists/${newId}_picture.jpg`)

    newId = id()
    const newAlbum = new Album({
      _id: newId,
    })
    imagePath = awsS3Api.generateS3ImagePath(newAlbum, 'album')
    expect(imagePath).to.equal(`albums/${newId}_picture.jpg`)
  })
})
