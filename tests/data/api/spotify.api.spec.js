import { expect } from 'chai'
import sinon from 'sinon'

import { SpotifyApi } from '../../../app/data/api/spotify.api'

describe('Spotify api', () => {
  it('should return true when the spotify accessToken is still valid', async () => {
    const stub = sinon.stub(SpotifyApi.prototype, 'getUserData').returns(true)
    const spotifyApi = new SpotifyApi()
    const result = await spotifyApi.validateAccessToken('a very good token')
    expect(result).to.equal(true)
    stub.restore()
  })

  it('should return false when the spotify accessToken is invalid', async () => {
    const stub = sinon
      .stub(SpotifyApi.prototype, 'getUserData')
      .returns(Promise.reject(new Error('test error')))
    const spotifyApi = new SpotifyApi()
    const result = await spotifyApi.validateAccessToken('a very bad token')
    expect(result).to.equal(false)
    stub.restore()
  })
})
