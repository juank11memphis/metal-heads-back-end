// Load environment variables
import mongoose from 'mongoose'
import chai from 'chai'
import chaiSubset from 'chai-subset'

import Constants from '../app/core/constants'
import Band from '../app/components/bands/band'
import Artist from '../app/components/artists/artist'
import Album from '../app/components/albums/album'
import Song from '../app/components/songs/song'
import elasticSearchApi from '../app/data/api/elasticsearch.api'

chai.use(chaiSubset)
mongoose.Promise = global.Promise

const fixtures = require('pow-mongodb-fixtures').connect('test')

export const connectMongoose = () => {
  const promise = async (resolve, reject) => {
    try {
      const alreadyConnected =
        mongoose.connection.db !== undefined && mongoose.connection.db !== null
      if (alreadyConnected) {
        resolve()
      } else {
        mongoose.Promise = global.Promise
        mongoose.connect(
          Constants.mongo.uri,
          { useNewUrlParser: true },
        )
        mongoose.connection.once('open', () => {
          fixtures.clear(() => resolve())
        })
      }
    } catch (error) {
      reject(error)
    }
  }
  return new Promise(promise)
}

export const testsSetup = async (fixturesFile, done) => {
  await connectMongoose()
  try {
    await elasticSearchApi.deleteCurrentIndex()
    await elasticSearchApi.createCurrentIndex()
  } catch (error) {}
  fixtures.clearAllAndLoad(fixturesFile, async () => {
    const allBands = await Band.find({})
    await elasticSearchApi.bulkBands(allBands)
    const allArtists = await Artist.find({})
    await elasticSearchApi.bulkArtists(allArtists)
    const allAlbums = await Album.find({})
    await elasticSearchApi.bulkAlbums(allAlbums)
    const allSongs = await Song.find({})
    await elasticSearchApi.bulkSongs(allSongs)
    done()
  })
}
