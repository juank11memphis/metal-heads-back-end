import { expect } from 'chai'

import { graphHandler } from '../../app/core/middleware/graph-handler'
import { generateToken } from '../../app/util/auth.util'

describe('Graph Handler', () => {
  it('should throw error on invalid token', async () => {
    const token = 'a bad token'
    try {
      await graphHandler({
        func: () => {},
        funcName: 'test',
        auth: { token },
      })
    } catch (error) {
      expect(error.message).to.equal('Invalid token')
    }
  })

  it('should throw error on undefined or null token', async () => {
    const token = undefined
    try {
      await graphHandler({
        func: () => {},
        funcName: 'test',
        auth: { token },
      })
    } catch (error) {
      expect(error.message).to.equal('Invalid token')
    }
  })

  it('should throw error on invalid permissions', async () => {
    const token = await generateToken({
      _id: '11',
      roles: [
        {
          name: 'test',
          permissions: ['permission-b'],
        },
      ],
    })
    try {
      await graphHandler({
        func: () => {},
        funcName: 'test',
        auth: {
          token: `bearer ${token}`,
          permission: 'permission-a',
        },
      })
    } catch (error) {
      expect(error.message).to.equal('Not enough permissions')
    }
  })

  it('should call function when token and permissions are fine', async () => {
    const token = await generateToken({
      _id: '11',
      roles: [
        {
          name: 'test',
          permissions: ['permission-a'],
        },
      ],
    })
    const theFunction = () => {
      return 'it works'
    }
    const result = await graphHandler({
      func: theFunction,
      funcName: 'test',
      auth: {
        token: `bearer ${token}`,
        permission: 'permission-a',
      },
    })
    expect(result).to.equal('it works')
  })
})
