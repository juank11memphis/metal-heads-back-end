const id = require('pow-mongodb-fixtures').createObjectId

export const songs = {
  speedKing: {
    _id: id(),
    name: 'Speed King',
    length: '7:11',
  },
  strangeWoman: {
    _id: id(),
    name: 'Strange Kind of Woman',
    length: '8:17',
  },
  weirdSong: {
    _id: id(),
    name: 'The Weirdest Song',
    length: '11:11',
  },
}
