import { expect } from 'chai'
import sinon from 'sinon'

import { testsSetup } from '../../tests.util'
import songsService from '../../../app/components/songs/songs.service'
import songsDao from '../../../app/components/songs/songs.dao'
import { songs } from './songs.service.fixtures'

describe('SongsService', () => {
  beforeEach(done => {
    testsSetup(__dirname + '/songs.service.fixtures.js', done)
  })

  it('should load songs by ids', async () => {
    const songsLoaded = await songsService.loadByIds([
      songs.speedKing._id,
      songs.strangeWoman._id,
      songs.weirdSong._id,
    ])
    expect(songsLoaded.length).to.equal(3)
  })

  it('should load a song by id', async () => {
    const songLoaded = await songsService.loadSongById(songs.speedKing._id)
    expect(songLoaded.name).to.equal('Speed King')
  })
})
