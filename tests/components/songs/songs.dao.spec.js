import { expect } from 'chai'

import { testsSetup } from '../../tests.util'
import songsDao from '../../../app/components/songs/songs.dao'
import elasticSearchApi from '../../../app/data/api/elasticsearch.api'

describe('SongsDao', () => {
  beforeEach(done => {
    testsSetup(__dirname + '/songs.service.fixtures.js', done)
  })

  it('should insert all songs into elasticsearch', async () => {
    await songsDao.storeAllInElasticsearch()
    const esSongs = await elasticSearchApi.searchByEntityType('song')
    expect(esSongs.length).to.equal(3)
  })
})
