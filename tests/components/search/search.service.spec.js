import { expect } from 'chai'
import sinon from 'sinon'

import { testsSetup } from '../../tests.util'
import searchService from '../../../app/components/search/search.service'
import { generateToken } from '../../../app/util/auth.util'
import { expectedMaidenSearchResults } from './fixtures/search.service.snapshots'
import { ElasticSearchApi } from '../../../app/data/api/elasticsearch.api'

describe('SearchService', () => {
  beforeEach(done => {
    testsSetup(__dirname + '/fixtures/search.service.fixtures.js', done)
  })

  it('should return results when searching for maiden', async () => {
    const found = await searchService.search('Iron Maiden')
    expect(found).to.deep.equal(expectedMaidenSearchResults)
  })

  it('should return nothing when searching did not find anything', async () => {
    const found = await searchService.search('bad search')
    expect(found.bands.length).to.equal(0)
    expect(found.albums.length).to.equal(0)
    expect(found.artists.length).to.equal(0)
  })
})
