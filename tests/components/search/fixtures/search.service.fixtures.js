const id = require('pow-mongodb-fixtures').createObjectId

export const bands = {
  maiden: {
    _id: id(),
    name: 'Iron Maiden',
    ratingsData: { ratingsCount: 4, globalRating: 10 },
    country: 'England',
  },
  bruce: {
    _id: id(),
    name: 'Bruce Dickinson',
    ratingsData: { ratingsCount: 1, globalRating: 9 },
    country: 'England',
  },
  judas: {
    _id: id(),
    name: 'Judas Priest',
    ratingsData: { ratingsCount: 3, globalRating: 9 },
    country: 'England',
  },
  dio: {
    _id: id(),
    name: 'Dio',
    ratingsData: { ratingsCount: 2, globalRating: 8 },
    country: 'USA',
  },
  saxon: {
    _id: id(),
    name: 'Saxon',
    ratingsData: { ratingsCount: 1, globalRating: 7 },
    country: 'England',
  },
  angelWitch: {
    _id: id(),
    name: 'Angel Witch',
    ratingsData: { ratingsCount: 1, globalRating: 5 },
    country: 'England',
  },
  apnr: {
    _id: id(),
    name: 'Ария',
    ratingsData: { ratingsCount: 2, globalRating: 4 },
    country: 'Russia',
  },
  helloween: {
    _id: id(),
    name: 'Helloween',
    ratingsData: { ratingsCount: 3, globalRating: 3 },
    country: 'Germany',
  },
  diamondHead: {
    _id: id(),
    name: 'Diamond Head',
    ratingsData: { ratingsCount: 1, globalRating: 2 },
    country: 'England',
  },
  satan: {
    _id: id(),
    name: 'Satan',
    ratingsData: { ratingsCount: 1, globalRating: 1 },
    country: 'Sweeden',
  },
  grimReaper: {
    _id: id(),
    name: 'Grim Reaper',
    ratingsData: { ratingsCount: 2, globalRating: 6 },
    country: 'England',
  },
  megadeth: {
    _id: id(),
    name: 'Megadeth',
    country: 'USA',
  },
  primalFear: {
    _id: id(),
    name: 'Primal Fear',
    country: 'Germany',
  },
  warcry: {
    _id: id(),
    name: 'Warcry',
    country: 'Spain',
  },
  maidenOfMars: {
    _id: id(),
    name: 'Maiden of Mars',
    country: 'USA',
  },
  maidens: {
    _id: id(),
    name: 'Iron Maidens',
    country: 'USA',
  },
}

export const albums = {
  powerslave: {
    _id: id(),
    name: 'Powerslave',
    band: bands.maiden._id,
    year: 1999,
  },
  deepPurpleConcert: {
    _id: id(),
    name: 'Deep Purple In Concert',
    band: bands.maiden._id,
    year: 2000,
  },
  ironMaiden: {
    _id: id(),
    name: 'Iron Maiden',
    band: bands.maiden._id,
    year: 2001,
  },
}

export const artists = {
  harris: {
    _id: id(),
    name: 'Steve Harris',
    instrument: 'Bass Guitar',
  },
  halford: {
    _id: id(),
    name: 'Rob Halford',
    instrument: 'Vocals',
  },
  mustaine: {
    _id: id(),
    name: 'Dave Mustaine',
    instrument: 'Lead Guitar',
  },
  murray: {
    _id: id(),
    name: 'Dave Murray',
    instrument: 'Lead Guitar',
  },
  maidenMorales: {
    _id: id(),
    name: 'Iron Maiden Morales',
    gender: 'Male',
  },
}
