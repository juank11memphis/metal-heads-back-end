import { bands, albums, artists } from './search.service.fixtures'

export const expectedMaidenSearchResults = {
  bands: [
    {
      id: bands.maiden._id + '',
      name: 'Iron Maiden',
      country: 'England',
      globalRating: 10,
    },
  ],
  albums: [
    {
      id: albums.ironMaiden._id + '',
      name: 'Iron Maiden',
      year: '2001',
    },
  ],
  artists: [
    {
      id: artists.maidenMorales._id + '',
      name: 'Iron Maiden Morales',
      gender: 'Male',
    },
  ],
}
