import { expect } from 'chai'
import sinon from 'sinon'

import { BlabbermouthWebScraper } from '../../../app/data/webscraping/blabbermouth.webscraper'
import { LoudwireWebScraper } from '../../../app/data/webscraping/loudwire.webscraper'
import {
  expectedBlabbermouthNews,
  expectedLoudwireNews,
} from '../../data/webscraping/snapshots/news.snapshots'
import newsService from '../../../app/components/news/news.service'

const sandbox = sinon.createSandbox()

describe('NewsService', () => {
  beforeEach(done => {
    sandbox
      .stub(BlabbermouthWebScraper.prototype, 'scrapNews')
      .returns(expectedBlabbermouthNews)
    sandbox
      .stub(LoudwireWebScraper.prototype, 'scrapNews')
      .returns(expectedLoudwireNews)
    done()
  })

  afterEach(() => {
    sandbox.restore()
  })

  it('should load blabbermouth news', async () => {
    const news = await newsService.loadNewsBySource('blabbermouth')
    expect(news).to.containSubset(expectedBlabbermouthNews)
  })

  it('should load loudwire news', async () => {
    const news = await newsService.loadNewsBySource('loudwire')
    expect(news).to.containSubset(expectedLoudwireNews)
  })

  it('should return empty array when source is not supported', async () => {
    const news = await newsService.loadNewsBySource('some-weird-source')
    expect(news).to.have.lengthOf(0)
  })
})
