const id = require('pow-mongodb-fixtures').createObjectId

export const usersocialaccounts = {
  spotify: {
    _id: id(),
    accountType: 'spotify',
    accessToken: 'acess-token',
    refreshToken: 'refresh-token',
  },
}

export const users = {
  juanca: {
    _id: id(),
    firstname: 'Juan',
    email: 'juank.memphis@gmail.com',
    socialAccounts: [usersocialaccounts.spotify._id],
  },
  san: {
    _id: id(),
    firstname: 'Sandra',
    email: 'sangmail.com',
  },
}

export const bands = {
  ironMaiden: {
    _id: id(),
    name: 'Iron Maiden',
  },
}

export const albums = {
  powerslave: {
    _id: id(),
    name: 'Powerslave',
    spotifyData: {
      id: '11',
      uri: 'spotify:album:11',
    },
  },
  ironMaiden: {
    _id: id(),
    name: 'Iron Maiden',
    band: bands.ironMaiden._id,
  },
}
