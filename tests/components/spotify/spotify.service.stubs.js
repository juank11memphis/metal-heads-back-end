export const ironMaidenAlbumStub = {
  album_type: 'album',
  artists: [
    {
      external_urls: {
        spotify: 'https://open.spotify.com/artist/11',
      },
      href: 'https://api.spotify.com/v1/artists/11',
      id: '11',
      name: 'Iron Maiden',
      type: 'artist',
      uri: 'spotify:artist:11',
    },
  ],
  external_urls: {
    spotify: 'https://open.spotify.com/album/1133',
  },
  href: 'https://api.spotify.com/v1/albums/1133',
  id: '1133',
  images: [
    {
      height: 640,
      url: 'https://i.scdn.co/image/1',
      width: 640,
    },
    {
      height: 300,
      url: 'https://i.scdn.co/image/2',
      width: 300,
    },
    {
      height: 64,
      url: 'https://i.scdn.co/image/3',
      width: 64,
    },
  ],
  name: 'Iron Maiden (1998 Remastered Edition)',
  type: 'album',
  uri: 'spotify:album:1133',
}
