import { expect } from 'chai'
import sinon from 'sinon'

import { testsSetup } from '../../tests.util'
import spotifyService from '../../../app/components/spotify/spotify.service'
import { generateToken } from '../../../app/util/auth.util'
import User from '../../../app/components/users/user'
import Album from '../../../app/components/albums/album'
import { SpotifyApi } from '../../../app/data/api/spotify.api'
import { users, albums } from './spotify.service.fixtures'
import bandsDao from '../../../app/components/bands/bands.dao'
import albumsDao from '../../../app/components/albums/albums.dao'
import usersDao from '../../../app/components/users/users.dao'
import {
  expectedSpotifyApiDevicesList,
  expectedIronMaidenSpotifyData,
  expectedIronMaidenAlbumSpotifyData,
  expectedIronMaidenBandSpotifyData,
  expectedPlayAlbumActivity,
} from './spotify.service.snapshots'
import { ironMaidenAlbumStub } from './spotify.service.stubs'
import activitiesDao from '../../../app/components/activities/activities.dao'
import { sleep } from '../../../app/util/general.util'

const sandbox = sinon.createSandbox()

describe('SpotifyService', () => {
  beforeEach(done => {
    sandbox.restore()
    testsSetup(__dirname + '/spotify.service.fixtures.js', done)
  })

  afterEach(done => {
    sandbox.restore()
    done()
  })

  it('should return an empty list if user is not connected to Spotify', async () => {
    const devicesList = await spotifyService.loadPlayerDevices(users.san._id)
    expect(devicesList).to.deep.equal([])
  })

  it('should return a proper list of devices', async () => {
    sandbox.stub(SpotifyApi.prototype, 'validateAccessToken').returns(false)
    sandbox
      .stub(SpotifyApi.prototype, 'refreshAccessToken')
      .returns('a-fresh-access-token')
    sandbox
      .stub(SpotifyApi.prototype, 'getPlayerDevices')
      .returns(expectedSpotifyApiDevicesList)

    const devicesList = await spotifyService.loadPlayerDevices(users.juanca._id)
    expect(devicesList).to.deep.equal(expectedSpotifyApiDevicesList.devices)
  })

  it('should return an album spotify uri when the album already had it on the database', async () => {
    const albumSpotifyUri = await spotifyService.getAlbumSpotifyUri(
      users.juanca._id,
      albums.powerslave._id,
    )
    expect(albumSpotifyUri).to.deep.equal('spotify:album:11')
  })

  it('should handle looking for an album spotify uri when the user is not connected to spotify', async () => {
    sandbox.stub(SpotifyApi.prototype, 'validateAccessToken').returns(false)
    sandbox.stub(SpotifyApi.prototype, 'refreshAccessToken').returns(null)

    const albumSpotifyUri = await spotifyService.getAlbumSpotifyUri(
      users.juanca._id,
      albums.ironMaiden._id,
    )
    expect(albumSpotifyUri).to.deep.equal(null)
  })

  it('should handle looking for an album spotify uri if it was not found on Spotify', async () => {
    sandbox.stub(SpotifyApi.prototype, 'validateAccessToken').returns(true)
    sandbox.stub(SpotifyApi.prototype, 'searchAlbum').returns(null)

    const albumSpotifyUri = await spotifyService.getAlbumSpotifyUri(
      users.juanca._id,
      albums.ironMaiden._id,
    )
    expect(albumSpotifyUri).to.deep.equal(null)
  })

  it('should handle looking for an album spotify uri if it was found on Spotify', async () => {
    sandbox.stub(SpotifyApi.prototype, 'validateAccessToken').returns(true)
    sandbox
      .stub(SpotifyApi.prototype, 'searchAlbum')
      .returns(ironMaidenAlbumStub)

    const albumSpotifyUri = await spotifyService.getAlbumSpotifyUri(
      users.juanca._id,
      albums.ironMaiden._id,
    )
    expect(albumSpotifyUri).to.deep.equal('spotify:album:1133')

    const expectedAlbum = await albumsDao.getDetail(albums.ironMaiden._id)
    expect(expectedAlbum.spotifyData).to.deep.equal(
      expectedIronMaidenAlbumSpotifyData,
    )

    const expectedBand = await bandsDao.getDetail(albums.ironMaiden.band)
    expect(expectedBand.spotifyData).to.deep.equal(
      expectedIronMaidenBandSpotifyData,
    )
  })

  it('should return true if service was able to play album on spotify', async () => {
    sandbox.stub(SpotifyApi.prototype, 'validateAccessToken').returns(true)
    sandbox.stub(SpotifyApi.prototype, 'toggleShuffle').returns(true)
    sandbox.stub(SpotifyApi.prototype, 'playOnDevice').returns(true)

    const playSuccess = await spotifyService.playAlbum(
      users.juanca._id,
      'a-device-id',
      albums.powerslave._id,
    )

    expect(playSuccess).to.equal(true)

    // assert activity
    await sleep(500)
    const activity = await activitiesDao.findOneLatest({
      actor: users.juanca._id,
      verb: 'play',
      target: 'spotify',
      objectId: albums.powerslave._id,
    })
    expect(activity._id).not.to.be.undefined
    expect(activity.actor + '').to.deep.equal(users.juanca._id + '')
    expect(activity.verb).to.equal('play')
    expect('' + activity.objectId).to.deep.equal('' + albums.powerslave._id)
    expect(activity.objectType).to.equal('album')
    expect(activity.target).to.equal('spotify')
  })
})
