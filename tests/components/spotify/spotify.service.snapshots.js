import { users, albums } from './spotify.service.fixtures'

export const expectedSpotifyApiDevicesList = {
  devices: [
    {
      id: 'some-id',
      is_active: true,
      is_restricted: false,
      name: 'Juan’s MacBook Pro',
      type: 'Computer',
      volume_percent: 100,
    },
  ],
}

export const expectedIronMaidenAlbumSpotifyData = {
  id: '1133',
  uri: 'spotify:album:1133',
  externalUrl: 'https://open.spotify.com/album/1133',
  images: [
    { height: 640, url: 'https://i.scdn.co/image/1', width: 640 },
    { height: 300, url: 'https://i.scdn.co/image/2', width: 300 },
    { height: 64, url: 'https://i.scdn.co/image/3', width: 64 },
  ],
}

export const expectedIronMaidenBandSpotifyData = {
  id: '11',
  uri: 'spotify:artist:11',
  externalUrl: 'https://open.spotify.com/artist/11',
  images: null,
}
