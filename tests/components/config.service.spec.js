import { expect } from 'chai'

import '../tests.util'
import configService from '../../app/components/config/config.service'

describe('Config Service', () => {
  it('should load config object', async () => {
    const config = await configService.loadConfig()
    expect(config).to.deep.equal({
      googleClientId: 'test-google-client-id',
    })
  })
})
