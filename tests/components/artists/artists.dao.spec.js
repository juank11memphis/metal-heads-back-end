import { expect } from 'chai'

import { testsSetup } from '../../tests.util'
import artistsDao from '../../../app/components/artists/artists.dao'
import elasticSearchApi from '../../../app/data/api/elasticsearch.api'

describe('ArtistsDao', () => {
  beforeEach(done => {
    testsSetup(__dirname + '/artists.service.fixtures.js', done)
  })

  it('should insert all artists into elasticsearch', async () => {
    await artistsDao.storeAllInElasticsearch()
    const esArtists = await elasticSearchApi.searchByEntityType('artist')
    expect(esArtists.length).to.equal(3)
  })
})
