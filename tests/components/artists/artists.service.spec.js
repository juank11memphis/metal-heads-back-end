import { expect } from 'chai'
import sinon from 'sinon'

import { testsSetup } from '../../tests.util'
import artistsService from '../../../app/components/artists/artists.service'
import { ArtistETL } from '../../../app/data/etl/artists/artist.etl'
import { expectedArtistData } from '../../data/etl/artists/artists.etl.snapshots'
import { artists } from './artists.service.fixtures'
import {
  expectedHalfordData,
  expectedMustainData,
} from './artists.service.snapshots'
import Artist from '../../../app/components/artists/artist'
import elasticsearchApi from '../../../app/data/api/elasticsearch.api'

const sandbox = sinon.createSandbox()

describe('ArtistsService', () => {
  beforeEach(done => {
    sandbox.stub(ArtistETL.prototype, 'start').returns(expectedArtistData)
    testsSetup(__dirname + '/artists.service.fixtures.js', done)
  })

  afterEach(() => {
    sandbox.restore()
  })

  it('should load artists by ids', async () => {
    const artistsLoaded = await artistsService.loadByIds([
      artists.harris._id,
      artists.mustaine._id,
    ])
    expect(artistsLoaded.length).to.equal(2)
  })

  it('should return artist details when they have been already extracted', async () => {
    const halfordDetails = await artistsService.loadArtistById(
      artists.halford._id,
    )
    expect(halfordDetails).to.containSubset(expectedHalfordData)
  })

  it('should return artist details when they have not been extracted yet', async () => {
    const artist = await artistsService.loadArtistById(artists.harris._id)
    expect(artist).to.containSubset(expectedArtistData)
  })

  it('should return artist details when they need data sync', async () => {
    const artist = await artistsService.loadArtistById(artists.mustaine._id)
    expect(artist).to.containSubset(expectedMustainData)
  })
})
