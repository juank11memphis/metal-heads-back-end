import { expect } from 'chai'

import '../../tests.util'
import Artist from '../../../app/components/artists/artist'
import { getRolesMapAsArray } from '../../../app/components/artists/artists.util'

describe('Artist Model', () => {
  it('should return roles map as empty array if current roles is not an object', () => {
    let artist = new Artist({ roles: 'Vocals (1985-present)' })
    let rolesArray = getRolesMapAsArray(artist)

    expect(rolesArray).to.have.lengthOf(0)

    artist = new Artist()
    rolesArray = getRolesMapAsArray(artist)

    expect(rolesArray).to.have.lengthOf(0)

    artist = new Artist({ roles: null })
    rolesArray = getRolesMapAsArray(artist)

    expect(rolesArray).to.have.lengthOf(0)
  })

  it('should return roles map as array if current roles is an object', () => {
    let artist = new Artist({
      roles: {
        'band-1': 'roles for band 1',
        'band-2': 'roles for band 2',
      },
    })
    const rolesArray = getRolesMapAsArray(artist)

    expect(rolesArray).to.deep.equal([
      { bandId: 'band-1', role: 'roles for band 1' },
      { bandId: 'band-2', role: 'roles for band 2' },
    ])
  })
})
