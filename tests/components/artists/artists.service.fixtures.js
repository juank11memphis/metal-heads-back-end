const id = require('pow-mongodb-fixtures').createObjectId

export const artists = {
  harris: {
    _id: id(),
    name: 'Steve Harris',
    metalArchivesId: '105',
  },
  halford: {
    _id: id(),
    name: 'Rob Halford',
    metalArchivesId: '3535',
    dataExtracted: true,
    lastExtractionDate: new Date(),
  },
  mustaine: {
    _id: id(),
    name: 'Dave Mustaine',
    metalArchivesId: '184',
    dataExtracted: true,
  },
}
