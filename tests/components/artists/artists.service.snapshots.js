import { artists } from './artists.service.fixtures'

export const expectedHalfordData = {
  _id: artists.halford._id,
  name: 'Rob Halford',
  metalArchivesId: '3535',
  dataExtracted: true,
  pastBands: [],
  activeBands: [],
}

export const expectedMustainData = {
  _id: artists.mustaine._id,
  name: 'Dave Mustaine',
  metalArchivesId: '184',
  dataExtracted: true,
  pastBands: [],
  activeBands: [],
}
