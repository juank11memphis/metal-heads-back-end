import { albums, bands } from './albums.service.fixtures'

export const expectedPowerslaveData = {
  _id: albums.powerslave._id,
  name: 'Powerslave',
  metalArchivesId: '77',
  band: bands.maiden._id,
  lineUp: [],
  tracks: [],
  dataExtracted: true,
}

export const albumsToSort = [
  {
    year: '2005',
    type: 'Full-length',
    name: 'The Battle',
  },
  {
    year: '2007',
    type: 'Full-length',
    name: 'The Revenge',
  },
  {
    year: '2010',
    type: 'Full-length',
    name: 'The Showdown',
  },
  {
    year: '2014',
    type: 'Full-length',
    name: 'The Great Divide',
  },
  {
    year: '2014',
    type: 'Live album',
    name: 'The Great Divide Live',
  },
  {
    year: '2014',
    type: 'Video',
    name: 'The Great Divide Video',
  },
  {
    year: '2014',
    type: 'Split video',
    name: 'The Great Divide Split Video',
  },
  {
    year: '2014',
    type: 'Compilation',
    name: 'The Great Divide Compilation',
  },
  {
    year: '2014',
    type: 'Boxed set',
    name: 'The Great Divide Boxed set',
  },
  {
    year: '2014',
    type: 'Single',
    name: 'The Great Divide Single',
  },
  {
    year: '2014',
    type: 'Demo',
    name: 'The Great Divide Demo',
  },
  {
    year: '2014',
    type: 'EP',
    name: 'The Great Divide EP',
  },
  {
    year: '2014',
    type: 'Split',
    name: 'The Great Divide Split',
  },
]

export const expectedSortedAlbums = {
  full: [
    { year: '2005', type: 'Full-length', name: 'The Battle' },
    { year: '2007', type: 'Full-length', name: 'The Revenge' },
    { year: '2010', type: 'Full-length', name: 'The Showdown' },
    { year: '2014', type: 'Full-length', name: 'The Great Divide' },
  ],
  live: [
    {
      year: '2014',
      type: 'Live album',
      name: 'The Great Divide Live',
    },
  ],
  video: [
    { year: '2014', type: 'Video', name: 'The Great Divide Video' },
    {
      year: '2014',
      type: 'Split video',
      name: 'The Great Divide Split Video',
    },
  ],
  compilations: [
    {
      year: '2014',
      type: 'Compilation',
      name: 'The Great Divide Compilation',
    },
    {
      year: '2014',
      type: 'Boxed set',
      name: 'The Great Divide Boxed set',
    },
  ],
  singles: [{ year: '2014', type: 'Single', name: 'The Great Divide Single' }],
  other: [
    { year: '2014', type: 'Demo', name: 'The Great Divide Demo' },
    { year: '2014', type: 'EP', name: 'The Great Divide EP' },
    { year: '2014', type: 'Split', name: 'The Great Divide Split' },
  ],
}
