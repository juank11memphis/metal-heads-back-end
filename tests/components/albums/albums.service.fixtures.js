const id = require('pow-mongodb-fixtures').createObjectId

export const bands = {
  maiden: {
    _id: id(),
    name: 'Iron Maiden',
  },
}

export const albums = {
  powerslave: {
    _id: id(),
    name: 'Powerslave',
    metalArchivesId: '77',
    band: bands.maiden._id,
    dataExtracted: true,
  },
  deepPurpleConcert: {
    _id: id(),
    name: 'Deep Purple In Concert',
    metalArchivesId: '3389',
    band: bands.maiden._id,
  },
}
