import { expect } from 'chai'

import { testsSetup } from '../../tests.util'
import albumsDao from '../../../app/components/albums/albums.dao'
import elasticSearchApi from '../../../app/data/api/elasticsearch.api'
import { bands } from './albums.service.fixtures'

describe('Albums DAO', () => {
  beforeEach(done => {
    testsSetup(__dirname + '/albums.service.fixtures.js', done)
  })

  it('should find albums', async () => {
    let albums = await albumsDao.find({ name: 'Powerslave' })
    expect(albums.length).to.equal(1)
    albums = await albumsDao.find({ band: bands.maiden._id })
    expect(albums.length).to.equal(2)
    albums = await albumsDao.find({ band: bands.maiden._id }, 1)
    expect(albums.length).to.equal(1)
  })

  it('should insert all albums into elasticsearch', async () => {
    await albumsDao.storeAllInElasticsearch()
    const esAlbums = await elasticSearchApi.searchByEntityType('album')
    expect(esAlbums.length).to.equal(2)
  })
})
