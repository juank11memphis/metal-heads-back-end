import { expect } from 'chai'
import sinon from 'sinon'

import { testsSetup } from '../../tests.util'
import albumsService from '../../../app/components/albums/albums.service'
import albumsDao from '../../../app/components/albums/albums.dao'
import { AlbumETL } from '../../../app/data/etl/albums/album.etl'
import { expectedAlbum } from '../../data/etl/albums/albums.etl.snapshots'
import { albums } from './albums.service.fixtures'
import {
  expectedPowerslaveData,
  albumsToSort,
  expectedSortedAlbums,
} from './albums.service.snapshots'
import Album from '../../../app/components/albums/album'
import elasticsearchApi from '../../../app/data/api/elasticsearch.api'

const sandbox = sinon.createSandbox()

describe('AlbumsService', () => {
  beforeEach(done => {
    sandbox.stub(AlbumETL.prototype, 'start').returns(expectedAlbum)
    testsSetup(__dirname + '/albums.service.fixtures.js', done)
  })

  afterEach(() => {
    sandbox.restore()
  })

  it('should load albums by ids', async () => {
    const artistsLoaded = await albumsService.loadByIds([
      albums.powerslave._id,
      albums.deepPurpleConcert._id,
    ])
    expect(artistsLoaded.length).to.equal(2)
  })

  it('should return album details when they have been already extracted', async () => {
    const powerslaveDetails = await albumsService.loadAlbumById(
      albums.powerslave._id,
    )
    expect(powerslaveDetails).to.containSubset(expectedPowerslaveData)
  })

  it('should return album details when they have not been extracted yet', async () => {
    const album = await albumsService.loadAlbumById(
      albums.deepPurpleConcert._id,
    )
    expect(album).to.containSubset(expectedAlbum)
  })

  it('should sort albums by type', () => {
    const sortedAlbums = albumsService.sortAlbumsByType(albumsToSort)
    expect(sortedAlbums).to.deep.equal(expectedSortedAlbums)
  })
})
