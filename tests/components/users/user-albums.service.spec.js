import { expect } from 'chai'

import { testsSetup } from '../../tests.util'
import userAlbumsService from '../../../app/components/user-albums/user-albums.service'
import albumsService from '../../../app/components/albums/albums.service'
import { users, bands, albums } from './users.service.fixtures'
import {
  expectedAlbumsByUserAndBand,
  expectedElasticSearchKillers,
  expectedElasticSearchPowerslave,
} from './users.service.snapshots'
import Band from '../../../app/components/bands/band'
import User from '../../../app/components/users/user'
import elasticSearchApi from '../../../app/data/api/elasticsearch.api'
import activitiesDao from '../../../app/components/activities/activities.dao'
import { sleep } from '../../../app/util/general.util'

describe('UserAlbumsService', () => {
  beforeEach(done => {
    testsSetup(__dirname + '/users.service.fixtures.js', done)
  })

  it('should load albums by user and band', async () => {
    const albumsByUserAndBand = await userAlbumsService.loadByUserAndBand(
      users.juanca._id,
      bands.maiden._id,
    )
    expect(albumsByUserAndBand).to.containSubset(expectedAlbumsByUserAndBand)
  })

  it('should load user album data when the user has data', async () => {
    const userAlbumData = await userAlbumsService.getUserAlbumData(
      users.juanca._id,
      albums.powerslave._id,
    )
    expect(userAlbumData).to.containSubset({
      rating: 10,
    })
  })

  it('should load user album data when the user has no data', async () => {
    const userAlbumData = await userAlbumsService.getUserAlbumData(
      users.san._id,
      albums.powerslave._id,
    )
    expect(userAlbumData).to.deep.equal({
      id: `${users.san._id}-${albums.powerslave._id}`,
      rating: null,
      userId: users.san._id,
      albumId: albums.powerslave._id,
    })
  })

  it('should rate an album: album with no prior ratings data', async () => {
    const userAlbum = await userAlbumsService.rateAlbum(
      users.juanca._id,
      albums.killers._id,
      9,
    )
    expect(userAlbum).to.deep.equal({
      id: `${users.juanca._id}-${albums.killers._id}`,
      rating: 9,
      userId: users.juanca._id,
      albumId: albums.killers._id,
    })

    const album = await albumsService.loadAlbumById(albums.killers._id)
    expect(album.ratingsData).to.deep.equal({
      ratingsSum: 9,
      ratingsCount: 1,
      globalRating: 9,
    })

    const userAlbumsByBand = await userAlbumsService.loadByUserAndBand(
      users.juanca._id,
      bands.maiden._id,
    )
    expect(userAlbumsByBand).to.have.lengthOf(4)

    const esAlbum = await elasticSearchApi.getDocumentById(`album-${album._id}`)
    expect(esAlbum).to.deep.equal(expectedElasticSearchKillers)

    // assert activity
    await sleep(500)
    const activity = await activitiesDao.findOneLatest({
      actor: users.juanca._id,
      verb: 'rate',
      objectId: albums.killers._id,
    })
    expect(activity._id).not.to.be.undefined
    expect(activity.actor + '').to.deep.equal(users.juanca._id + '')
    expect(activity.verb).to.equal('rate')
    expect('' + activity.objectId).to.deep.equal('' + albums.killers._id)
    expect(activity.metadata.rating).to.equal(9)
    expect(activity.objectType).to.equal('album')
  })

  it('should rate an album: album with existing ratings data, new user rating', async () => {
    const userAlbum = await userAlbumsService.rateAlbum(
      users.juanca._id,
      albums.bookOfSoulds._id,
      9,
    )
    expect(userAlbum).to.deep.equal({
      id: `${users.juanca._id}-${albums.bookOfSoulds._id}`,
      rating: 9,
      userId: users.juanca._id,
      albumId: albums.bookOfSoulds._id,
    })

    const album = await albumsService.loadAlbumById(albums.bookOfSoulds._id)
    expect(album.ratingsData).to.deep.equal({
      ratingsSum: 44,
      ratingsCount: 5,
      globalRating: 8.8,
    })

    const userAlbumsByBand = await userAlbumsService.loadByUserAndBand(
      users.juanca._id,
      bands.maiden._id,
    )
    expect(userAlbumsByBand).to.have.lengthOf(4)
  })

  it('should rate an album: album with existing ratings data, updating existing user rating', async () => {
    const userAlbum = await userAlbumsService.rateAlbum(
      users.juanca._id,
      albums.finalFrontier._id,
      10,
    )
    expect(userAlbum).to.deep.equal({
      id: `${users.juanca._id}-${albums.finalFrontier._id}`,
      rating: 10,
      userId: users.juanca._id,
      albumId: albums.finalFrontier._id,
    })

    const album = await albumsService.loadAlbumById(albums.finalFrontier._id)
    expect(album.ratingsData).to.deep.equal({
      ratingsSum: 19,
      ratingsCount: 2,
      globalRating: 9.5,
    })

    const userAlbumsByBand = await userAlbumsService.loadByUserAndBand(
      users.juanca._id,
      bands.maiden._id,
    )
    expect(userAlbumsByBand).to.have.lengthOf(3)
  })

  it('should handle rating an album sending lower than 0 ratings', async () => {
    const userAlbum = await userAlbumsService.rateAlbum(
      users.juanca._id,
      albums.killers._id,
      -10,
    )
    expect(userAlbum).to.deep.equal({
      id: `${users.juanca._id}-${albums.killers._id}`,
      rating: 0,
      userId: users.juanca._id,
      albumId: albums.killers._id,
    })

    const album = await albumsService.loadAlbumById(albums.killers._id)
    expect(album.ratingsData).to.deep.equal({
      ratingsSum: 0,
      ratingsCount: 1,
      globalRating: 0,
    })
  })

  it('should handle rating an album sending greater than 10 ratings', async () => {
    const userAlbum = await userAlbumsService.rateAlbum(
      users.juanca._id,
      albums.killers._id,
      60,
    )
    expect(userAlbum).to.deep.equal({
      id: `${users.juanca._id}-${albums.killers._id}`,
      rating: 10,
      userId: users.juanca._id,
      albumId: albums.killers._id,
    })

    const album = await albumsService.loadAlbumById(albums.killers._id)
    expect(album.ratingsData).to.deep.equal({
      ratingsSum: 10,
      ratingsCount: 1,
      globalRating: 10,
    })
  })

  it('should handle removing an album rating when there is no rate to remove', async () => {
    const userAlbum = await userAlbumsService.removeAlbumRating(
      users.san._id,
      albums.powerslave._id,
    )
    expect(userAlbum).to.deep.equal({
      id: `${users.san._id}-${albums.powerslave._id}`,
      rating: null,
      userId: users.san._id,
      albumId: albums.powerslave._id,
    })
    const album = await albumsService.loadAlbumById(albums.powerslave._id)
    expect(album.ratingsData).to.deep.equal({
      ratingsSum: 58,
      ratingsCount: 7,
      globalRating: 8.28,
    })
  })

  it('should handle removing an album rating', async () => {
    const userAlbum = await userAlbumsService.removeAlbumRating(
      users.juanca._id,
      albums.powerslave._id,
    )
    expect(userAlbum).to.deep.equal({
      id: `${users.juanca._id}-${albums.powerslave._id}`,
      rating: null,
      userId: users.juanca._id,
      albumId: albums.powerslave._id,
    })

    const album = await albumsService.loadAlbumById(albums.powerslave._id)

    expect(album.ratingsData).to.deep.equal({
      ratingsSum: 48,
      ratingsCount: 6,
      globalRating: 8,
    })

    const esAlbum = await elasticSearchApi.getDocumentById(`album-${album._id}`)
    expect(esAlbum).to.deep.equal(expectedElasticSearchPowerslave)
  })
})
