import { expect } from 'chai'
import sinon from 'sinon'

import { testsSetup } from '../../tests.util'
import userBandsService from '../../../app/components/user-bands/user-bands.service'
import { users, bands, albums, userbands } from './users.service.fixtures'
import {
  expectedSavedBands,
  expectedRatedBands,
  expectedUserBandJuancaMaiden,
  expectedUserBandJuancaBruce,
  expectedUserBandRemoveJuancaMaiden,
  expectedUserBandRemoveSanMaiden,
  expectedMaidenWithUserData,
  expectedElasticSearchMaiden,
  expectedElasticSearchPrimalFear,
} from './users.service.snapshots'
import UserBand from '../../../app/components/user-bands/user-band'
import Band from '../../../app/components/bands/band'
import User from '../../../app/components/users/user'
import * as DataUtil from '../../../app/util/data.util'
import { sleep } from '../../../app/util/general.util'
import bandsService from '../../../app/components/bands/bands.service'
import elasticSearchApi from '../../../app/data/api/elasticsearch.api'
import activitiesDao from '../../../app/components/activities/activities.dao'

const sandbox = sinon.createSandbox()

describe('UserBandsService', () => {
  beforeEach(done => {
    sandbox.stub(DataUtil, 'needsSync').returns(false)
    testsSetup(__dirname + '/users.service.fixtures.js', done)
  })

  afterEach(() => {
    sandbox.restore()
  })

  it('should handle save band for later when the band was already saved', async () => {
    const userBand = await userBandsService.saveBandForLater(
      users.juanca._id,
      bands.maiden._id,
    )
    expect(userBand).to.containSubset(expectedUserBandJuancaMaiden)

    const savedBands = await userBandsService.loadSavedBands(users.juanca._id)
    expect(savedBands.length).to.equal(1)
  })

  it('should handle save band for later successfully', async () => {
    const userBand = await userBandsService.saveBandForLater(
      users.juanca._id,
      bands.bruce._id,
    )
    expect(userBand).to.containSubset(expectedUserBandJuancaBruce)

    const savedBands = await userBandsService.loadSavedBands(users.juanca._id)
    expect(savedBands.length).to.equal(2)
    expect(savedBands[0].bandId + '').to.deep.equal(bands.bruce._id + '')

    // assert activity
    await sleep(500)
    const activity = await activitiesDao.findOneLatest({
      actor: users.juanca._id,
      verb: 'add',
      objectId: bands.bruce._id,
    })
    expect(activity._id).not.to.be.undefined
    expect(activity.actor + '').to.deep.equal(users.juanca._id + '')
    expect(activity.verb).to.equal('add')
    expect('' + activity.objectId).to.deep.equal('' + bands.bruce._id)
    expect(activity.objectType).to.equal('band')
    expect(activity.target).to.equal('watchlist')
  })

  it('should save band for later, remove it and save it again without creating multiple records', async () => {
    await userBandsService.saveBandForLater(users.juanca._id, bands.bruce._id)
    await userBandsService.removeFromSavedBands(
      users.juanca._id,
      bands.bruce._id,
    )
    await userBandsService.saveBandForLater(users.juanca._id, bands.bruce._id)
    const result = await UserBand.find({
      user: users.juanca._id,
      band: bands.bruce._id,
    })
    expect(result.length).to.equal(1)
  })

  it('should handle remove from saved bands when the user-band combination is invalid', async () => {
    try {
      const savedBands = await userBandsService.removeFromSavedBands(
        users.juanca._id,
        bands.primalFear._id,
      )
    } catch (error) {
      expect(error.message).to.deep.equal('Invalid user or band')
    }
  })

  it('should handle removeFromSavedBands successfully', async () => {
    let userBand = await userBandsService.removeFromSavedBands(
      users.juanca._id,
      bands.maiden._id,
    )
    expect(userBand).to.containSubset(expectedUserBandRemoveJuancaMaiden)

    let savedBands = await userBandsService.loadSavedBands(users.juanca._id)
    expect(savedBands.length).to.equal(0)

    userBand = await userBandsService.removeFromSavedBands(
      users.san._id,
      bands.maiden._id,
    )

    expect(userBand).to.containSubset(expectedUserBandRemoveSanMaiden)

    savedBands = await userBandsService.loadSavedBands(users.san._id)
    expect(savedBands.length).to.equal(1)

    const megadethId = savedBands[0].bandId
    expect(megadethId + '').to.deep.equal(bands.megadeth._id + '')
  })

  it('should load user saved bands successfully', async () => {
    const savedBands = await userBandsService.loadSavedBands(users.san._id)
    expect(savedBands).to.containSubset(expectedSavedBands)
  })

  it('should rate a band and remove it from savedBands: band with no prior ratings data', async () => {
    const userBandData = await userBandsService.rateBand(
      users.juanca._id,
      bands.maiden._id,
      10,
    )
    const band = await bandsService.loadBandDetails(bands.maiden._id)
    expect(band.ratingsData).to.deep.equal({
      ratingsSum: 10,
      ratingsCount: 1,
      globalRating: 10,
    })

    expect(userBandData.rating).to.equal(10)

    const savedBands = await userBandsService.loadSavedBands(users.juanca._id)
    expect(savedBands).to.have.lengthOf(0)

    const esBand = await elasticSearchApi.getDocumentById(`band-${band._id}`)
    expect(esBand).to.deep.equal(expectedElasticSearchMaiden)
  })

  it('should rate a band: band with existing ratings data, new user rating', async () => {
    const userBandData = await userBandsService.rateBand(
      users.juanca._id,
      bands.primalFear._id,
      9,
    )
    const band = await bandsService.loadBandDetails(bands.primalFear._id)
    expect(band.ratingsData).to.deep.equal({
      ratingsSum: 67,
      ratingsCount: 8,
      globalRating: 8.375,
    })

    expect(userBandData.rating).to.equal(9)

    // assert activity
    await sleep(500)
    const activity = await activitiesDao.findOneLatest({
      actor: users.juanca._id,
      verb: 'rate',
      objectId: bands.primalFear._id,
    })
    expect(activity._id).not.to.be.undefined
    expect(activity.actor + '').to.deep.equal(users.juanca._id + '')
    expect(activity.verb).to.equal('rate')
    expect('' + activity.objectId).to.deep.equal('' + bands.primalFear._id)
    expect(activity.metadata.rating).to.equal(9)
    expect(activity.objectType).to.equal('band')
  })

  it('should rate a band: band with existing ratings data, updating existing user rating', async () => {
    const userBandData = await userBandsService.rateBand(
      users.san._id,
      bands.primalFear._id,
      10,
    )
    const band = await bandsService.loadBandDetails(bands.primalFear._id)
    expect(band.ratingsData).to.deep.equal({
      ratingsSum: 59,
      ratingsCount: 7,
      globalRating: 8.428571428571429,
    })

    expect(userBandData.rating).to.equal(10)
  })

  it('should handle rating a band sending lower than 0 ratings', async () => {
    const userBandData = await userBandsService.rateBand(
      users.juanca._id,
      bands.maiden._id,
      -10,
    )
    const band = await bandsService.loadBandDetails(bands.maiden._id)
    expect(band.ratingsData).to.deep.equal({
      ratingsSum: 0,
      ratingsCount: 1,
      globalRating: 0,
    })

    expect(userBandData.rating).to.equal(0)
  })

  it('should handle rating a band sending greater than 10 ratings', async () => {
    const userBandData = await userBandsService.rateBand(
      users.juanca._id,
      bands.maiden._id,
      60,
    )
    const band = await bandsService.loadBandDetails(bands.maiden._id)
    expect(band.ratingsData).to.deep.equal({
      ratingsSum: 10,
      ratingsCount: 1,
      globalRating: 10,
    })

    expect(userBandData.rating).to.equal(10)
  })

  it('should handle removing a band rating when there is no rate to remove', async () => {
    const userBand = await userBandsService.removeBandRating(
      users.juanca._id,
      bands.primalFear._id,
    )
    const band = await bandsService.loadBandDetails(bands.primalFear._id)
    expect(band.ratingsData).to.deep.equal({
      ratingsSum: 58,
      ratingsCount: 7,
      globalRating: 8.28,
    })

    expect(userBand.id).to.equal(`${users.juanca._id}-${bands.primalFear._id}`)
    expect(userBand.rating).to.equal(null)
    expect(userBand.saved).to.equal(false)
    expect(userBand.userId).to.equal(users.juanca._id)
    expect(userBand.bandId).to.equal(bands.primalFear._id)
  })

  it('should handle removing a band rating', async () => {
    const userBand = await userBandsService.removeBandRating(
      users.san._id,
      bands.primalFear._id,
    )
    const band = await bandsService.loadBandDetails(bands.primalFear._id)
    expect(band.ratingsData).to.deep.equal({
      ratingsSum: 49,
      ratingsCount: 6,
      globalRating: 8.166666666666666,
    })

    expect(userBand.id).to.equal(`${users.san._id}-${bands.primalFear._id}`)
    expect(userBand.rating).to.equal(null)
    expect(userBand.saved).to.equal(false)
    expect(userBand.userId).to.equal(users.san._id)
    expect(userBand.bandId).to.equal(bands.primalFear._id)

    const esBand = await elasticSearchApi.getDocumentById(`band-${band._id}`)
    expect(esBand).to.deep.equal(expectedElasticSearchPrimalFear)
  })

  it('should load rated bands', async () => {
    const ratedBands = await userBandsService.loadRatedBands(users.san._id)
    expect(ratedBands).to.containSubset(expectedRatedBands)
    expect(ratedBands[0].bandId + '').to.deep.equal(bands.bruce._id + '')
    expect(ratedBands[1].bandId + '').to.deep.equal(bands.primalFear._id + '')
  })

  it('should return band user data', async () => {
    let userBandData = await userBandsService.getUserBandData(
      users.san._id,
      bands.megadeth._id,
    )
    expect(userBandData.id).to.equal(`${users.san._id}-${bands.megadeth._id}`)
    expect(userBandData.rating).to.equal(null)
    expect(userBandData.saved).to.equal(true)
    expect(userBandData.userId).to.equal(users.san._id)
    expect(userBandData.bandId).to.equal(bands.megadeth._id)
    expect(userBandData.status).to.equal('saved')

    userBandData = await userBandsService.getUserBandData(
      users.san._id,
      bands.bruce._id,
    )
    expect(userBandData.id).to.equal(`${users.san._id}-${bands.bruce._id}`)
    expect(userBandData.rating).to.equal(10)
    expect(userBandData.saved).to.equal(false)
    expect(userBandData.status).to.equal('rated')
    expect(userBandData.userId).to.equal(users.san._id)
    expect(userBandData.bandId).to.equal(bands.bruce._id)
  })

  it('should return band user data when user has no data related to a specific band', async () => {
    const userBandData = await userBandsService.getUserBandData(
      users.san._id,
      bands.judas._id,
    )
    expect(userBandData.id).to.equal(`${users.san._id}-${bands.judas._id}`)
    expect(userBandData.rating).to.equal(null)
    expect(userBandData.saved).to.equal(false)
    expect(userBandData.userId).to.equal(users.san._id)
    expect(userBandData.bandId).to.equal(bands.judas._id)
  })
})
