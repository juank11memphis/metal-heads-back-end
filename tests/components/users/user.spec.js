import { expect } from 'chai'

import { testsSetup } from '../../tests.util'
import User from '../../../app/components/users/user'

describe('User Model', () => {
  beforeEach(done => {
    testsSetup(__dirname + '/users.service.fixtures.js', done)
  })

  it('should not save a user that has an already existing email', async () => {
    let user = new User({
      firstname: 'Jose',
      email: 'juank.memphis@gmail.com',
      password: 'test-password',
    })
    try {
      await user.save()
    } catch (error) {
      expect(error.message).to.equal(
        'User validation failed: email: Email already in use',
      )
    }
  })
})
