import {
  users,
  usersocialaccounts,
  userroles,
  bands,
  albums,
} from './users.service.fixtures'

export const expectedUserDetails = {
  _id: users.juanca._id,
  firstname: 'Juan',
  lastname: 'Morales',
  email: 'juank.memphis@gmail.com',
  password: '$2a$04$8mHfVd43uWR2WGG7wzbMb.GC2.WyOG1KjEMI7dgoElp1i8R5Qhniu',
  hasPassword: true,
  country: 'cr',
  socialAccounts: [
    {
      _id: usersocialaccounts.spotify._id,
      accountType: 'spotify',
    },
  ],
  roles: [
    {
      _id: userroles.admins._id,
      name: 'admins',
      permissions: ['create:quote', 'update:footer'],
    },
    {
      _id: userroles.metalGeeks._id,
      name: 'Metal Geeks',
      permissions: ['update:band', 'update:artist'],
    },
  ],
}

export const expectedUpdatedUser = {
  _id: users.juanca._id,
  firstname: 'Juan',
  lastname: 'Morales',
  email: 'juank.memphis@gmail.com',
  password: '$2a$04$8mHfVd43uWR2WGG7wzbMb.GC2.WyOG1KjEMI7dgoElp1i8R5Qhniu',
  hasPassword: true,
  country: 'nz',
  socialAccounts: null,
  roles: [],
}

export const expectedUserBandJuancaMaiden = {
  rating: null,
  saved: true,
  status: 'saved',
  userId: users.juanca._id,
  bandId: bands.maiden._id,
}

export const expectedUserBandJuancaBruce = {
  rating: null,
  saved: true,
  status: 'saved',
  bandId: bands.bruce._id,
  userId: users.juanca._id,
}

export const expectedUserBandRemoveJuancaMaiden = {
  rating: null,
  saved: false,
  status: 'removed',
  userId: users.juanca._id,
  bandId: bands.maiden._id,
}

export const expectedUserBandRemoveSanMaiden = {
  rating: null,
  saved: false,
  status: 'removed',
  userId: users.san._id,
  bandId: bands.maiden._id,
}

export const expectedSavedBands = [
  {
    rating: null,
    saved: true,
    status: 'saved',
    userId: users.san._id,
    bandId: bands.megadeth._id,
  },
  {
    rating: null,
    saved: true,
    status: 'saved',
    userId: users.san._id,
    bandId: bands.maiden._id,
  },
]

export const expectedRatedBands = [
  {
    rating: 10,
    saved: false,
    status: 'rated',
    userId: users.san._id,
    bandId: bands.bruce._id,
  },
  {
    rating: 9,
    saved: false,
    status: 'rated',
    userId: users.san._id,
    bandId: bands.primalFear._id,
  },
]

export const expectedJuancaMaidenAlbums = [
  {
    user: users.juanca._id,
    album: {
      _id: albums.powerslave._id,
      name: 'Powerslave',
      ratingsData: {
        ratingsSum: 58,
        ratingsCount: 7,
        globalRating: 8.28,
      },
    },
  },
  {
    user: users.juanca._id,
    album: {
      _id: albums.pieceOfMind._id,
      name: 'Piece of Mind',
      ratingsData: {
        ratingsSum: 40,
        ratingsCount: 10,
        globalRating: 10,
      },
    },
  },
]

export const expectedMaidenWithUserData = {
  dataExtracted: true,
  similarBandsExtracted: false,
  albums: [],
  currentMembers: [],
  _id: bands.maiden._id,
  name: 'Iron Maiden',
  metalArchivesId: '25',
  country: 'England',
  picture: 'a nice picture',
  userData: {
    saved: true,
    rating: null,
    status: 'saved',
  },
}

export const expectedAlbumsByUserAndBand = [
  {
    albumId: albums.powerslave._id,
    rating: 10,
  },
  {
    albumId: albums.pieceOfMind._id,
    rating: 8,
  },
  {
    albumId: albums.finalFrontier._id,
    rating: 8,
  },
]

export const expectedElasticSearchMaiden = {
  name: 'Iron Maiden',
  entityType: 'band',
  metadata: {
    id: bands.maiden._id + '',
    country: 'England',
    picture: 'a nice picture',
    globalRating: 10,
  },
}

export const expectedElasticSearchPrimalFear = {
  name: 'Primal Fear',
  entityType: 'band',
  metadata: {
    id: bands.primalFear._id + '',
    globalRating: 8.166666666666666,
  },
}

export const expectedElasticSearchKillers = {
  name: 'Killers',
  entityType: 'album',
  metadata: { id: albums.killers._id + '', globalRating: 9 },
}

export const expectedElasticSearchPowerslave = {
  name: 'Powerslave',
  entityType: 'album',
  metadata: { id: albums.powerslave._id + '', globalRating: 8 },
}
