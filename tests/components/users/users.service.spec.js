import { expect } from 'chai'
import bcrypt from 'bcryptjs'

import { testsSetup } from '../../tests.util'
import User from '../../../app/components/users/user'
import usersService from '../../../app/components/users/users.service'
import usersDao from '../../../app/components/users/users.dao'
import { users, bands } from './users.service.fixtures'
import {
  expectedUserDetails,
  expectedUpdatedUser,
} from './users.service.snapshots'

describe('UsersService', () => {
  beforeEach(done => {
    testsSetup(__dirname + '/users.service.fixtures.js', done)
  })

  it('should load users by ids', async () => {
    const songsLoaded = await usersService.loadByIds([
      users.juanca._id,
      users.san._id,
    ])
    expect(songsLoaded.length).to.equal(2)
  })

  it('should create an user', async () => {
    const user = await usersService.create({
      firstname: 'Juanca',
      email: 'juank.memphis2@gmail.com',
    })
    expect(user._id).not.to.be.undefined
    expect(user.firstname).to.equal('Juanca')
    expect(user.email).to.equal('juank.memphis2@gmail.com')
  })

  it('Should load user details by email', async () => {
    const user = await usersService.getDetailByEmail('juank.memphis@gmail.com')
    expect(user._id + '').to.deep.equal(users.juanca._id + '')
    expect(user.firstname).to.equal('Juan')
  })

  it('should load user details', async () => {
    const user = await usersService.loadUserDetails(users.juanca._id)
    expect(user).to.containSubset(expectedUserDetails)
  })

  it('should update user', async () => {
    const user = await usersService.updateUser(users.juanca._id, {
      firstname: 'Juan',
      country: 'nz',
      socialAccounts: null,
    })
    expect(user).to.containSubset(expectedUpdatedUser)
  })

  it('should change user password', async () => {
    const user = await usersService.changeUserPassword(
      users.juanca._id,
      'test-password',
      'new-password',
    )
    expect(bcrypt.compareSync('new-password', user.password)).to.equal(true)
  })

  it('should not change user password if current password is invalid', async () => {
    try {
      const user = await usersService.changeUserPassword(
        users.juanca._id,
        'test-password2',
        'new-password2',
      )
    } catch (error) {
      expect(error.message).to.equal('Your current password is not correct')
    }
  })

  it('should not change user password if new password is not valid', async () => {
    try {
      const user = await usersService.changeUserPassword(
        users.juanca._id,
        'test-password',
        'bad',
      )
    } catch (error) {
      expect(error.message).to.equal(
        'Validation failed: password: Password must be at least 6 characters long',
      )
    }
  })

  it('should disconnect user from spotify', async () => {
    const user = await usersService.disconnectUserFromSpotify(users.juanca._id)
    expect(user.socialAccounts.length).to.equal(0)
  })

  it('should add new social accounts', async () => {
    await usersService.addOrUpdateSocialAccount({
      userId: users.juanca._id,
      accountType: 'google',
      accessToken: 'access-token',
      refreshToken: 'refresh-token',
    })
    const user = await usersDao.getDetailById(users.juanca._id)
    expect(user.socialAccounts.length).to.equal(2)
    const socialAccount = usersDao.findSocialAccountByType(user, 'google')
    expect(socialAccount.accountType).to.equal('google')
  })

  it('should return null when getting social accounts and there are none', () => {
    const user = new User({ firstname: 'Juan' })
    const socialAccount = usersDao.findSocialAccountByType(user, 'spotify')
    expect(socialAccount).to.equal(null)
  })

  it('should return null when getting social accounts and there are none for the type we want', async () => {
    const user = await usersDao.getDetailByEmail('juank.memphis@gmail.com')
    const socialAccount = usersDao.findSocialAccountByType(user, 'google')
    expect(socialAccount).to.equal(null)
  })

  it('should remove a social account', async () => {
    let user = await usersDao.getDetailByEmail(
      'sandra.aguilar.delgado@gmail.com',
    )
    await usersDao.removeSocialAccount(user, 'google')
    user = await usersDao.getDetailByEmail('juank.memphis@gmail.com')
    expect(user.socialAccounts.length).to.equal(1)
    const socialAccount = usersDao.findSocialAccountByType(user, 'google')
    expect(socialAccount).to.equal(null)
  })
})
