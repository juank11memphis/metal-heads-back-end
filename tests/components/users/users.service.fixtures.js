const id = require('pow-mongodb-fixtures').createObjectId

const maidenId = id()
const bruceId = id()
const primalFearId = id()
const megadethId = id()

export const bands = {
  maiden: {
    _id: maidenId,
    name: 'Iron Maiden',
    metalArchivesId: '25',
    country: 'England',
    picture: 'a nice picture',
    dataExtracted: true,
  },
  primalFear: {
    _id: primalFearId,
    name: 'Primal Fear',
    ratingsData: {
      ratingsSum: 58,
      ratingsCount: 7,
      globalRating: 8.28,
    },
    dataExtracted: true,
  },
  bruce: {
    _id: bruceId,
    name: 'Bruce Dickinson',
    similarBands: [maidenId, primalFearId],
    dataExtracted: true,
  },
  megadeth: {
    _id: megadethId,
    name: 'Megadeth',
    dataExtracted: true,
  },
  judas: {
    _id: id(),
    name: 'Judas Priest',
    dataExtracted: true,
  },
}

export const albums = {
  powerslave: {
    _id: id(),
    name: 'Powerslave',
    band: bands.maiden._id,
    dataExtracted: true,
    ratingsData: {
      ratingsSum: 58,
      ratingsCount: 7,
      globalRating: 8.28,
    },
  },
  pieceOfMind: {
    _id: id(),
    name: 'Piece of Mind',
    band: bands.maiden._id,
    dataExtracted: true,
    ratingsData: {
      ratingsSum: 40,
      ratingsCount: 10,
      globalRating: 10,
    },
  },
  killers: {
    _id: id(),
    name: 'Killers',
    band: bands.maiden._id,
    dataExtracted: true,
  },
  bookOfSoulds: {
    _id: id(),
    name: 'Book of Soulds',
    band: bands.maiden._id,
    dataExtracted: true,
    ratingsData: {
      ratingsSum: 35,
      ratingsCount: 4,
      globalRating: 8.8,
    },
  },
  finalFrontier: {
    _id: id(),
    name: 'The Final Frontier',
    dataExtracted: true,
    band: bands.maiden._id,
    ratingsData: {
      ratingsSum: 17,
      ratingsCount: 2,
      globalRating: 8.5,
    },
  },
}

export const usersocialaccounts = {
  spotify: {
    _id: id(),
    accountType: 'spotify',
    accessToken: 'access-token',
    refreshToken: 'refresh-token',
  },
  google: {
    _id: id(),
    accountType: 'google',
    accessToken: 'access-token',
    refreshToken: 'refresh-token',
  },
}

export const userroles = {
  admins: {
    _id: id(),
    name: 'admins',
    permissions: ['create:quote', 'update:footer'],
  },
  metalGeeks: {
    _id: id(),
    name: 'Metal Geeks',
    permissions: ['update:band', 'update:artist'],
  },
}

export const users = {
  juanca: {
    _id: id(),
    firstname: 'Juan',
    lastname: 'Morales',
    email: 'juank.memphis@gmail.com',
    password: '$2a$04$8mHfVd43uWR2WGG7wzbMb.GC2.WyOG1KjEMI7dgoElp1i8R5Qhniu',
    hasPassword: true,
    country: 'cr',
    socialAccounts: [usersocialaccounts.spotify._id],
    roles: [userroles.admins._id, userroles.metalGeeks._id],
  },
  san: {
    _id: id(),
    firstname: 'Sandra',
    lastname: 'Aguilar',
    email: 'sandra.aguilar.delgado@gmail.com',
    password: '$2a$04$8mHfVd43uWR2WGG7wzbMb.GC2.WyOG1KjEMI7dgoElp1i8R5Qhniu',
    hasPassword: true,
    country: 'cr',
    socialAccounts: [
      usersocialaccounts.spotify._id,
      usersocialaccounts.google._id,
    ],
  },
  jose: {
    _id: id(),
    firstname: 'Jose',
    lastname: 'Morales',
    email: 'joseinf10@gmail.com',
  },
}

export const userbands = {
  juankMaiden: {
    user: users.juanca._id,
    band: bands.maiden._id,
    status: 'saved',
    updatedAt: '2018-01-25T02:00:11.959Z',
  },
  sanMaiden: {
    user: users.san._id,
    band: bands.maiden._id,
    status: 'saved',
    updatedAt: '2018-01-25T02:00:11.959Z',
  },
  sanMegadeth: {
    user: users.san._id,
    band: bands.megadeth._id,
    status: 'saved',
    updatedAt: '2018-01-25T04:00:11.959Z',
  },
  sanPrimalFear: {
    user: users.san._id,
    band: bands.primalFear._id,
    rating: 9,
    status: 'rated',
  },
  sanBruce: {
    user: users.san._id,
    band: bands.bruce._id,
    rating: 10,
    status: 'rated',
  },
}

export const useralbums = {
  juancaMaidenPowerslave: {
    user: users.juanca._id,
    band: bands.maiden._id,
    album: albums.powerslave._id,
    rating: 10,
  },
  juancaMaidenPieceOfMind: {
    user: users.juanca._id,
    band: bands.maiden._id,
    album: albums.pieceOfMind._id,
    rating: 8,
  },
  juancaMaidenFinalFrontier: {
    user: users.juanca._id,
    band: bands.maiden._id,
    album: albums.finalFrontier._id,
    rating: 8,
  },
}
