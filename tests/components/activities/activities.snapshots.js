import { users, bands, albums } from './activities.fixtures'

export const expectedRateBandActivity = {
  actor: users.juanca._id,
  verb: 'rate',
  objectId: '' + bands.maiden._id,
  objectType: 'band',
  metadata: {
    rating: 10,
  },
}

export const expectedRateAlbumActivity = {
  actor: users.juanca._id,
  verb: 'rate',
  objectId: '' + albums.powerslave._id,
  objectType: 'album',
  metadata: {
    rating: 9,
  },
}

export const expectedBandAddedToWatchlistActivity = {
  actor: users.juanca._id,
  verb: 'add',
  objectId: '' + bands.maiden._id,
  objectType: 'band',
  target: 'watchlist',
}

export const expectedPlayAlbumActivity = {
  actor: users.juanca._id,
  verb: 'play',
  objectId: '' + albums.powerslave._id,
  objectType: 'album',
  target: 'youtube',
}
