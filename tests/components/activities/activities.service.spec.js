import { expect } from 'chai'

import { testsSetup } from '../../tests.util'
import { users, bands, albums } from './activities.fixtures'
import {
  expectedRateBandActivity,
  expectedRateAlbumActivity,
  expectedBandAddedToWatchlistActivity,
  expectedPlayAlbumActivity,
} from './activities.snapshots'
import activitiesService from '../../../app/components/activities/activities.service'
import bandsDao from '../../../app/components/bands/bands.dao'
import albumsDao from '../../../app/components/albums/albums.dao'

describe('Activities Service', () => {
  beforeEach(done => {
    testsSetup(__dirname + '/activities.fixtures.js', done)
  })

  it('should create a rate band activity', async () => {
    const band = await bandsDao.getDetail(bands.maiden._id)
    const newActivity = await activitiesService.createRateBandActivity(
      users.juanca._id,
      band,
      10,
    )
    expect(newActivity).to.containSubset(expectedRateBandActivity)
  })

  it('should create a rate album activity', async () => {
    const album = await albumsDao.getDetail(albums.powerslave._id)
    const newActivity = await activitiesService.createRateAlbumActivity(
      users.juanca._id,
      album,
      9,
    )
    expect(newActivity).to.containSubset(expectedRateAlbumActivity)
  })

  it('should create a band added to watchlist activity', async () => {
    const newActivity = await activitiesService.createBandAddedToWatchlistActivity(
      users.juanca._id,
      bands.maiden._id,
    )
    expect(newActivity).to.containSubset(expectedBandAddedToWatchlistActivity)
  })

  it('should create a play album activity', async () => {
    const newActivity = await activitiesService.createPlayAlbumActivity(
      users.juanca._id,
      albums.powerslave._id,
      'youtube',
    )
    expect(newActivity).to.containSubset(expectedPlayAlbumActivity)
  })

  it('should load latest rate band activities', async () => {
    const activities = await activitiesService.getLatestBandRatings(3)
    expect(activities.length).to.equal(3)
    expect(activities[0].objectId).to.equal('5')
    expect(activities[1].objectId).to.equal('3')
    expect(activities[2].objectId).to.equal('2')
  })

  it('should load latest rate album activities', async () => {
    const activities = await activitiesService.getLatestAlbumRatings(3)
    expect(activities.length).to.equal(3)
    expect(activities[0].objectId).to.equal('5')
    expect(activities[1].objectId).to.equal('3')
    expect(activities[2].objectId).to.equal('2')
  })

  it('should load latest music played activities', async () => {
    const activities = await activitiesService.getLatestMusicPlayed(3)
    expect(activities.length).to.equal(3)
    expect(activities[0].objectId).to.equal('1')
    expect(activities[1].objectId).to.equal('5')
    expect(activities[2].objectId).to.equal('3')
  })
})
