const id = require('pow-mongodb-fixtures').createObjectId

export const users = {
  juanca: {
    _id: id(),
    firstname: 'Juan',
    lastname: 'Morales',
    email: 'juank.memphis@gmail.com',
  },
  san: {
    _id: id(),
    firstname: 'Sandra',
    lastname: 'Aguilar',
    email: 'san@gmail.com',
  },
}

export const bands = {
  maiden: {
    _id: id(),
    name: 'Iron Maiden',
    metalArchivesId: '25',
    ratingsData: {
      ratingsCount: 4,
      globalRating: 10,
    },
  },
}

export const albums = {
  powerslave: {
    _id: id(),
    name: 'Powerslave',
    metalArchivesId: '77',
    dataExtracted: true,
  },
}

export const activities = {
  rateBand1: {
    actor: users.juanca._id,
    verb: 'rate',
    objectId: '1',
    objectType: 'band',
    createdAt: '2018-04-22T18:11:16.367Z',
  },
  rateBand2: {
    actor: users.juanca._id,
    verb: 'rate',
    objectId: '2',
    objectType: 'band',
    createdAt: '2018-04-26T18:11:16.367Z',
  },
  rateBand3: {
    actor: users.juanca._id,
    verb: 'rate',
    objectId: '3',
    objectType: 'band',
    createdAt: '2018-04-28T18:11:16.367Z',
  },
  rateBand4: {
    actor: users.juanca._id,
    verb: 'rate',
    objectId: '4',
    objectType: 'band',
    createdAt: '2018-04-20T18:11:16.367Z',
  },
  rateBand5: {
    actor: users.juanca._id,
    verb: 'rate',
    objectId: '5',
    objectType: 'band',
    createdAt: '2018-04-29T18:11:16.367Z',
  },
  rateAlbum1: {
    actor: users.juanca._id,
    verb: 'rate',
    objectId: '1',
    objectType: 'album',
    createdAt: '2018-04-22T18:11:16.367Z',
  },
  rateAlbum2: {
    actor: users.juanca._id,
    verb: 'rate',
    objectId: '2',
    objectType: 'album',
    createdAt: '2018-04-26T18:11:16.367Z',
  },
  rateAlbum3: {
    actor: users.juanca._id,
    verb: 'rate',
    objectId: '3',
    objectType: 'album',
    createdAt: '2018-04-28T18:11:16.367Z',
  },
  rateAlbum4: {
    actor: users.juanca._id,
    verb: 'rate',
    objectId: '4',
    objectType: 'album',
    createdAt: '2018-04-20T18:11:16.367Z',
  },
  rateAlbum5: {
    actor: users.juanca._id,
    verb: 'rate',
    objectId: '5',
    objectType: 'album',
    createdAt: '2018-04-29T18:11:16.367Z',
  },
  musicPlayed1: {
    actor: users.juanca._id,
    verb: 'play',
    objectId: '1',
    objectType: 'album',
    target: 'spotify',
    createdAt: '2018-04-30T18:11:16.367Z',
  },
  musicPlayed2: {
    actor: users.juanca._id,
    verb: 'play',
    objectId: '2',
    objectType: 'album',
    target: 'youtube',
    createdAt: '2018-04-22T18:11:16.367Z',
  },
  musicPlayed3: {
    actor: users.juanca._id,
    verb: 'play',
    objectId: '3',
    objectType: 'album',
    target: 'spotify',
    createdAt: '2018-04-28T18:11:16.367Z',
  },
  musicPlayed4: {
    actor: users.juanca._id,
    verb: 'play',
    objectId: '4',
    objectType: 'album',
    target: 'spotify',
    createdAt: '2018-04-20T18:11:16.367Z',
  },
  musicPlayed5: {
    actor: users.juanca._id,
    verb: 'play',
    objectId: '5',
    objectType: 'album',
    target: 'youtube',
    createdAt: '2018-04-29T18:11:16.367Z',
  },
}
