import { expect } from 'chai'

import { testsSetup } from '../../tests.util'
import { users, albums } from './activities.fixtures'
import activitiesDao from '../../../app/components/activities/activities.dao'

describe('Activities DAO', () => {
  beforeEach(done => {
    testsSetup(__dirname + '/activities.fixtures.js', done)
  })

  it('should create a new activity', async () => {
    const data = {
      actor: users.juanca._id,
      verb: 'rate',
      objectId: albums.powerslave._id,
      objectType: 'band',
      metadata: {
        rating: 10,
      },
    }
    const newActivity = await activitiesDao.create(data)
    const savedActivity = await activitiesDao.findOneLatest({
      actor: users.juanca._id,
      verb: 'rate',
      objectId: albums.powerslave._id,
    })
    expect(savedActivity._id).not.to.be.undefined
    expect(savedActivity.actor + '').to.equal(users.juanca._id + '')
    expect(savedActivity.objectType).to.deep.equal('band')
    expect(savedActivity.metadata.rating).to.equal(10)
  })

  it('Should find a single latest activity for a specific query', async () => {
    await activitiesDao.create({
      actor: users.juanca._id,
      verb: 'rate',
    })
    await activitiesDao.create({
      actor: users.san._id,
      verb: 'rate',
    })
    const latestActivity = await activitiesDao.findOneLatest({
      verb: 'rate',
    })
    expect(latestActivity.actor + '').to.deep.equal(users.san._id + '')
  })

  it('Should not create duplicate activitiest', async () => {
    const data = {
      actor: users.juanca._id,
      verb: 'rate',
      objectId: '1',
      objectType: 'band',
    }
    let currentActivities = await activitiesDao.findLatest(data)
    expect(currentActivities.length).to.equal(1)
    const newData = {
      ...data,
      metadata: {
        rating: 10,
      },
    }
    await activitiesDao.create(newData)
    currentActivities = await activitiesDao.findLatest(data)
    expect(currentActivities.length).to.equal(1)
  })
})
