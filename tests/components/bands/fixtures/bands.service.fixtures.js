const id = require('pow-mongodb-fixtures').createObjectId

const maidenId = id()
const bruceId = id()

export const bands = {
  maiden: {
    _id: maidenId,
    name: 'Iron Maiden',
    metalArchivesId: '25',
    ratingsData: { ratingsCount: 4, globalRating: 10 },
  },
  bruce: {
    _id: bruceId,
    name: 'Bruce Dickinson',
    metalArchivesId: '295',
    ratingsData: { ratingsCount: 1, globalRating: 9 },
    dataExtracted: true,
  },
  judas: {
    _id: id(),
    name: 'Judas Priest',
    metalArchivesId: '97',
    ratingsData: { ratingsCount: 3, globalRating: 9 },
    similarBands: [maidenId, bruceId],
    similarBandsExtracted: true,
    dataExtracted: true,
    lastExtractionDate: new Date(),
  },
  dio: {
    _id: id(),
    name: 'Dio',
    metalArchivesId: '163',
    ratingsData: { ratingsCount: 2, globalRating: 8 },
  },
  saxon: {
    _id: id(),
    name: 'Saxon',
    metalArchivesId: '626',
    ratingsData: { ratingsCount: 1, globalRating: 7 },
  },
  angelWitch: {
    _id: id(),
    name: 'Angel Witch',
    metalArchivesId: '537',
    ratingsData: { ratingsCount: 1, globalRating: 5 },
  },
  apnr: {
    _id: id(),
    name: 'Ария',
    metalArchivesId: '272',
    ratingsData: { ratingsCount: 2, globalRating: 4 },
  },
  helloween: {
    _id: id(),
    name: 'Helloween',
    metalArchivesId: '159',
    ratingsData: { ratingsCount: 3, globalRating: 3 },
  },
  diamondHead: {
    _id: id(),
    name: 'Diamond Head',
    metalArchivesId: '401',
    ratingsData: { ratingsCount: 1, globalRating: 2 },
  },
  satan: {
    _id: id(),
    name: 'Satan',
    metalArchivesId: '774',
    ratingsData: { ratingsCount: 1, globalRating: 1 },
  },
  grimReaper: {
    _id: id(),
    name: 'Grim Reaper',
    metalArchivesId: '952',
    ratingsData: { ratingsCount: 2, globalRating: 6 },
  },
  megadeth: {
    _id: id(),
    name: 'Megadeth',
    metalArchivesId: '138',
  },
  primalFear: {
    _id: id(),
    name: 'Primal Fear',
    metalArchivesId: '1029',
  },
  warcry: {
    _id: id(),
    name: 'Warcry',
    metalArchivesId: '5917',
  },
  maidenOfMars: {
    _id: id(),
    name: 'Maiden of Mars',
    metalArchivesId: '3540391115',
  },
  maidens: {
    _id: id(),
    name: 'Maidens',
    metalArchivesId: '3540359694',
  },
}

export const users = {
  san: {
    _id: id(),
    firstname: 'Sandra',
    lastname: 'Aguilar',
    email: 'sandra.aguilar.delgado@gmail.com',
  },
}

export const userbands = {
  sanPrimalFear: {
    user: users.san._id,
    band: bands.primalFear._id,
    rating: 9,
    status: 'rated',
  },
  sanBruce: {
    user: users.san._id,
    band: bands.bruce._id,
    rating: 10,
    status: 'rated',
  },
}
