import { bands } from '../fixtures/bands.service.fixtures'

export const expectedMostRatedBandsData = [
  {
    dataExtracted: false,
    similarBandsExtracted: false,
    albums: [],
    currentMembers: [],
    similarBands: [],
    _id: bands.maiden._id,
    name: 'Iron Maiden',
    metalArchivesId: '25',
    ratingsData: { ratingsCount: 4, globalRating: 10 },
  },
  {
    dataExtracted: true,
    similarBandsExtracted: true,
    albums: [],
    currentMembers: [],
    similarBands: [bands.maiden._id, bands.bruce._id],
    _id: bands.judas._id,
    name: 'Judas Priest',
    metalArchivesId: '97',
    ratingsData: { ratingsCount: 3, globalRating: 9 },
  },
  {
    dataExtracted: false,
    similarBandsExtracted: false,
    albums: [],
    currentMembers: [],
    similarBands: [],
    _id: bands.helloween._id,
    name: 'Helloween',
    metalArchivesId: '159',
    ratingsData: { ratingsCount: 3, globalRating: 3 },
  },
  {
    dataExtracted: false,
    similarBandsExtracted: false,
    albums: [],
    currentMembers: [],
    similarBands: [],
    _id: bands.dio._id,
    name: 'Dio',
    metalArchivesId: '163',
    ratingsData: { ratingsCount: 2, globalRating: 8 },
  },
  {
    dataExtracted: false,
    similarBandsExtracted: false,
    albums: [],
    currentMembers: [],
    similarBands: [],
    _id: bands.grimReaper._id,
    name: 'Grim Reaper',
    metalArchivesId: '952',
    ratingsData: { ratingsCount: 2, globalRating: 6 },
  },
  {
    dataExtracted: false,
    similarBandsExtracted: false,
    albums: [],
    currentMembers: [],
    similarBands: [],
    _id: bands.apnr._id,
    name: 'Ария',
    metalArchivesId: '272',
    ratingsData: { ratingsCount: 2, globalRating: 4 },
  },
  {
    dataExtracted: true,
    similarBandsExtracted: false,
    albums: [],
    currentMembers: [],
    similarBands: [],
    _id: bands.bruce._id,
    name: 'Bruce Dickinson',
    metalArchivesId: '295',
    ratingsData: { ratingsCount: 1, globalRating: 9 },
  },
  {
    dataExtracted: false,
    similarBandsExtracted: false,
    albums: [],
    currentMembers: [],
    similarBands: [],
    _id: bands.saxon._id,
    name: 'Saxon',
    metalArchivesId: '626',
    ratingsData: { ratingsCount: 1, globalRating: 7 },
  },
  {
    dataExtracted: false,
    similarBandsExtracted: false,
    albums: [],
    currentMembers: [],
    similarBands: [],
    _id: bands.angelWitch._id,
    name: 'Angel Witch',
    metalArchivesId: '537',
    ratingsData: { ratingsCount: 1, globalRating: 5 },
  },
  {
    dataExtracted: false,
    similarBandsExtracted: false,
    albums: [],
    currentMembers: [],
    similarBands: [],
    _id: bands.diamondHead._id,
    name: 'Diamond Head',
    metalArchivesId: '401',
    ratingsData: { ratingsCount: 1, globalRating: 2 },
  },
]

export const expectedSearchBandsData = [
  {
    _id: bands.maidens._id,
    name: 'Maidens',
    metalArchivesId: '3540359694',
    similarBands: [],
    currentMembers: [],
    albums: [],
    similarBandsExtracted: false,
    dataExtracted: false,
  },
  {
    _id: bands.maiden._id,
    name: 'Iron Maiden',
    metalArchivesId: '25',
    similarBands: [],
    currentMembers: [],
    albums: [],
    similarBandsExtracted: false,
    dataExtracted: false,
  },
  {
    _id: bands.maidenOfMars._id,
    name: 'Maiden of Mars',
    metalArchivesId: '3540391115',
    similarBands: [],
    currentMembers: [],
    albums: [],
    similarBandsExtracted: false,
    dataExtracted: false,
  },
]

export const expectedSimilarBandsData = [
  {
    _id: bands.maiden._id,
    name: 'Iron Maiden',
    metalArchivesId: '25',
    similarBands: [],
    currentMembers: [],
    albums: [],
    similarBandsExtracted: false,
    dataExtracted: false,
  },
  {
    _id: bands.bruce._id,
    name: 'Bruce Dickinson',
    metalArchivesId: '295',
    similarBands: [],
    currentMembers: [],
    albums: [],
    similarBandsExtracted: false,
    dataExtracted: true,
  },
]

export const expectedJudasDetails = {
  _id: bands.judas._id,
  name: 'Judas Priest',
  metalArchivesId: '97',
  currentMembers: [],
  albums: [],
  similarBandsExtracted: true,
  dataExtracted: true,
}

export const expectedMaidenDetails = {
  name: 'Iron Maiden',
  metalArchivesId: '25',
  picture: './iron-maiden_files/25_photo.jpg',
  genre: 'Heavy Metal, NWOBHM',
  location: 'London, England',
  status: 'Active',
  lyricalThemes: 'History, Literature, War, Mythology, Society, Religion',
  yearsActive: '1975-present',
  formedIn: '1975',
  similarBandsExtracted: false,
  dataExtracted: true,
}

export const expectedBruceDetails = {
  _id: bands.bruce._id,
  name: 'Bruce Dickinson',
  metalArchivesId: '295',
  currentMembers: [],
  albums: [],
  similarBandsExtracted: false,
  dataExtracted: true,
}
