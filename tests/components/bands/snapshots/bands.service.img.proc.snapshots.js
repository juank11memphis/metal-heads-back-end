import {
  bands,
  albums,
  artists,
} from '../fixtures/bands.service.img.proc.fixtures'

export const expectedMaiden = {
  ...bands.maiden,
  picture: `http://metalheads.imgix.net/bands/${bands.maiden._id}_picture.jpg`,
}

export const expectedAlbums = [
  {
    ...albums.powerslave,
    picture: `http://metalheads.imgix.net/albums/${
      albums.powerslave._id
    }_picture.jpg`,
  },
  {
    ...albums.bookOfSouls,
    picture: `http://metalheads.imgix.net/albums/${
      albums.bookOfSouls._id
    }_picture.jpg`,
  },
  {
    ...albums.killers,
    picture: 'http://metalheads.imgix.net/killers.png',
  },
]

export const expectedArtists = [
  {
    ...artists.harris,
    picture: `http://metalheads.imgix.net/artists/${
      artists.harris._id
    }_picture.jpg`,
  },
  {
    ...artists.bruce,
    picture: `http://metalheads.imgix.net/artists/${
      artists.bruce._id
    }_picture.jpg`,
  },
  {
    ...artists.nicko,
    picture: 'http://metalheads.imgix.net/nicko.png',
  },
]
