import { expect } from 'chai'

import { testsSetup } from '../../tests.util'
import bandsDao from '../../../app/components/bands/bands.dao'
import elasticSearchApi from '../../../app/data/api/elasticsearch.api'

describe('BandsDao', () => {
  beforeEach(done => {
    testsSetup(__dirname + '/fixtures/bands.service.fixtures.js', done)
  })

  it('should insert all bands into elasticsearch', async () => {
    await bandsDao.storeAllInElasticsearch()
    const esBands = await elasticSearchApi.searchByEntityType('band')
    expect(esBands.length).to.equal(10)
  })

  it('should find bands with limit', async () => {
    const bands = await bandsDao.find({}, 2)
    expect(bands.length).to.equal(2)
  })
})
