import { expect } from 'chai'
import sinon from 'sinon'

import { testsSetup } from '../../tests.util'
import bandsService from '../../../app/components/bands/bands.service'
import {
  bands,
  users,
  maidenId,
  bruceId,
} from './fixtures/bands.service.fixtures'
import {
  expectedMostRatedBandsData,
  expectedSimilarBandsData,
  expectedJudasDetails,
  expectedMaidenDetails,
  expectedBruceDetails,
} from './snapshots/bands.service.snapshots'
import { BandSimilarBandsETL } from '../../../app/data/etl/bands/band-similar-bands.etl'
import { BandETL } from '../../../app/data/etl/bands/band.etl'
import { expectedSimilarBandsEtlData } from '../../data/etl/bands/snapshots/band-similar-bands.etl.snapshots'
import { expectedBandEtlData } from '../../data/etl/bands/snapshots/band.etl.snapshots'
import ImagesToProcess from '../../../app/components/images/images-to-process'
import Band from '../../../app/components/bands/band'
import { generateToken } from '../../../app/util/auth.util'

const sandbox = sinon.createSandbox()

describe('BandsService', () => {
  beforeEach(done => {
    sandbox
      .stub(BandSimilarBandsETL.prototype, 'start')
      .returns(expectedSimilarBandsEtlData)
    sandbox.stub(BandETL.prototype, 'start').returns(expectedBandEtlData())
    testsSetup(__dirname + '/fixtures/bands.service.fixtures.js', done)
  })

  afterEach(() => {
    sandbox.restore()
  })

  it('should load most rated bands', async () => {
    const mostRatedBands = await bandsService.loadMostRatedBands()
    expect(mostRatedBands).to.containSubset(expectedMostRatedBandsData)
    expect(mostRatedBands[0].name).to.deep.equal('Iron Maiden')
    expect(mostRatedBands[1].name).to.deep.equal('Judas Priest')
    expect(mostRatedBands[2].name).to.deep.equal('Helloween')
    expect(mostRatedBands[3].name).to.deep.equal('Dio')
    expect(mostRatedBands[4].name).to.deep.equal('Grim Reaper')
    expect(mostRatedBands[5].name).to.deep.equal('Ария')
    expect(mostRatedBands[6].name).to.deep.equal('Bruce Dickinson')
    expect(mostRatedBands[7].name).to.deep.equal('Saxon')
    expect(mostRatedBands[8].name).to.deep.equal('Angel Witch')
    expect(mostRatedBands[9].name).to.deep.equal('Diamond Head')
  })

  it('should return similar bands when they have been already extracted', async () => {
    const similarBands = await bandsService.loadSimilarBands(bands.judas._id)
    expect(similarBands).to.containSubset(expectedSimilarBandsData)
  })

  it('should return similar bands when they have not been extracted yet', async () => {
    const similarBands = await bandsService.loadSimilarBands(bands.maiden._id)
    expect(similarBands).to.containSubset(expectedSimilarBandsEtlData)
  })

  it('should return band details when they have been already extracted', async () => {
    const judasDetails = await bandsService.loadBandDetails(bands.judas._id)
    expect(judasDetails).to.containSubset(expectedJudasDetails)
  })

  it('should return band details when they have not been extracted yet', async () => {
    const maidenDetails = await bandsService.loadBandDetails(bands.maiden._id)
    expect(maidenDetails).to.containSubset(expectedMaidenDetails)
    const imagesToProcess = await ImagesToProcess.find({})
    expect(imagesToProcess.length).to.equal(1)
  })

  it('should return band details when they need data sync', async () => {
    const bruceDetails = await bandsService.loadBandDetails(bands.bruce._id)
    expect(bruceDetails).to.containSubset(expectedBruceDetails)
    const imagesToProcess = await ImagesToProcess.find({})
    expect(imagesToProcess.length).to.equal(1)
  })

  it('should load bands by ids', async () => {
    const bandsLoaded = await bandsService.loadByIds([
      bands.judas._id,
      bands.maiden._id,
    ])
    expect(bandsLoaded.length).to.equal(2)
  })
})
