import { expect } from 'chai'
import request from 'request'
import sinon from 'sinon'

import { testsSetup } from '../../tests.util'
import ImagesToProcess from '../../../app/components/images/images-to-process'
import { ImagesToProcessDao } from '../../../app/components/images/images-to-process.dao'
import imagesToProcessService from '../../../app/components/images/images-to-process.service'
import Band from '../../../app/components/bands/band'
import Album from '../../../app/components/albums/album'
import Artist from '../../../app/components/artists/artist'
import {
  expectedMaiden,
  expectedAlbums,
  expectedArtists,
} from './snapshots/bands.service.img.proc.snapshots'

describe('ImagesToProcess Service Images Processing', () => {
  beforeEach(done => {
    sinon.stub(request, 'get').yields(null, { statusCode: 200 }, true)
    testsSetup(__dirname + '/fixtures/bands.service.img.proc.fixtures.js', done)
  })

  afterEach(() => {
    request.get.restore()
  })

  it('should run images processing job successfully', async () => {
    const result = await imagesToProcessService.processImages()
    expect(result).to.equal(true)
    const imagesToProcess = await ImagesToProcess.find()
    expect(imagesToProcess.length).to.equal(0)

    // Asserts band picture
    const dbMaiden = await Band.findOne({ name: 'Iron Maiden' })
    expect(dbMaiden).to.containSubset(expectedMaiden)

    // Asserts albums pictures
    const dbAlbums = await Album.find({})
    expect(dbAlbums).to.containSubset(expectedAlbums)

    // Asserts artists pictures
    const dbArtists = await Artist.find({})
    expect(dbArtists).to.containSubset(expectedArtists)
  })

  it('should run images processing job successfully when there are no images to process', async () => {
    const stub = sinon
      .stub(ImagesToProcessDao.prototype, 'findLatestAdded')
      .returns(null)
    const result = await imagesToProcessService.processImages()
    expect(result).to.equal(false)
    stub.restore()
  })
})
