import { quotes } from './quotes.service.fixtures'

export const expectedActiveQuote = {
  _id: quotes.first._id,
  text: 'Real music is not on the radio',
  author: 'Ozzy Ozbourne',
  quoteIndex: 0,
  status: 'active',
}

export const expectedNewQuote = {
  text: 'a new quote 1',
  author: 'a new author 1',
  quoteIndex: 0,
  status: 'active',
}

export const expectedNewQuote2 = {
  text: 'a new quote 2',
  author: 'Unknown',
  quoteIndex: 3,
  status: 'inactive',
}

export const expectedOldActiveQuote = {
  _id: quotes.first._id,
  text: 'Real music is not on the radio',
  quoteIndex: 0,
  status: 'inactive',
  author: 'Ozzy Ozbourne',
}

export const expectedNewActiveQuote = {
  _id: quotes.second._id,
  text: 'Keep calm and listen to metal',
  quoteIndex: 1,
  status: 'active',
  author: 'Unknown',
}
