import { expect } from 'chai'
import sinon from 'sinon'

import { testsSetup } from '../../tests.util'
import quotesService from '../../../app/components/quotes/quotes.service'
import { users } from './quotes.service.fixtures'
import {
  expectedActiveQuote,
  expectedNewQuote,
  expectedNewQuote2,
  expectedOldActiveQuote,
  expectedNewActiveQuote,
} from './quotes.service.snapshots'
import Quote from '../../../app/components/quotes/quote'
import { generateToken } from '../../../app/util/auth.util'
import User from '../../../app/components/users/user'
import quotesDao, { QuotesDao } from '../../../app/components/quotes/quotes.dao'

describe('QuotesService', () => {
  beforeEach(done => {
    testsSetup(__dirname + '/quotes.service.fixtures.js', done)
  })

  it('should find active quote', async () => {
    const activeQuote = await quotesService.loadActiveQuote()
    expect(activeQuote).to.containSubset(expectedActiveQuote)
  })

  it('should create a new quote when no quotes exist', async () => {
    await Quote.remove({})
    const newQuote = await quotesService.createQuote({
      text: 'a new quote 1',
      author: 'a new author 1',
    })
    expect(newQuote).to.containSubset(expectedNewQuote)
  })

  it('should create a new quote when quotes already exist', async () => {
    const newQuote = await quotesService.createQuote({
      text: 'a new quote 2',
    })
    expect(newQuote).to.containSubset(expectedNewQuote2)
  })

  it('should throw an error when creating a duplicate quote', async () => {
    const newQuote = await quotesService.createQuote({
      text: 'a new quote 2',
    })
    try {
      const newQuote = await quotesService.createQuote({
        text: 'a new quote 2',
      })
    } catch (error) {
      expect(error.message).to.equal('Quote already exists')
    }
  })

  it('should return null on setNextActiveQuote if there are no quotes on the db', async () => {
    await Quote.remove({})
    const nextActiveQuote = await quotesService.setNextActiveQuote()
    expect(nextActiveQuote).to.equal(null)
  })

  it('should setNextActiveQuote successfully', async () => {
    await quotesService.setNextActiveQuote()
    const oldActiveQuote = await quotesDao.findByQuoteIndex(0)
    const newActiveQuote = await quotesDao.findByQuoteIndex(1)
    expect(oldActiveQuote).to.containSubset(expectedOldActiveQuote)
    expect(newActiveQuote).to.containSubset(expectedNewActiveQuote)
  })

  it('should setNextActiveQuote back to the first quote if current active quote is the last on db', async () => {
    await quotesService.setNextActiveQuote() // sets active to second quote
    await quotesService.setNextActiveQuote() // sets active to third quote
    await quotesService.setNextActiveQuote() // sets active back to first quote
    const newActiveQuote = await quotesDao.findByQuoteIndex(0)
    expect(newActiveQuote).to.containSubset(expectedActiveQuote)
  })
})
