const id = require('pow-mongodb-fixtures').createObjectId

export const userroles = {
  admins: {
    _id: id(),
    name: 'admins',
    permissions: ['create:quote', 'update:footer'],
  },
  metalGeeks: {
    _id: id(),
    name: 'Metal Geeks',
    permissions: ['update:band', 'update:artist'],
  },
}

export const users = {
  juanca: {
    _id: id(),
    firstname: 'Juan',
    email: 'juank.memphis@gmail.com',
    roles: [userroles.admins._id, userroles.metalGeeks._id],
  },
  san: {
    _id: id(),
    firstname: 'Sandra',
    email: 'sandra.aguilar.delgado@gmail.com',
  },
}

export const quotes = {
  first: {
    _id: id(),
    text: 'Real music is not on the radio',
    author: 'Ozzy Ozbourne',
    quoteIndex: 0,
    status: 'active',
  },
  second: {
    _id: id(),
    text: 'Keep calm and listen to metal',
    author: 'Unknown',
    quoteIndex: 1,
    status: 'inactive',
  },
  third: {
    _id: id(),
    text: 'Life is too short for shit music',
    author: 'Dave Mustaine',
    quoteIndex: 2,
    status: 'inactive',
  },
}
