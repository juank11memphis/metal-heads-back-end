const id = require('pow-mongodb-fixtures').createObjectId

export const userroles = {
  admins: {
    _id: id(),
    name: 'admins',
    permissions: ['create:quote', 'update:footer'],
  },
  metalGeeks: {
    _id: id(),
    name: 'Metal Geeks',
    permissions: ['update:band', 'update:artist'],
  },
}

export const users = {
  juanca: {
    _id: id(),
    firstname: 'Juan',
    lastname: 'Morales',
    email: 'juank.memphis@gmail.com',
    password: '$2a$04$8mHfVd43uWR2WGG7wzbMb.GC2.WyOG1KjEMI7dgoElp1i8R5Qhniu',
    hasPassword: true,
    country: 'cr',
    roles: [userroles.admins._id, userroles.metalGeeks._id],
  },
  san: {
    _id: id(),
    firstname: 'Sandra',
    lastname: 'Aguilar',
    email: 'sandra.aguilar.delgado@gmail.com',
    password: '$2a$04$8mHfVd43uWR2WGG7wzbMb.GC2.WyOG1KjEMI7dgoElp1i8R5Qhniu',
    hasPassword: true,
    country: 'cr',
  },
}
