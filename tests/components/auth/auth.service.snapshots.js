import { users, userroles } from './auth.service.fixtures'

export const newUser = {
  firstname: 'Jose Pablo',
  lastname: 'Morales',
  email: 'jose@gmail.com',
  password: '$2a$04$8mHfVd43uWR2WGG7wzbMb.GC2.WyOG1KjEMI7dgoElp1i8R5Qhniu',
}

export const expectedSignInData = {
  user: {
    _id: users.juanca._id,
    firstname: 'Juan',
    lastname: 'Morales',
    email: 'juank.memphis@gmail.com',
    password: '$2a$04$8mHfVd43uWR2WGG7wzbMb.GC2.WyOG1KjEMI7dgoElp1i8R5Qhniu',
    hasPassword: true,
    country: 'cr',
    socialAccounts: [],
    roles: [
      {
        _id: userroles.admins._id,
        name: 'admins',
        permissions: ['create:quote', 'update:footer'],
      },
      {
        _id: userroles.metalGeeks._id,
        name: 'Metal Geeks',
        permissions: ['update:band', 'update:artist'],
      },
    ],
  },
}

export const expectedSignupData = {
  user: {
    firstname: 'new firstname',
    lastname: 'new lastname',
    email: 'new@gmail.com',
    hasPassword: true,
    socialAccounts: [],
    roles: [],
  },
}

export const expectedSignupSocialData = {
  user: {
    firstname: 'new firstname',
    lastname: 'new lastname',
    email: 'new@gmail.com',
    hasPassword: false,
    socialAccounts: [],
    roles: [],
  },
}

export const expectedSignupSocialExistingUserData = {
  user: {
    _id: users.juanca._id,
    firstname: 'Juan',
    lastname: 'Morales',
    email: 'juank.memphis@gmail.com',
    password: '$2a$04$8mHfVd43uWR2WGG7wzbMb.GC2.WyOG1KjEMI7dgoElp1i8R5Qhniu',
    hasPassword: true,
    country: 'cr',
    socialAccounts: [],
    roles: [],
  },
}

export const expectedResetPasswordData = {
  user: {
    _id: users.juanca._id,
    firstname: 'Juan',
    lastname: 'Morales',
    email: 'juank.memphis@gmail.com',
    hasPassword: true,
    country: 'cr',
    socialAccounts: [],
    roles: [],
  },
}
