import { expect } from 'chai'

import Band from '../../app/components/bands/band'
import { removeMany, updateMany } from '../../app/util/db.util'

describe('DB util', () => {
  it('should handle updateMany when empty array is sent', async () => {
    const result = await updateMany(Band)
    expect(result).to.deep.equal([])
  })

  it('should handle removeMany when empty array is sent', async () => {
    const result = await removeMany(Band)
    expect(result).to.deep.equal([])
  })
})
