import { expect } from 'chai'
import { IsUserError } from 'graphql-errors'

import '../tests.util'
import * as ErrorsUtil from '../../app/util/errors.util'
import AppError from '../../app/core/app.error'

describe('Errors util', () => {
  it('should return an AppError as a UserError', () => {
    const graphError = ErrorsUtil.errorAsGraphError(new AppError('a new error'))
    expect(graphError.name).to.equal('Error')
    expect(graphError.message).to.equal('a new error')
    expect(graphError[IsUserError]).to.equal(true)
  })

  it('should return a ValidationError as a UserError', () => {
    const validationError = new Error(
      'Validation failed: password: Password must be at least 6 characters long',
    )
    validationError.name = 'ValidationError'
    const graphError = ErrorsUtil.errorAsGraphError(validationError)
    expect(graphError.name).to.equal('Error')
    expect(graphError.message).to.equal(
      'Password must be at least 6 characters long',
    )
    expect(graphError[IsUserError]).to.equal(true)
  })

  it('should just return a normal error as is', () => {
    const graphError = ErrorsUtil.errorAsGraphError(new Error('a new error'))
    expect(graphError.name).to.equal('Error')
    expect(graphError.message).to.equal('a new error')
    expect(graphError[IsUserError]).to.equal(undefined)
  })
})
