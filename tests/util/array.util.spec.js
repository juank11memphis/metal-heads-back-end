import { expect } from 'chai'

import { sortBasedOnArray } from '../../app/util/array.util'

describe('Array util', () => {
  it('should sort an array based on another', () => {
    const sortedArray = [
      { id: 1, name: 'uno' },
      { id: 2, name: 'dos' },
      { id: 3, name: 'tres' },
      { id: 4, name: 'four' },
    ]
    const unsortedArray = [
      { id: 3, name: 'tres', lastName: 'three' },
      { id: 1, name: 'uno', lastName: 'one' },
      { id: 2, name: 'dos', lastName: 'two' },
    ]
    const newArray = sortBasedOnArray({
      unordered: unsortedArray,
      ordered: sortedArray,
      unorderedField: 'id',
      orderedField: 'id',
    })
    expect(newArray.length).to.equal(3)
    expect(newArray[0]).to.deep.equal({ id: 1, name: 'uno', lastName: 'one' })
    expect(newArray[1]).to.deep.equal({ id: 2, name: 'dos', lastName: 'two' })
    expect(newArray[2]).to.deep.equal({
      id: 3,
      name: 'tres',
      lastName: 'three',
    })
  })

  it('should work with default values', () => {
    const newArray = sortBasedOnArray()
    expect(newArray).to.deep.equal([])
  })
})
