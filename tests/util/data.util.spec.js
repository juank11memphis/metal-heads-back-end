import { expect } from 'chai'
import moment from 'moment'

import Constants from '../../app/core/constants'
import '../tests.util'
import * as DataUtil from '../../app/util/data.util'
import Band from '../../app/components/bands/band'
import Album from '../../app/components/albums/album'
import Artist from '../../app/components/artists/artist'

describe('Data Util', () => {
  it('should know if an image url is already in S3 or not', () => {
    expect(
      DataUtil.isImageAlreadyInS3(
        'https://metalheads.imgix.net/bands/1133.jpg',
      ),
    ).to.equal(true)
    expect(
      DataUtil.isImageAlreadyInS3('https://metalheads.com/bands/1133.jpg'),
    ).to.equal(false)
  })

  it('should generate Band picture urls when band has no picture', () => {
    const band = new Band({ metalArchivesId: '3540350534' })
    const pictureUrl = DataUtil.getBandPictureUrl(band)
    expect(pictureUrl).to.equal(
      'https://www.metal-archives.com/images/3/5/4/0/3540350534_photo.jpg?auto=compress,enhance,redeye',
    )
  })

  it('should generate Band picture urls when band has a picture', () => {
    const band = new Band({
      metalArchivesId: '3540350534',
      picture: 'http://image.png',
    })
    const pictureUrl = DataUtil.getBandPictureUrl(band)
    expect(pictureUrl).to.equal(
      'https://image.png?auto=compress,enhance,redeye',
    )
  })

  it('should not generate Band pictures urls if a metalArchivesId is not provided', () => {
    const band = new Band()
    const pictureUrl = DataUtil.getBandPictureUrl(band)
    expect(pictureUrl).to.equal('')
    expect(DataUtil.generateBandPictureUrl(band)).to.equal('')
  })

  it('should generate Album picture url when Album has no picture', () => {
    const album = new Album({ metalArchivesId: '348987' })
    const pictureUrl = DataUtil.getAlbumPictureUrl(album)
    expect(pictureUrl).to.equal(
      'https://www.metal-archives.com/images/3/4/8/9/348987.jpg?auto=compress,enhance,redeye',
    )
  })

  it('should generate Album picture url when Album has a picture', () => {
    const album = new Album({
      metalArchivesId: '348987',
      picture: 'http://image.png',
    })
    const pictureUrl = DataUtil.getAlbumPictureUrl(album)
    expect(pictureUrl).to.equal(
      'https://image.png?auto=compress,enhance,redeye',
    )
  })

  it('should not generate Album pictures urls if a metalArchivesId is not provided', () => {
    const album = new Album()
    const pictureUrl = DataUtil.getAlbumPictureUrl(album)
    expect(pictureUrl).to.equal('')
    expect(DataUtil.generateAlbumPictureUrl(album)).to.equal('')
  })

  it('should generate Artist picture url when Artist has no picture', () => {
    const artist = new Artist({ metalArchivesId: '26839' })
    const pictureUrl = DataUtil.getArtistPictureUrl(artist)
    expect(pictureUrl).to.equal(
      'https://www.metal-archives.com/images/2/6/8/3/26839_artist.jpg?auto=compress,enhance,redeye',
    )
  })

  it('should generate Artist picture url when Artist has a picture', () => {
    const artist = new Artist({
      metalArchivesId: '26839',
      picture: 'http://image.png',
    })
    const pictureUrl = DataUtil.getArtistPictureUrl(artist)
    expect(pictureUrl).to.equal(
      'https://image.png?auto=compress,enhance,redeye',
    )
  })

  it('should not generate Artist pictures urls if a metalArchivesId is not provided', () => {
    const artist = new Artist()
    const pictureUrl = DataUtil.getArtistPictureUrl(artist)
    expect(pictureUrl).to.equal('')
    expect(DataUtil.generateArtistPictureUrl(artist)).to.equal('')
  })

  it('should know if a band needs to sync with its source or not', () => {
    const band = {}

    band.dataExtracted = false
    let result = DataUtil.needsSync(band)
    expect(result).to.be.false

    band.dataExtracted = true
    band.lastExtractionDate = null
    result = DataUtil.needsSync(band)
    expect(result).to.be.true

    // Last extraction date no older than DAYS_BEFORE_SYNC_BAND_DATA requires no sync
    let lastExtractionDate = moment()
    lastExtractionDate.subtract(Constants.daysBeforeSyncBandData, 'days')
    band.dataExtracted = true
    band.lastExtractionDate = lastExtractionDate.format()
    result = DataUtil.needsSync(band)
    expect(result).to.be.false

    // Last extraction date older than DAYS_BEFORE_SYNC_BAND_DATA requires sync of data
    lastExtractionDate = moment()
    lastExtractionDate.subtract(Constants.daysBeforeSyncBandData + 1, 'days')
    band.dataExtracted = true
    band.lastExtractionDate = lastExtractionDate.format()
    result = DataUtil.needsSync(band)
    expect(result).to.be.true
  })
})
