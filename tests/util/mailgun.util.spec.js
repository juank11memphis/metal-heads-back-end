import { expect } from 'chai'
import sinon from 'sinon'
import Mailgun from 'mailgun-js'

import '../tests.util'
import * as MailgunUtil from '../../app/util/mailgun.util'

const sandbox = sinon.createSandbox()

describe('Mailgun Util', () => {
  before(() => {
    sandbox
      .stub(
        Mailgun({
          apiKey: 'foo',
          domain: 'bar',
        }).Mailgun.prototype,
        'messages',
      )
      .returns({
        sendMime: (data, cb) => cb(null, true),
      })
  })

  after(() => {
    sandbox.restore()
  })

  it('should send welcome emails', async () => {
    const result = await MailgunUtil.sendWelcomeEmail({
      firstname: 'Juan',
      lastname: 'Morales',
      email: 'test@gmail.com',
    })

    expect(result).to.equal(true)
  })

  it('should send forgot password emails', async () => {
    const result = await MailgunUtil.sendResetPasswordEmail({
      email: 'test@gmail.com',
      link: 'an_awesome_reset_password_link',
    })
    expect(result).to.equal(true)
  })
})
