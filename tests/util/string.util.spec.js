import { expect } from 'chai'

import '../tests.util'
import * as StringUtil from '../../app/util/string.util'

describe('String util', () => {
  it('should remove diacritics from a string', () => {
    expect(StringUtil.removeDiacritics('Køldun')).to.equal('Koldun')
    expect(StringUtil.removeDiacritics('Í blóði og anda')).to.equal(
      'I bloði og anda',
    )
  })

  it('should generate random strings', () => {
    const randomString = StringUtil.generateRandomString(20)
    expect(randomString.length).to.equal(20)
  })
})
