import mongoose from 'mongoose'

const Schema = mongoose.Schema

const UserAlbumSchema = new Schema(
  {
    user: {
      type: Schema.Types.ObjectId,
      ref: 'User',
    },
    band: {
      type: Schema.Types.ObjectId,
      ref: 'Band',
    },
    album: {
      type: Schema.Types.ObjectId,
      ref: 'Album',
    },
    rating: Number,
  },
  {
    timestamps: true,
    collection: 'useralbums',
  },
)

export default mongoose.models.UserAlbum ||
  mongoose.model('UserAlbum', UserAlbumSchema)
