import UserAlbum from './user-album'

export class UserAlbumsDao {
  create(data) {
    const newUserAlbum = new UserAlbum(data)
    return newUserAlbum.save()
  }

  update(dbUserAlbum, updateData) {
    return dbUserAlbum.update(updateData)
  }

  getDetail(userId, albumId) {
    return UserAlbum.findOne({ user: userId, album: albumId })
  }

  remove(userAlbum) {
    return userAlbum.remove()
  }

  loadByUserAndBand(userId, bandId) {
    return UserAlbum.find({ user: userId, band: bandId })
  }
}

export default new UserAlbumsDao()
