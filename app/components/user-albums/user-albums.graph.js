import {
  graphHandler,
  graphHandlerSilent,
} from '../../core/middleware/graph-handler'

export const userAlbumType = `
  type UserAlbum {
    id: String
    rating: Float
    album: Album
    albumId: String
    userId: String
  }
`

const loadAlbum = async (userAlbum, loaders) => {
  return userAlbum.albumId ? loaders.albums.load(userAlbum.albumId) : null
}

export const userAlbumTypeResolver = {
  id: userAlbum => userAlbum._id || userAlbum.id,
  album: (userAlbum, _, { loaders }) => {
    return graphHandlerSilent({
      func: loadAlbum,
      funcName: 'userAlbumType -> loadAlbum',
      args: [userAlbum, loaders],
    })
  },
}

export const userAlbumsQueryTypes = `
  loadUserAlbumData(userId: String, albumId: String): UserAlbum
`

export const userAlbumsQueriesResolvers = {
  loadUserAlbumData: async (
    _,
    { userId, albumId },
    { userAlbumsService, token },
  ) => {
    return graphHandler({
      func: userAlbumsService.getUserAlbumData,
      funcName: 'userAlbumsService -> getUserAlbumData',
      args: [userId, albumId],
      auth: { token },
      sendUserId: false,
    })
  },
}

export const userAlbumsMutationTypes = `
  rateAlbum(albumId: String, rating: Float): UserAlbum
  removeAlbumRating(albumId: String): UserAlbum
`

export const userAlbumsMutationsResolvers = {
  rateAlbum: async (_, { albumId, rating }, { userAlbumsService, token }) => {
    return graphHandler({
      func: userAlbumsService.rateAlbum,
      funcName: 'userAlbumsService -> rateAlbum',
      args: [albumId, rating],
      auth: { token },
    })
  },
  removeAlbumRating: async (_, { albumId }, { userAlbumsService, token }) => {
    return graphHandler({
      func: userAlbumsService.removeAlbumRating,
      funcName: 'userAlbumsService -> removeAlbumRating',
      args: [albumId],
      auth: { token },
    })
  },
}
