import { get } from 'lodash'

import {
  recalculateRatingsData,
  recalculateRatingsDataOnRemoveRating,
  getValidRatingValue,
} from '../../util/data.util'
import albumsDao from '../albums/albums.dao'
import userAlbumsDao from './user-albums.dao'
import activitiesService from '../activities/activities.service'

export class UserAlbumsService {
  getUserAlbumData = async (userId, albumId) => {
    const userAlbum = await userAlbumsDao.getDetail(userId, albumId)
    return this.formatUserAlbumDataObject(userAlbum, userId, albumId)
  }

  loadByUserAndBand = async (userId, bandId) => {
    const albumsByBand = await userAlbumsDao.loadByUserAndBand(userId, bandId)
    return albumsByBand.map(userAlbum =>
      this.formatUserAlbumDataObject(userAlbum, userId, userAlbum.album),
    )
  }

  rateAlbum = async (userId, albumId, rating) => {
    let newRating = getValidRatingValue(rating)
    let album = await albumsDao.getDetail(albumId)
    let dbUserAlbum = await userAlbumsDao.getDetail(userId, album._id)

    const currentRating = get(dbUserAlbum, 'rating', null)
    const ratingsData = recalculateRatingsData(album, currentRating, newRating)
    if (dbUserAlbum === null) {
      await userAlbumsDao.create({
        user: userId,
        band: album.band,
        album,
        rating: newRating,
      })
    } else {
      await userAlbumsDao.update(dbUserAlbum, { rating: newRating })
    }
    album = await albumsDao.update(album, { ratingsData })
    activitiesService.createRateAlbumActivity(userId, album, newRating)
    return this.getUserAlbumData(userId, albumId)
  }

  removeAlbumRating = async (userId, albumId) => {
    let album = await albumsDao.getDetail(albumId)
    let userAlbum = await userAlbumsDao.getDetail(userId, album._id)
    const currentRating = get(userAlbum, 'rating', null)
    if (!userAlbum || !currentRating) {
      return this.formatUserAlbumDataObject(userAlbum, userId, albumId)
    }

    const ratingsData = recalculateRatingsDataOnRemoveRating(
      album,
      currentRating,
    )

    album = await albumsDao.update(album, { ratingsData })
    await userAlbumsDao.remove(userAlbum)

    return this.getUserAlbumData(userId, albumId)
  }

  formatUserAlbumDataObject(userAlbum, userId, albumId) {
    return {
      id: `${userId}-${albumId}`,
      rating: get(userAlbum, 'rating', null),
      userId,
      albumId,
    }
  }
}

export default new UserAlbumsService()
