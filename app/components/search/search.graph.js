import { graphHandler } from '../../core/middleware/graph-handler'

export const searchTypes = `
  type BandSearch {
    id: String!
    name: String
    genre: String
    country: String
  }

  type ArtistSearch {
    id: String!
    name: String
    gender: String
  }

  type AlbumSearch {
    id: String!
    name: String
    year: String
    type: String
  }

  type Search {
    bands: [BandSearch],
    albums: [AlbumSearch],
    artists: [ArtistSearch],
  }
`

export const searchQueryTypes = `
  search(searchText: String): Search
`

export const searchQueriesResolvers = {
  search: (_, { searchText }, { searchService }) => {
    return graphHandler({
      func: searchService.search,
      funcName: 'searchService -> search',
      args: [searchText],
    })
  },
}
