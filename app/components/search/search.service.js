import elasticsearchApi from '../../data/api/elasticsearch.api'

export class SearchService {
  search = async (searchText = '') => {
    if (!searchText || searchText === '') {
      return []
    }
    const result = await elasticsearchApi.searchByName(searchText)
    const {
      hits: { hits },
    } = result
    const finalResponse = this.groupSearchResultsByEntityType(hits)
    return finalResponse
  }

  groupSearchResultsByEntityType = searchResult => {
    const response = {
      bands: [],
      albums: [],
      artists: [],
    }
    for (const searchItem of searchResult) {
      const { _source } = searchItem
      const { entityType } = _source
      response[`${entityType}s`].push(
        this.transformElasticSearchEntity(_source),
      )
    }
    return response
  }

  transformElasticSearchEntity = esItem => {
    const {
      name,
      metadata: { id, ...rest },
    } = esItem
    return {
      id,
      name,
      ...rest,
    }
  }
}

export default new SearchService()
