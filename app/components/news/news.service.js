import loudwireWebScraper from '../../data/webscraping/loudwire.webscraper'
import blabbermouthScraper from '../../data/webscraping/blabbermouth.webscraper'

export class NewsService {
  async loadNewsBySource(source = '') {
    let news
    switch (source) {
      case 'loudwire':
        news = await loudwireWebScraper.scrapNews()
        break
      case 'blabbermouth':
        news = await blabbermouthScraper.scrapNews()
        break
      default:
        news = []
    }
    return news
  }
}

export default new NewsService()
