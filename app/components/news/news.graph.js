import { graphHandler } from '../../core/middleware/graph-handler'

export const newsType = `
  type NewsItem {
    id: String!
    link: String
    title: String
  }
`

export const newsQueryTypes = `
  loadNewsBySource(source: String): [NewsItem]
`

export const newsQueriesResolvers = {
  loadNewsBySource: (_, { source }, { newsService }) => {
    return graphHandler({
      func: newsService.loadNewsBySource,
      funcName: 'newsService -> loadNewsBySource',
      args: [source],
    })
  },
}
