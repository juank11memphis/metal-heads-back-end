import mongoose from 'mongoose'

const Schema = mongoose.Schema

const ActivitySchema = new Schema(
  {
    actor: {
      type: Schema.Types.ObjectId,
      ref: 'User',
    },
    verb: String,
    objectId: String,
    objectType: String,
    metadata: {},
    target: String,
  },
  {
    timestamps: true,
    collection: 'activities',
  },
)

export default mongoose.models.Activity ||
  mongoose.model('Activity', ActivitySchema)
