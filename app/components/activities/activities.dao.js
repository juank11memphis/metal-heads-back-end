import Activity from './activity'

export class ActivityDao {
  async create(data) {
    const { actor, verb, objectId, objectType } = data
    // We only keep one activity record per actor-verb-objectId-objectType combination
    await Activity.remove({ actor, verb, objectId, objectType })
    return Activity.create(data)
  }

  async findOneLatest(query) {
    const activities = await Activity.find(query).sort({
      createdAt: -1,
    })
    return activities[0]
  }

  async findLatest(query, limit) {
    return Activity.find(query)
      .sort({
        createdAt: -1,
      })
      .limit(limit)
  }
}

export default new ActivityDao()
