import activitiesDao from './activities.dao'
import albumsDao from '../albums/albums.dao'

export class ActivitiesService {
  createRateBandActivity(userId, band, rating) {
    return activitiesDao.create({
      actor: userId,
      verb: 'rate',
      objectId: band._id,
      objectType: 'band',
      metadata: {
        rating,
      },
    })
  }

  createRateAlbumActivity(userId, album, rating) {
    return activitiesDao.create({
      actor: userId,
      verb: 'rate',
      objectId: album._id,
      objectType: 'album',
      metadata: {
        rating,
      },
    })
  }

  createBandAddedToWatchlistActivity(userId, bandId) {
    return activitiesDao.create({
      actor: userId,
      verb: 'add',
      objectId: bandId,
      objectType: 'band',
      target: 'watchlist',
    })
  }

  createPlayAlbumActivity = async (userId, albumId, target) => {
    const album = await albumsDao.getDetail(albumId)
    return activitiesDao.create({
      actor: userId,
      verb: 'play',
      objectId: album._id,
      objectType: 'album',
      target,
    })
  }

  getLatestBandRatings(limit) {
    return activitiesDao.findLatest(
      {
        verb: 'rate',
        objectType: 'band',
      },
      limit,
    )
  }

  getLatestAlbumRatings(limit) {
    return activitiesDao.findLatest(
      {
        verb: 'rate',
        objectType: 'album',
      },
      limit,
    )
  }

  getLatestMusicPlayed(limit) {
    return activitiesDao.findLatest(
      {
        verb: 'play',
      },
      limit,
    )
  }
}

export default new ActivitiesService()
