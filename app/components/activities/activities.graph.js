import {
  graphHandler,
  graphHandlerSilent,
} from '../../core/middleware/graph-handler'
export const activityType = `
  type ActivityMetadata {
    rating: String
  }

  type Activity {
    id: String
    actor: User
    verb: String
    band: Band
    album: Album
    metadata: ActivityMetadata
    target: String
  }
`

const loadActivityUser = async (activity, loaders) => {
  return activity.actor ? loaders.users.load(activity.actor) : null
}

const loadActivityBand = async ({ objectId }, loaders) => {
  return objectId ? loaders.bands.load(objectId) : null
}

const loadActivityAlbum = async ({ objectId }, loaders) => {
  return objectId ? loaders.albums.load(objectId) : null
}

export const activityTypeResolver = {
  id: activity => activity._id,
  actor: (activity, args, { loaders }) => {
    return graphHandlerSilent({
      func: loadActivityUser,
      funcName: 'activityType -> loadActivityUser',
      args: [activity, loaders],
    })
  },
  band: (activity, args, { loaders }) => {
    return graphHandlerSilent({
      func: loadActivityBand,
      funcName: 'activityType -> loadActivityBand',
      args: [activity, loaders],
    })
  },
  album: (activity, args, { loaders }) => {
    return graphHandlerSilent({
      func: loadActivityAlbum,
      funcName: 'activityType -> loadActivityAlbum',
      args: [activity, loaders],
    })
  },
}

export const activitiesQueryTypes = `
  getLatestBandRatings(limit: Float): [Activity]
  getLatestAlbumRatings(limit: Float): [Activity]
`

export const activitiesQueriesResolvers = {
  getLatestBandRatings: (_, { limit }, { activitiesService }) => {
    return graphHandler({
      func: activitiesService.getLatestBandRatings,
      funcName: 'activitiesService -> getLatestBandRatings',
      args: [limit],
    })
  },
  getLatestAlbumRatings: (_, { limit }, { activitiesService }) => {
    return graphHandler({
      func: activitiesService.getLatestAlbumRatings,
      funcName: 'activitiesService -> getLatestAlbumRatings',
      args: [limit],
    })
  },
}

export const activitiesMutationTypes = `
  createPlayAlbumActivity(albumId: String, target: String): Boolean
`

export const activitiesMutationsResolvers = {
  createPlayAlbumActivity: async (
    _,
    { albumId, target },
    { activitiesService, token },
  ) => {
    return graphHandlerSilent({
      func: activitiesService.createPlayAlbumActivity,
      funcName: 'activitiesService -> createPlayAlbumActivity',
      args: [albumId, target],
      auth: {
        token,
      },
    })
  },
}
