import { get } from 'lodash'

import {
  recalculateRatingsData,
  recalculateRatingsDataOnRemoveRating,
  getValidRatingValue,
} from '../../util/data.util'
import AppError from '../../core/app.error'
import bandsDao from '../bands/bands.dao'
import userBandsDao from './user-bands.dao'
import activitiesService from '../activities/activities.service'

export class UserBandsService {
  loadSavedBands = async userId => {
    const savedBands = await userBandsDao.loadSavedBands(userId)
    return savedBands.map(userBand =>
      this.formatUserBandDataObject(userBand, userId, userBand.band),
    )
  }

  loadRatedBands = async userId => {
    const ratedUserBands = await userBandsDao.loadRatedBands(userId)
    return ratedUserBands.map(userBand =>
      this.formatUserBandDataObject(userBand, userId, userBand.band),
    )
  }

  saveBandForLater = async (userId, bandId) => {
    let userBand = await userBandsDao.getDetailByStatus(userId, bandId, 'saved')
    if (userBand) {
      return this.formatUserBandDataObject(userBand, userId, bandId)
    }
    const data = { status: 'saved' }
    await this.createOrUpdate({ userId, bandId, status: 'removed', data })
    activitiesService.createBandAddedToWatchlistActivity(userId, bandId)
    return this.getUserBandData(userId, bandId)
  }

  removeFromSavedBands = async (userId, bandId) => {
    const userBand = await userBandsDao.getDetailByStatus(
      userId,
      bandId,
      'saved',
    )
    if (!userBand) {
      throw new AppError('Invalid user or band')
    }
    await userBandsDao.update(userBand, { status: 'removed' })
    return this.getUserBandData(userId, bandId)
  }

  rateBand = async (userId, bandId, rating) => {
    const newRating = getValidRatingValue(rating)
    const data = { rating: newRating, status: 'rated' }

    const currentRating = await this.getCurrentRating(userId, bandId)
    await this.createOrUpdate({ userId, bandId, data })
    let band = await bandsDao.getDetail(bandId)
    const ratingsData = recalculateRatingsData(band, currentRating, newRating)
    band = await bandsDao.updateBand(band, { ratingsData: ratingsData })
    activitiesService.createRateBandActivity(userId, band, newRating)
    return this.getUserBandData(userId, bandId)
  }

  removeBandRating = async (userId, bandId) => {
    let band = await bandsDao.getDetail(bandId)
    let userBand = await userBandsDao.getDetail(userId, band._id)
    const currentRating = get(userBand, 'rating', null)
    if (!userBand || !currentRating) {
      return this.getUserBandData(userId, bandId)
    }
    const ratingsData = recalculateRatingsDataOnRemoveRating(
      band,
      currentRating,
    )
    await userBandsDao.remove(userBand)
    await bandsDao.updateBand(band, { ratingsData })
    return this.getUserBandData(userId, bandId)
  }

  getCurrentRating = async (userId, bandId) => {
    const userBandData = await this.getUserBandData(userId, bandId)
    return userBandData ? userBandData.rating : null
  }

  getUserBandData = async (userId, bandId) => {
    const currentUserBandData = await userBandsDao.getDetail(userId, bandId)
    return this.formatUserBandDataObject(currentUserBandData, userId, bandId)
  }

  createOrUpdate = async ({ userId, bandId, data, status }) => {
    let userBand
    if (status) {
      userBand = await userBandsDao.getDetailByStatus(userId, bandId, status)
    } else {
      userBand = await userBandsDao.getDetail(userId, bandId)
    }
    if (userBand === null) {
      return userBandsDao.create({
        user: userId,
        band: bandId,
        ...data,
      })
    }
    await userBandsDao.update(userBand, { ...data })
  }

  formatUserBandDataObject(userBand, userId, bandId) {
    const status = get(userBand, 'status', null)
    return {
      id: `${userId}-${bandId}`,
      rating: get(userBand, 'rating', null),
      saved: status === 'saved',
      status,
      userId,
      bandId,
    }
  }
}

export default new UserBandsService()
