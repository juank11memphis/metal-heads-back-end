import mongoose from 'mongoose'

const Schema = mongoose.Schema

const UserBandSchema = new Schema(
  {
    user: {
      type: Schema.Types.ObjectId,
      ref: 'User',
    },
    band: {
      type: Schema.Types.ObjectId,
      ref: 'Band',
    },
    rating: Number,
    status: String, // rated or saved
  },
  {
    timestamps: true,
    collection: 'userbands',
  },
)

export default mongoose.models.UserBand ||
  mongoose.model('UserBand', UserBandSchema)
