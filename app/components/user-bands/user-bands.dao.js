import UserBand from './user-band'

export class UserBandsDao {
  create(data) {
    const newUserBand = new UserBand(data)
    return newUserBand.save()
  }

  update(dbUserBand, updateData) {
    return dbUserBand.update(updateData)
  }

  getDetail(userId, bandId) {
    return UserBand.findOne({ user: userId, band: bandId })
  }

  getDetailByStatus(userId, bandId, status) {
    return UserBand.findOne({ user: userId, band: bandId, status })
  }

  remove(userBand) {
    return userBand.remove()
  }

  loadRatedBands(userId) {
    return UserBand.find({ user: userId, status: 'rated' }).sort({
      rating: -1,
    })
  }

  loadSavedBands(userId) {
    return UserBand.find({ user: userId, status: 'saved' }).sort({
      updatedAt: -1,
    })
  }
}

export default new UserBandsDao()
