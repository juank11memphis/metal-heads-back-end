import {
  graphHandler,
  graphHandlerSilent,
} from '../../core/middleware/graph-handler'

export const userBandType = `
  type UserBand {
    id: String
    rating: Float
    status: String
    saved: Boolean
    band: Band
    bandId: String
    userId: String
    userAlbums: [UserAlbum]
  }
`

const loadBand = async (userBand, loaders) => {
  return userBand.bandId ? loaders.bands.load(userBand.bandId) : null
}

export const userBandTypeResolver = {
  id: userBand => userBand._id || userBand.id,
  userAlbums: (userBand, _, { userAlbumsService }) => {
    return graphHandlerSilent({
      func: userAlbumsService.loadByUserAndBand,
      funName: 'userAlbumsService -> loadByUserAndBand',
      args: [userBand.userId, userBand.bandId],
    })
  },
  band: (userBand, _, { loaders }) => {
    return graphHandlerSilent({
      func: loadBand,
      funcName: 'userBandType -> loadBand',
      args: [userBand, loaders],
    })
  },
}

export const userBandsQueryTypes = `
  loadSavedBands(userId: String): [UserBand]
  loadRatedBands(userId: String): [UserBand]
  loadUserBandData(userId: String, bandId: String): UserBand
`

export const userBandsQueriesResolvers = {
  loadSavedBands: async (_, { userId }, { userBandsService }) => {
    return graphHandler({
      func: userBandsService.loadSavedBands,
      funcName: 'userBandsService -> loadSavedBands',
      args: [userId],
    })
  },
  loadRatedBands: async (_, { userId }, { userBandsService }) => {
    return graphHandler({
      func: userBandsService.loadRatedBands,
      funcName: 'userBandsService -> loadRatedBands',
      args: [userId],
    })
  },
  loadUserBandData: async (
    _,
    { userId, bandId },
    { userBandsService, token },
  ) => {
    return graphHandler({
      func: userBandsService.getUserBandData,
      funcName: 'userBandsService -> getUserBandData',
      args: [userId, bandId],
      auth: { token },
      sendUserId: false,
    })
  },
}

export const userBandsMutationTypes = `
  saveBandForLater(bandId: String): UserBand
  removeFromSavedBands(bandId: String): UserBand
  rateBand(bandId: String, rating: Float): UserBand
  removeBandRating(bandId: String): UserBand
`

export const userBandsMutationsResolvers = {
  saveBandForLater: async (_, { bandId }, { userBandsService, token }) => {
    return graphHandler({
      func: userBandsService.saveBandForLater,
      funcName: 'userBandsService -> saveBandForLater',
      args: [bandId],
      auth: { token },
    })
  },
  removeFromSavedBands: async (_, { bandId }, { userBandsService, token }) => {
    return graphHandler({
      func: userBandsService.removeFromSavedBands,
      funcName: 'userBandsService -> removeFromSavedBands',
      args: [bandId],
      auth: { token },
    })
  },
  rateBand: async (_, { bandId, rating }, { userBandsService, token }) => {
    return graphHandler({
      func: userBandsService.rateBand,
      funcName: 'userBandsService -> rateBand',
      args: [bandId, rating],
      auth: { token },
    })
  },
  removeBandRating: async (_, { bandId }, { userBandsService, token }) => {
    return graphHandler({
      func: userBandsService.removeBandRating,
      funcName: 'userBandsService -> removeBandRating',
      args: [bandId],
      auth: { token },
    })
  },
}
