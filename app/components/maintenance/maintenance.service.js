import bandsDao from '../bands/bands.dao'
import artistsDao from '../artists/artists.dao'
import albumsDao from '../albums/albums.dao'
// import songsService from '../songs/songs.service'

export class MaintenanceService {
  fillElasticSearchData = async (action = 'index') => {
    await bandsDao.storeAllInElasticsearch(action)
    await artistsDao.storeAllInElasticsearch(action)
    await albumsDao.storeAllInElasticsearch(action)
    // await songsService.storeAllInElasticsearch(action)
    return true
  }
}

export default new MaintenanceService()
