import { get } from 'lodash'

import maintenanceService from './maintenance.service'

export class MaintenanceRest {
  fillElasticSearchData = async (req, res, next) => {
    const update = get(req, 'query.update', false)
    const action = update ? 'update' : 'index'
    await maintenanceService.fillElasticSearchData(action)
    return res.json(true)
  }
}

export default new MaintenanceRest()
