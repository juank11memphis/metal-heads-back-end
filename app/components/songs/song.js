import mongoose from 'mongoose'

const Schema = mongoose.Schema

const SongSchema = new Schema(
  {
    name: String,
    length: String,
    dataExtracted: {
      type: Boolean,
      default: false,
    },
  },
  {
    timestamps: true,
    collection: 'songs',
  },
)

export default mongoose.models.Song || mongoose.model('Song', SongSchema)
