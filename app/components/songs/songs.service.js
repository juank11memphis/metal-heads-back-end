import { keyBy } from 'lodash'

import songsDao from './songs.dao'

export class SongsService {
  async loadByIds(ids) {
    const songs = await songsDao.findByIds(ids)
    const songsById = keyBy(songs, '_id')
    return ids.map(songId => songsById[songId])
  }

  async loadSongById(songId) {
    let song = await songsDao.getDetail(songId)
    return song
  }
}

export default new SongsService()
