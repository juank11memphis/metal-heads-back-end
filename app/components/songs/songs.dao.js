import Song from './song'
import elasticsearchApi from '../../data/api/elasticsearch.api'
import { generalLogger } from '../../util/logs'
import * as dbUtil from '../../util/db.util'

export class SongsDao {
  async inserMany(songs) {
    const songsAdded = await Song.insertMany(songs)
    await elasticsearchApi.bulkSongs(songsAdded)
    return songsAdded
  }

  async updateMany(songs) {
    const songsUpdated = await dbUtil.updateMany(Song, songs)
    await elasticsearchApi.bulkSongs(songsUpdated, 'update')
    return songsUpdated
  }

  async removeMany(songIdsToDelete) {
    await dbUtil.removeMany(Song, songIdsToDelete)
    const songIdsToDeleteAsObjs = songIdsToDelete.map(songDeletedId => ({
      _id: songDeletedId,
    }))
    await elasticsearchApi.bulkSongs(songIdsToDeleteAsObjs, 'delete')
    return true
  }

  findByIds(ids) {
    return Song.find({ _id: { $in: ids } })
  }

  getDetail(songId) {
    return Song.findById(songId)
  }

  async storeAllInElasticsearch(action = 'index') {
    const allSongs = await Song.find({})
    generalLogger.info('Fetching all songs...', allSongs.length)
    await elasticsearchApi.bulkSongs(allSongs, action)
    generalLogger.info('All songs inserted in elasticsearch...')
  }
}

export default new SongsDao()
