export const songType = `
  type Song {
    id: String
    name: String
    length: String
  }
`

export const songTypeResolver = {
  id: song => song._id,
}
