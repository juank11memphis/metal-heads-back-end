import Band from './band'
import elasticsearchApi from '../../data/api/elasticsearch.api'
import { generalLogger } from '../../util/logs'

export class BandsDao {
  async updateBand(dbBand, updateData) {
    await dbBand.update(updateData)
    const dbBandUpdated = await this.getDetail(dbBand._id)
    await elasticsearchApi.bulkBands([dbBandUpdated], 'update')
    return dbBandUpdated
  }

  async inserMany(bands) {
    const bandsAdded = await Band.insertMany(bands)
    await elasticsearchApi.bulkBands(bandsAdded)
    return bandsAdded
  }

  getDetail(bandId) {
    return Band.findById(bandId, {
      similarBands: 0,
    })
  }

  getFullDetail(bandId) {
    return Band.findById(bandId, {
      similarBands: 0,
    })
      .populate('currentMembers', {
        picture: 1,
        name: 1,
        roles: 1,
        metalArchivesId: 1,
      })
      .populate('albums', {
        picture: 1,
        name: 1,
        year: 1,
        type: 1,
        metalArchivesId: 1,
      })
  }

  findByIds(ids) {
    return Band.find({
      _id: {
        $in: ids,
      },
    })
  }

  find(query, limit) {
    if (limit) {
      return Band.find(query).limit(limit)
    }
    return Band.find(query)
  }

  findOne(query) {
    return Band.findOne(query)
  }

  loadMostRatedBands() {
    return Band.find({})
      .sort({
        'ratingsData.ratingsCount': -1,
        'ratingsData.globalRating': -1,
      })
      .limit(10)
  }

  loadSimilarBands(bandId) {
    return Band.findById(bandId, {
      _id: 1,
      similarBandsExtracted: 1,
      name: 1,
      metalArchivesId: 1,
      similarBands: 1,
    }).populate('similarBands')
  }

  async storeAllInElasticsearch(action = 'index') {
    const allBands = await Band.find({})
    generalLogger.info('Fetching all bands...', allBands.length)
    await elasticsearchApi.bulkBands(allBands, action)
    generalLogger.info('All bands inserted in elasticsearch...')
  }
}

export default new BandsDao()
