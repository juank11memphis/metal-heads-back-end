import mongoose from 'mongoose'

import '../albums/album'
import '../artists/artist'

const Schema = mongoose.Schema

const BandSchema = new Schema(
  {
    name: String,
    country: String,
    genre: String,
    status: String,
    metalArchivesId: String,
    dataExtracted: {
      type: Boolean,
      default: false,
    },
    extractionFailed: {
      type: Boolean,
      default: false,
    },
    lastExtractionDate: Date,
    similarBandsExtracted: {
      type: Boolean,
      default: false,
    },
    formedIn: String,
    yearsActive: String,
    lyricalThemes: String,
    location: String,
    picture: String,
    albums: [
      {
        type: Schema.Types.ObjectId,
        ref: 'Album',
      },
    ],
    currentMembers: [
      {
        type: Schema.Types.ObjectId,
        ref: 'Artist',
      },
    ],
    similarBands: [
      {
        type: Schema.Types.ObjectId,
        ref: 'Band',
      },
    ],
    ratingsData: {}, // { globalRating, ratingsCount, ratingsSum }
    spotifyData: {}, // { id, uri, externalUrl, images }
  },
  {
    timestamps: true,
  },
)

export default mongoose.models.Band || mongoose.model('Band', BandSchema)
