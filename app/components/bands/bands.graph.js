import { get } from 'lodash'

import * as DataUtil from '../../util/data.util'
import {
  graphHandler,
  graphHandlerSilent,
} from '../../core/middleware/graph-handler'

export const bandType = `
  type Rating {
    id: String
    rating: Float
  }

  type Band {
    id: String!
    name: String
    picture: String
    country: String
    genre: String
    location: String
    formedIn: String
    yearsActive: String
    status: String
    lyricalThemes: String
    albums: AlbumsByType
    currentMembers: [Artist]
    globalRating: Float
    ratingsCount: Int
  }
`

const loadBandAlbums = async (band, loaders, albumsService) => {
  const albums = await loaders.albums.loadMany(band.albums)
  return albumsService.sortAlbumsByType(albums)
}

const loadBandArtists = async (band, loaders) => {
  return loaders.artists.loadMany(band.currentMembers)
}

export const bandTypeResolver = {
  id: band => band._id,
  picture: band => DataUtil.getBandPictureUrl(band),
  albums: async (band, _, { loaders, albumsService }) => {
    return graphHandlerSilent({
      func: loadBandAlbums,
      funcName: 'bandType -> loadBandAlbums',
      args: [band, loaders, albumsService],
    })
  },
  currentMembers: (band, args, { loaders }) => {
    return graphHandlerSilent({
      func: loadBandArtists,
      funcName: 'bandType -> loadBandArtists',
      args: [band, loaders],
    })
  },
  globalRating: band =>
    band.ratingsData ? Math.round(band.ratingsData.globalRating * 10) : null,
  ratingsCount: band => get(band, 'ratingsData.ratingsCount'),
}

export const bandsQueryTypes = `
  loadMostRatedBands: [Band]
  loadSimilarBands(bandId: String): [Band]
  loadBandDetails(bandId: String): Band
`

export const bandQueriesResolvers = {
  loadMostRatedBands: (_, args, { bandsService }) => {
    return graphHandler({
      func: bandsService.loadMostRatedBands,
      funcName: 'bandsService -> loadMostRatedBands',
    })
  },
  loadSimilarBands: (_, { bandId }, { bandsService }) => {
    return graphHandler({
      func: bandsService.loadSimilarBands,
      funcName: 'bandsService -> loadSimilarBands',
      args: [bandId],
    })
  },
  loadBandDetails: (_, { bandId }, { bandsService }) => {
    return graphHandler({
      func: bandsService.loadBandDetails,
      funcName: 'bandsService -> loadBandDetails',
      args: [bandId],
    })
  },
}
