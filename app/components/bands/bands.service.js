import { keyBy } from 'lodash'

import similarBandsETL from '../../data/etl/bands/band-similar-bands.etl'
import bandETL from '../../data/etl/bands/band.etl'
import * as DataUtil from '../../util/data.util'
import bandsDao from './bands.dao'
import imagesToProcessDao from '../images/images-to-process.dao'

export class BandsService {
  loadBandDetails = async bandId => {
    let band = await bandsDao.getDetail(bandId)
    if (!band.dataExtracted) {
      band = await bandETL.start(await bandsDao.getFullDetail(bandId))
      await imagesToProcessDao.markBandForImageProccessing(band)
    } else if (band.dataExtracted && DataUtil.needsSync(band)) {
      bandETL.start(await bandsDao.getFullDetail(bandId))
      await imagesToProcessDao.markBandForImageProccessing(band)
    }
    return band
  }

  loadByIds = async ids => {
    const bands = await bandsDao.findByIds(ids)
    const bandsById = keyBy(bands, '_id')
    return ids.map(bandId => bandsById[bandId])
  }

  loadMostRatedBands() {
    return bandsDao.loadMostRatedBands()
  }

  loadSimilarBands = async bandId => {
    let band = await bandsDao.loadSimilarBands(bandId)
    let similarBands
    if (band.similarBandsExtracted) {
      similarBands = band.similarBands
    } else {
      similarBands = await similarBandsETL.start(band)
    }
    return similarBands
  }
}

export default new BandsService()
