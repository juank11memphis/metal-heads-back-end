import querystring from 'querystring'
import { get } from 'lodash'

import { generateRandomString } from '../../util/string.util'
import Constants from '../../core/constants'
import spotifyApi from '../../data/api/spotify.api'
import { sendWelcomeEmail } from '../../util/mailgun.util'
import { generateToken } from '../../util/auth.util'
import usersService from '../users/users.service'

export class AuthSpotifyRest {
  stateKey = 'spotify_auth_state'
  redirectPageKey = 'spotify_redirect_page'
  userIdKey = 'spotify_redirect_user_id'

  signinWithSpotify = (req, res, next) => {
    const state = generateRandomString(16)

    res.cookie(this.stateKey, state)
    res.cookie(this.redirectPageKey, '/signin')
    return res.redirect(
      'https://accounts.spotify.com/authorize?' +
        querystring.stringify({
          response_type: 'code',
          client_id: Constants.spotifyClientId,
          scope: Constants.spotifyScopes,
          redirect_uri: Constants.spotifyRedirectUrl,
          state,
        }),
    )
  }

  signinWithSpotifyCallback = async (req, res, next) => {
    let shouldSendWelcomeEmail = false
    const code = req.query.code || null
    const state = req.query.state || null
    const storedState = req.cookies ? req.cookies[this.stateKey] : null
    const redirectPage = req.cookies
      ? req.cookies[this.redirectPageKey]
      : '/signin'

    if (state === null || state !== storedState) {
      return res.redirect(
        `${redirectPage}?` +
          querystring.stringify({
            spotifyError: 'state_mismatch',
          }),
      )
    }
    res.clearCookie(this.stateKey)
    res.clearCookie(this.redirectPageKey)
    const spotifyTokens = await spotifyApi.getToken(
      code,
      Constants.spotifyRedirectUrl,
    )
    const spotifyUserData = await spotifyApi.getUserData(
      spotifyTokens.accessToken,
    )
    let user = await usersService.getDetailByEmail(spotifyUserData.email)

    if (!user) {
      await usersService.create({ ...spotifyUserData })
      shouldSendWelcomeEmail = true
    }

    if (shouldSendWelcomeEmail && !Constants.envs.test) {
      sendWelcomeEmail(user)
    }

    await usersService.addOrUpdateSocialAccount({
      userId: user._id,
      accountType: 'spotify',
      accessToken: spotifyTokens.accessToken,
      refreshToken: spotifyTokens.refreshToken,
    })

    const token = generateToken(user)

    return res.redirect(
      `${redirectPage}?` +
        querystring.stringify({
          token: token,
        }),
    )
  }

  connectToSpotify = (req, res, next) => {
    const state = generateRandomString(16)
    const userId = get(req, 'userId')

    res.cookie(this.stateKey, state)
    res.cookie(this.redirectPageKey, '/user/account/social-accounts')
    res.cookie(this.userIdKey, userId)
    return res.redirect(
      'https://accounts.spotify.com/authorize?' +
        querystring.stringify({
          response_type: 'code',
          client_id: Constants.spotifyClientId,
          scope: Constants.spotifyScopes,
          redirect_uri: Constants.spotifyConnectRedirectUrl,
          state,
        }),
    )
  }

  connectToSpotifyCallback = async (req, res, next) => {
    const code = req.query.code || null
    const state = req.query.state || null
    const storedState = req.cookies ? req.cookies[this.stateKey] : null
    const redirectPage = req.cookies
      ? req.cookies[this.redirectPageKey]
      : '/signin'
    const userId = req.cookies ? req.cookies[this.userIdKey] : null
    let user = await usersService.loadUserDetails(userId)
    if (state === null || state !== storedState || !user) {
      return res.redirect(
        `${redirectPage}?` +
          querystring.stringify({
            spotifyError: 'state_mismatch',
          }),
      )
    }
    res.clearCookie(this.stateKey)
    res.clearCookie(this.redirectPageKey)
    res.clearCookie(this.userIdKey)
    const spotifyTokens = await spotifyApi.getToken(
      code,
      Constants.spotifyConnectRedirectUrl,
    )
    await usersService.addOrUpdateSocialAccount({
      userId,
      accountType: 'spotify',
      accessToken: spotifyTokens.accessToken,
      refreshToken: spotifyTokens.refreshToken,
    })
    return res.redirect(redirectPage)
  }
}

export default new AuthSpotifyRest()
