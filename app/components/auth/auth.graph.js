import { graphHandler } from '../../core/middleware/graph-handler'

export const authType = `
  type AuthResponse {
    user: User!
    token: String!
  }
`

export const authMutationTypes = `
  signin(email: String, password: String): AuthResponse
  signup(user: UserInput): AuthResponse
  authSocial(user: UserInput): AuthResponse
  requestPasswordReset(email: String): User
  resetPassword(password: String, token: String): AuthResponse
`

export const authMutationsResolvers = {
  signin: async (_, { email, password }, { authService }) => {
    return graphHandler({
      func: authService.signin,
      funcName: 'authService -> signin',
      args: [email, password],
    })
  },
  signup: async (_, { user }, { authService }) => {
    return graphHandler({
      func: authService.signup,
      funcName: 'authService -> signup',
      args: [user],
    })
  },
  authSocial: (_, { user }, { authService }) => {
    return graphHandler({
      func: authService.createSocial,
      funcName: 'authService -> createSocial',
      args: [user],
    })
  },
  requestPasswordReset: async (_, { email }, { authService }) => {
    return graphHandler({
      func: authService.requestPasswordReset,
      funcName: 'authService -> requestPasswordReset',
      args: [email],
    })
  },
  resetPassword: async (_, { password, token }, { authService }) => {
    return graphHandler({
      func: authService.resetPassword,
      funcName: 'authService -> resetPassword',
      args: [password],
      auth: { token: `bearer ${token}` },
    })
  },
}
