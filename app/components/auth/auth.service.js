import AppError from '../../core/app.error'
import {
  sendWelcomeEmail,
  sendResetPasswordEmail,
} from '../../util/mailgun.util'
import {
  generateToken,
  generateResetPasswordToken,
  comparePasswords,
  hashPassword,
} from '../../util/auth.util'
import Constants from '../../core/constants'
import usersDao from '../users/users.dao'

export class AuthService {
  async signin(email, password) {
    const user = await usersDao.getDetailByEmail(email)
    if (!user) {
      throw new AppError('This email is not registered')
    }
    if (!comparePasswords(password, user.password)) {
      throw new AppError('This password is not correct')
    }
    const token = generateToken(user)
    return {
      user,
      token,
    }
  }

  async signup(user) {
    const hasPassword = user.password ? true : false

    // first save is just to perform validations
    let savedUser = await usersDao.create({ ...user, hasPassword })
    // after having a valid record we encrypt the password and update it
    const encryptedPassword = hashPassword(user.password)
    await usersDao.update(savedUser, { password: encryptedPassword })

    savedUser = await usersDao.getDetailById(savedUser._id)

    const token = generateToken(savedUser)
    sendWelcomeEmail(savedUser)
    return {
      user: savedUser,
      token,
    }
  }

  async createSocial(user) {
    const email = user.email
    let dbUser = await usersDao.getDetailByEmail(email)
    const shouldCreate = dbUser === null
    if (shouldCreate) {
      dbUser = await usersDao.create({ ...user, hasPassword: false })
      sendWelcomeEmail(dbUser)
    }
    const token = generateToken(dbUser)
    return {
      user: dbUser,
      token,
    }
  }

  async requestPasswordReset(email) {
    const user = await usersDao.getDetailByEmail(email)
    if (!user) {
      throw new AppError('This email is not registered')
    }
    const token = generateResetPasswordToken(user)
    sendResetPasswordEmail(
      user,
      `${Constants.security.resetPasswordEmailLink}?token=${token}`,
    )
    return user
  }

  resetPassword = async (userId, password) => {
    const user = await usersDao.getDetailById(userId)
    if (!user) {
      throw new AppError(
        'An unexpected error occurred while reseting your password',
      )
    }
    // first update is just to validate the password is valid
    await user.update({ password })
    const encryptedPassword = hashPassword(password)
    // after having a valid password we encrypt it and update again
    await user.update({
      password: encryptedPassword,
      hasPassword: true,
    })
    const updatedUser = await usersDao.getDetailById(user.id)
    const newToken = generateToken(updatedUser)
    return {
      user: updatedUser,
      token: newToken,
    }
  }
}

export default new AuthService()
