import { graphHandler } from '../../core/middleware/graph-handler'

export const userType = `
  type UserSocialAccount {
    id: String!
    accountType: String!
  }

  type UserRole {
    name: String!
    permissions: [String]
  }

  type User {
    id: String!
    firstname: String
    lastname: String
    email: String
    country: String
    hasPassword: Boolean
    socialAccounts: [UserSocialAccount]
    roles: [UserRole]
  }

  type ExtUser {
    id: String!
    firstname: String
    lastname: String
    country: String
  }

  input UserInput {
    firstname: String
    lastname: String
    email: String
    country: String
    password: String
  }
`

export const usersQueryTypes = `
  loadTokenUser: User
  loadUser(userId: String): ExtUser
`

export const userQueriesResolvers = {
  loadTokenUser: (_, args, { usersService, token }) => {
    return graphHandler({
      func: usersService.loadUserDetails,
      funcName: 'usersService -> loadUserDetails',
      auth: { token },
    })
  },
  loadUser: (_, { userId }, { usersService }) => {
    return graphHandler({
      func: usersService.loadUserDetails,
      funcName: 'usersService -> ext -> loadUserDetails',
      args: [userId],
    })
  },
}

export const usersMutationTypes = `
  updateTokenUser(user: UserInput): User
  changeTokenUserPassword(password: String, newPassword: String): User
  spotifyDisconnect: User
`

export const userMutationsResolvers = {
  updateTokenUser: async (_, { user }, { usersService, token }) => {
    return graphHandler({
      func: usersService.updateUser,
      funcName: 'usersService -> updateUser',
      auth: { token },
      args: [user],
    })
  },
  changeTokenUserPassword: async (
    _,
    { password, newPassword },
    { usersService, token },
  ) => {
    return graphHandler({
      func: usersService.changeUserPassword,
      funcName: 'usersService -> changeUserPassword',
      auth: { token },
      args: [password, newPassword],
    })
  },
  spotifyDisconnect: async (_, args, { usersService, token }) => {
    return graphHandler({
      func: usersService.disconnectUserFromSpotify,
      funcName: 'usersService -> disconnectUserFromSpotify',
      auth: { token },
    })
  },
}
