import { keyBy } from 'lodash'

import AppError from '../../core/app.error'
import { comparePasswords, hashPassword } from '../../util/auth.util'
import usersDao from './users.dao'

export class UsersService {
  async loadByIds(ids) {
    const users = await usersDao.loadByIds(ids)
    const usersById = keyBy(users, '_id')
    return ids.map(userId => usersById[userId])
  }

  create(data) {
    return usersDao.create(data)
  }

  getDetailByEmail(email) {
    return usersDao.getDetailByEmail(email)
  }

  loadUserDetails(userId) {
    return usersDao.getDetailById(userId)
  }

  updateUser = async (userId, updateData) => {
    delete updateData.password
    delete updateData.email
    const dbUser = await usersDao.getDetailById(userId)
    await dbUser.update(updateData)
    return usersDao.getDetailById(userId)
  }

  changeUserPassword = async (userId, password, newPassword) => {
    const dbUser = await usersDao.getDetailById(userId)
    if (password && !comparePasswords(password, dbUser.password)) {
      throw new AppError('Your current password is not correct')
    }
    // first update is just to validate the password is valid
    await dbUser.update({ password: newPassword })
    // after having a valid password we encrypt it and update again
    const encryptedPassword = hashPassword(newPassword)
    await dbUser.update({
      password: encryptedPassword,
      hasPassword: true,
    })
    return usersDao.getDetailById(userId)
  }

  disconnectUserFromSpotify = async userId => {
    const dbUser = await usersDao.getDetailById(userId)
    await usersDao.removeSocialAccount(dbUser, 'spotify')
    return usersDao.getDetailById(userId)
  }

  async addOrUpdateSocialAccount({
    userId,
    accountType,
    accessToken,
    refreshToken,
  }) {
    const dbUser = await usersDao.getDetailById(userId)
    await usersDao.addOrUpdateSocialAccount({
      dbUser,
      accountType,
      accessToken,
      refreshToken,
    })
    return true
  }
}

export default new UsersService()
