import mongoose from 'mongoose'

const Schema = mongoose.Schema

const UserSocialAccountsSchema = new Schema({
  accountType: {
    type: String,
    required: [true, 'Account type is required.'],
  },
  accessToken: {
    type: String,
    required: [true, 'Access token is required.'],
  },
  refreshToken: {
    type: String,
    required: [true, 'Refresh token is required.'],
  },
})

export const ACCOUNT_TYPE_SPOTIFY = 'spotify'

export default mongoose.models.UserSocialAccounts ||
  mongoose.model('UserSocialAccounts', UserSocialAccountsSchema)
