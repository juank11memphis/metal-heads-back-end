import User from './user'
import UserSocialAccounts from './user-social-accounts'

export class UsersDao {
  loadByIds(ids) {
    return User.find({ _id: { $in: ids } })
  }

  create(data) {
    const newUser = new User(data)
    return newUser.save()
  }

  update(dbUser, updateData) {
    return dbUser.update(updateData)
  }

  getDetailByEmail(email) {
    return User.findOne({ email })
      .populate('socialAccounts', { accountType: 1 })
      .populate('roles', { name: 1, permissions: 1 })
  }

  getDetailById(id) {
    return User.findById(id)
      .populate('socialAccounts', { accountType: 1 })
      .populate('roles', { name: 1, permissions: 1 })
  }

  getSocialAccounts = function(id) {
    return User.findById(id).populate('socialAccounts')
  }

  async addOrUpdateSocialAccount({
    dbUser,
    accountType,
    accessToken,
    refreshToken,
  }) {
    let socialAccount = this.findSocialAccountByType(dbUser, accountType)
    if (socialAccount) {
      await socialAccount.update({ accessToken, refreshToken })
      return true
    }
    socialAccount = await UserSocialAccounts.create({
      accountType,
      accessToken,
      refreshToken,
    })
    dbUser.socialAccounts = dbUser.socialAccounts ? dbUser.socialAccounts : []
    dbUser.socialAccounts.push(socialAccount)
    await this.update(dbUser, { socialAccounts: dbUser.socialAccounts })
    return true
  }

  async removeSocialAccount(dbUser, accountType) {
    let socialAccount = this.findSocialAccountByType(dbUser, accountType)
    if (!socialAccount) {
      return true
    }
    const socialAccounts = []
    for (const account of dbUser.socialAccounts) {
      if (account.accountType !== accountType) {
        socialAccounts.push(account)
      }
    }
    await this.update(dbUser, { socialAccounts })
    await socialAccount.remove()
    return true
  }

  findSocialAccountByType(dbUser, type) {
    if (!dbUser.socialAccounts || dbUser.socialAccounts.length === 0) {
      return null
    }
    for (const socialAccount of dbUser.socialAccounts) {
      if (socialAccount.accountType === type) {
        return socialAccount
      }
    }
    return null
  }
}

export default new UsersDao()
