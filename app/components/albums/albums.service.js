import { keyBy } from 'lodash'

import albumETL from '../../data/etl/albums/album.etl'
import albumsDao from './albums.dao'
import bandsDao from '../bands/bands.dao'

export class AlbumsService {
  loadByIds = async ids => {
    const albums = await albumsDao.findByIds(ids)
    const albumsById = keyBy(albums, '_id')
    return ids.map(albumId => albumsById[albumId])
  }

  loadAlbumById = async albumId => {
    let album = await albumsDao.getDetail(albumId)
    if (album.dataExtracted) {
      return album
    }
    const band = await bandsDao.getDetail(album.band)
    album.band = band
    album = await albumETL.start(album)
    return album
  }

  sortAlbumsByType(albums) {
    albums.sort((a, b) => (+a.year < +b.year ? -1 : +a.year > +b.year ? 1 : 0))
    const sortedAlbums = {
      'Full-length': [],
      'Live album': [],
      Video: [],
      'Split video': [],
      Compilation: [],
      'Boxed set': [],
      Single: [],
      Demo: [],
      EP: [],
      Split: [],
    }
    albums.forEach(
      album => sortedAlbums[album.type] && sortedAlbums[album.type].push(album),
    )
    return {
      full: [...sortedAlbums['Full-length']],
      live: [...sortedAlbums['Live album']],
      video: [...sortedAlbums['Video'], ...sortedAlbums['Split video']],
      compilations: [
        ...sortedAlbums['Compilation'],
        ...sortedAlbums['Boxed set'],
      ],
      singles: [...sortedAlbums['Single']],
      other: [
        ...sortedAlbums['Demo'],
        ...sortedAlbums['EP'],
        ...sortedAlbums['Split'],
      ],
    }
  }
}

export default new AlbumsService()
