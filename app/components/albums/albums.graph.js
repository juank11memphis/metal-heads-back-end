import { get } from 'lodash'

import * as DataUtil from '../../util/data.util'
import {
  graphHandlerSilent,
  graphHandler,
} from '../../core/middleware/graph-handler'

export const albumType = `
  type AlbumsByType {
    compilations: [Album]
    full: [Album]
    live: [Album]
    other: [Album]
    singles: [Album]
    video: [Album]
  }

  type AlbumArtist {
    id: String
    name: String
    roles: String
  }

  type Album {
    id: String!
    name: String
    picture: String
    type: String
    year: String
    label: String
    catalogId: String
    format: String
    releaseDate: String
    songs: [Song]
    lineUp: [AlbumArtist]
    band: Band
    globalRating: Float
    ratingsCount: Int
  }
`

const loadAlbumBand = async (album, loaders) => {
  return album.band ? loaders.bands.load(album.band) : null
}

const loadAlbumSongs = async (album, loaders) => {
  return loaders.songs.loadMany(album.tracks)
}

export const albumTypeResolver = {
  id: album => album._id,
  picture: album => DataUtil.getAlbumPictureUrl(album),
  band: (album, args, { loaders }) => {
    return graphHandlerSilent({
      func: loadAlbumBand,
      funcName: 'albumType -> loadAlbumBand',
      args: [album, loaders],
    })
  },
  songs: (album, _, { loaders }) => {
    return graphHandlerSilent({
      func: loadAlbumSongs,
      funcName: 'albumType -> loadAlbumSongs',
      args: [album, loaders],
    })
  },
  globalRating: album => {
    if (!album.ratingsData) {
      return null
    }
    return Math.round(album.ratingsData.globalRating * 10)
  },
  ratingsCount: band => get(band, 'ratingsData.ratingsCount'),
}

export const albumsQueryTypes = `
  loadAlbumDetails(albumId: String): Album
`

export const albumQueriesResolvers = {
  loadAlbumDetails: (_, { albumId }, { albumsService }) => {
    return graphHandler({
      func: albumsService.loadAlbumById,
      funcName: 'albumsService -> loadAlbumById',
      args: [albumId],
    })
  },
}
