import mongoose from 'mongoose'
import '../songs/song'

const Schema = mongoose.Schema

const AlbumSchema = new Schema(
  {
    name: String,
    year: String,
    type: String,
    metalArchivesId: String,
    band: {
      type: Schema.Types.ObjectId,
      ref: 'Band',
    },
    picture: String,
    dataExtracted: {
      type: Boolean,
      default: false,
    },
    extractionFailed: {
      type: Boolean,
      default: false,
    },
    releaseDate: String,
    catalogId: String,
    label: String,
    format: String,
    reviews: String,
    tracks: [
      {
        type: Schema.Types.ObjectId,
        ref: 'Song',
      },
    ],
    lineUp: Array,
    ratingsData: {}, // { globalRating, ratingsCount, ratingsSum }
    spotifyData: {}, // { id, uri, externalUrl, images }
  },
  {
    timestamps: true,
  },
)

export default mongoose.models.Album || mongoose.model('Album', AlbumSchema)
