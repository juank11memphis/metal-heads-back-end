import Album from './album'
import elasticsearchApi from '../../data/api/elasticsearch.api'
import { generalLogger } from '../../util/logs'
import * as dbUtil from '../../util/db.util'

export class AlbumsDao {
  getNewAlbumInstance(data) {
    return new Album(data)
  }

  async update(dbAlbum, updateData) {
    await dbAlbum.update(updateData)
    const dbAlbumUpdated = await this.getDetail(dbAlbum._id)
    await elasticsearchApi.bulkAlbums([dbAlbumUpdated], 'update')
    return dbAlbumUpdated
  }

  async inserMany(albums) {
    const albumsAdded = await Album.insertMany(albums)
    await elasticsearchApi.bulkAlbums(albumsAdded)
    return albumsAdded
  }

  async updateMany(albums) {
    const albumsUpdated = await dbUtil.updateMany(Album, albums)
    await elasticsearchApi.bulkAlbums(albumsUpdated, 'update')
    return albumsUpdated
  }

  getDetail(albumId) {
    return Album.findById(albumId)
  }

  getFullDetail(albumId) {
    return Album.findById(albumId)
      .populate('band')
      .populate('tracks', {
        name: 1,
        length: 1,
      })
  }

  getDetailWithBand(albumId) {
    return Album.findById(albumId).populate('band')
  }

  findByIds(ids) {
    return Album.find({
      _id: {
        $in: ids,
      },
    })
  }

  find(query, limit) {
    if (limit) {
      return Album.find(query).limit(limit)
    }
    return Album.find(query)
  }

  async storeAllInElasticsearch(action = 'index') {
    const allAlbums = await Album.find({})
    generalLogger.info('Fetching all albums...', allAlbums.length)
    await elasticsearchApi.bulkAlbums(allAlbums, action)
    generalLogger.info('All albums inserted in elasticsearch...')
  }
}

export default new AlbumsDao()
