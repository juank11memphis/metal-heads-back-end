import Constants from '../../core/constants'

export class ConfigService {
  async loadConfig() {
    return {
      googleClientId: Constants.googleClientId,
    }
  }
}

export default new ConfigService()
