import { graphHandler } from '../../core/middleware/graph-handler'

export const configType = `
  type Config {
    googleClientId: String
  }
`

export const configQueryTypes = `
  loadConfig: Config
`

export const configQueriesResolvers = {
  loadConfig: (_, args, { configService }) => {
    return graphHandler({
      func: configService.loadConfig,
      funcName: 'configService -> loadConfig',
    })
  },
}
