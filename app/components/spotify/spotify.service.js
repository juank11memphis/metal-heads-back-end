import { get } from 'lodash'

import spotifyApi from '../../data/api/spotify.api'
import albumsDao from '../albums/albums.dao'
import usersDao from '../users/users.dao'
import activitiesService from '../activities/activities.service'

export class SpotifyService {
  loadPlayerDevices = async userId => {
    const spotifyAccessToken = await this.getAccessToken(userId)
    if (!spotifyAccessToken) {
      return []
    }
    const { devices = [] } = await spotifyApi.getPlayerDevices(
      spotifyAccessToken,
    )
    return devices
  }

  getAlbumSpotifyUri = async (userId, albumId) => {
    const album = await albumsDao.getDetailWithBand(albumId)
    const spotifyUri = get(album, 'spotifyData.uri')
    if (spotifyUri) {
      return spotifyUri
    }
    const spotifyAccessToken = await this.getAccessToken(userId)
    if (!spotifyAccessToken) {
      return null
    }
    const albumName = get(album, 'name', '')
    const bandName = get(album, 'band.name', '')
    const spotifyAlbum = await spotifyApi.searchAlbum(
      spotifyAccessToken,
      albumName,
      bandName,
    )
    if (!spotifyAlbum) {
      return null
    }
    const spotifyBand = get(spotifyAlbum, 'artists.0')
    await this.updateAlbumSpotifyData(album, spotifyAlbum)
    await this.updateBandSpotifyData(album.band, spotifyBand)
    return get(spotifyAlbum, 'uri')
  }

  playAlbum = async (userId, deviceId, albumId) => {
    const spotifyAlbumUri = await this.getAlbumSpotifyUri(userId, albumId)
    const spotifyAccessToken = await this.getAccessToken(userId)
    await spotifyApi.toggleShuffle({
      accessToken: spotifyAccessToken,
      state: false,
      deviceId,
    })
    await spotifyApi.playOnDevice(spotifyAccessToken, deviceId, spotifyAlbumUri)
    activitiesService.createPlayAlbumActivity(userId, albumId, 'spotify')
    return true
  }

  getAccessToken = async userId => {
    const { socialAccounts = [] } = await usersDao.getSocialAccounts(userId)
    const spotifyAccount = socialAccounts.find(
      account => account.accountType === 'spotify',
    )
    if (!spotifyAccount) {
      return null
    }
    let accessToken = spotifyAccount.accessToken
    const isSpotifyAccessTokenValid = await spotifyApi.validateAccessToken(
      accessToken,
    )
    if (!isSpotifyAccessTokenValid) {
      accessToken = await this.refreshAccessToken(
        userId,
        spotifyAccount.refreshToken,
      )
    }
    return accessToken
  }

  async refreshAccessToken(userId, refreshToken) {
    const user = await usersDao.getDetailById(userId)
    const accessToken = await spotifyApi.refreshAccessToken(refreshToken)
    if (!accessToken) {
      return null
    }
    await usersDao.addOrUpdateSocialAccount({
      dbUser: user,
      accountType: 'spotify',
      accessToken,
      refreshToken,
    })
    return accessToken
  }

  async updateAlbumSpotifyData(album, spotifyAlbum) {
    const spotifyData = {
      id: get(spotifyAlbum, 'id'),
      uri: get(spotifyAlbum, 'uri'),
      externalUrl: get(spotifyAlbum, 'external_urls.spotify'),
      images: get(spotifyAlbum, 'images'),
    }
    await album.update({ spotifyData })
  }

  async updateBandSpotifyData(band, spotifyBand) {
    const spotifyData = {
      id: get(spotifyBand, 'id'),
      uri: get(spotifyBand, 'uri'),
      externalUrl: get(spotifyBand, 'external_urls.spotify'),
      images: get(spotifyBand, 'images'),
    }
    await band.update({ spotifyData })
  }
}

export default new SpotifyService()
