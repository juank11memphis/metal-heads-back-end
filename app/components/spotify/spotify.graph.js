import { graphHandler } from '../../core/middleware/graph-handler'

export const spotifyTypes = `
  type SpotifyDevice {
    id: String
    name: String
  }
`

export const spotifyQueryTypes = `
  loadSpotifyDevices: [SpotifyDevice]
  getAlbumSpotifyUri(albumId: String): String
`

export const spotifyQueriesResolvers = {
  loadSpotifyDevices: async (_, args, { spotifyService, token }) => {
    return graphHandler({
      func: spotifyService.loadPlayerDevices,
      funcName: 'spotifyService -> loadPlayerDevices',
      auth: { token },
    })
  },
  getAlbumSpotifyUri: async (_, { albumId }, { spotifyService, token }) => {
    return graphHandler({
      func: spotifyService.getAlbumSpotifyUri,
      funcName: 'spotifyService -> getAlbumSpotifyUri',
      args: [albumId],
      auth: { token },
    })
  },
}

export const spotifyMutationTypes = `
  playAlbum(deviceId: String, albumId: String): Boolean
`

export const spotifyMutationsResolvers = {
  playAlbum: async (_, { deviceId, albumId }, { spotifyService, token }) => {
    return graphHandler({
      func: spotifyService.playAlbum,
      funcName: 'spotifyService -> playAlbum',
      args: [deviceId, albumId],
      auth: { token },
    })
  },
}
