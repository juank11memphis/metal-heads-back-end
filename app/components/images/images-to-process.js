import mongoose from 'mongoose'

const Schema = mongoose.Schema

const ImagesToProcessSchema = new Schema(
  {
    band: {
      type: Schema.Types.ObjectId,
      ref: 'Band',
    },
  },
  {
    timestamps: true,
    collection: 'imagestoprocess',
  },
)

export default mongoose.models.ImagesToProcess ||
  mongoose.model('ImagesToProcess', ImagesToProcessSchema)
