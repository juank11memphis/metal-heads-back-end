import ImagesToProcess from './images-to-process'

export class ImagesToProcessDao {
  removeByBand(dbBand) {
    return ImagesToProcess.remove({ band: dbBand })
  }

  async markBandForImageProccessing(band) {
    const bands = await ImagesToProcess.find({ band })
    if (bands.length > 0) {
      return
    }
    return ImagesToProcess.create({ band: band })
  }

  findLatestAdded() {
    return ImagesToProcess.findOne({})
      .populate('band', { _id: 1, name: 1 })
      .sort({
        createdAt: -1,
      })
  }
}

export default new ImagesToProcessDao()
