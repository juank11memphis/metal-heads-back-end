import imagesToProcessDao from './images-to-process.dao'
import bandImagesEtl from '../../data/etl/bands/band-images.etl'

export class ImagesToProcessService {
  async processImages() {
    const imageToProcess = await imagesToProcessDao.findLatestAdded()
    if (imageToProcess === null) {
      console.log('no images to process at the moment...') // eslint-disable-line no-console
      return false
    }
    console.log('band images processing started...', imageToProcess.band.name) // eslint-disable-line no-console

    await bandImagesEtl.start(imageToProcess.band._id)

    console.log('band images processing finished...', imageToProcess.band.name) // eslint-disable-line no-console
    return true
  }
}

export default new ImagesToProcessService()
