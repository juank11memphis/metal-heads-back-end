export const getRolesMapAsArray = artist => {
  const rolesMap = artist.roles
  if (rolesMap === null || typeof rolesMap !== 'object') {
    return []
  }
  const result = Object.keys(rolesMap).map(bandId => ({
    bandId,
    role: rolesMap[bandId],
  }))
  return result
}
