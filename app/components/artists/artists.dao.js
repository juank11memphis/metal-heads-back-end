import Artist from './artist'
import elasticsearchApi from '../../data/api/elasticsearch.api'
import { generalLogger } from '../../util/logs'
import * as dbUtil from '../../util/db.util'

export class ArtistsDao {
  getNewArtistInstance(data) {
    return new Artist(data)
  }

  async create(data) {
    const dbArtist = await Artist.create(data)
    await elasticsearchApi.bulkArtists([dbArtist])
    return dbArtist
  }

  async update(dbArtist, updateData) {
    await dbArtist.update(updateData)
    const dbArtistUpdated = await this.getDetail(dbArtist._id)
    await elasticsearchApi.bulkArtists([dbArtistUpdated], 'update')
    return dbArtistUpdated
  }

  async inserMany(artists) {
    const artistsAdded = await Artist.insertMany(artists)
    await elasticsearchApi.bulkArtists(artistsAdded)
    return artistsAdded
  }

  async updateMany(artists) {
    const artistsUpdated = await dbUtil.updateMany(Artist, artists)
    await elasticsearchApi.bulkArtists(artistsUpdated, 'update')
    return artistsUpdated
  }

  getDetail(artistId) {
    return Artist.findById(artistId)
  }

  findByIds(ids) {
    return Artist.find({
      _id: {
        $in: ids,
      },
    })
  }

  findOne(query) {
    return Artist.findOne(query)
  }

  async storeAllInElasticsearch(action = 'index') {
    const allArtists = await Artist.find({})
    generalLogger.info('Fetching all artists...', allArtists.length)
    await elasticsearchApi.bulkArtists(allArtists, action)
    generalLogger.info('All artists inserted in elasticsearch...')
  }
}

export default new ArtistsDao()
