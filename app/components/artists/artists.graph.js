import * as DataUtil from '../../util/data.util'
import { getRolesMapAsArray } from './artists.util'
import { graphHandler } from '../../core/middleware/graph-handler'

export const artistType = `
  type ArtistBand {
    id: String
    name: String
    role: String
  }

  type ArtistRole {
    bandId: String
    role: String
  }

  type Artist {
    id: String!
    name: String
    picture: String
    roles: [ArtistRole]
    activeBands: [ArtistBand]
    pastBands: [ArtistBand]
    realName: String
    placeOfOrigin: String
    age: String
    gender: String
  }
`

export const artistTypeResolver = {
  id: artist => artist._id,
  picture: artist => DataUtil.getArtistPictureUrl(artist),
  roles: artist => getRolesMapAsArray(artist),
}

export const artistsQueryTypes = `
  loadArtistDetails(artistId: String): Artist
`

export const artistQueriesResolvers = {
  loadArtistDetails: (_, { artistId }, { artistsService }) => {
    return graphHandler({
      func: artistsService.loadArtistById,
      funcName: 'artistsService -> loadArtistById',
      args: [artistId],
    })
  },
}
