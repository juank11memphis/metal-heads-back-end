import { keyBy } from 'lodash'

import ArtistETL from '../../data/etl/artists/artist.etl'
import artistSyncETL from '../../data/etl/artists/artist-sync.etl'
import * as DataUtil from '../../util/data.util'
import artistsDao from './artists.dao'

export class ArtistsService {
  loadByIds = async ids => {
    const artists = await artistsDao.findByIds(ids)
    const artistsById = keyBy(artists, '_id')
    return ids.map(artistId => artistsById[artistId])
  }

  loadArtistById = async artistId => {
    let artist = await artistsDao.getDetail(artistId)
    if (!artist.dataExtracted) {
      artist = await ArtistETL.start(artist)
    } else if (artist.dataExtracted && DataUtil.needsSync(artist)) {
      artistSyncETL.start(artist)
    }
    return artist
  }
}

export default new ArtistsService()
