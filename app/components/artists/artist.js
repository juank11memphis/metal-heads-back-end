import mongoose from 'mongoose'

const Schema = mongoose.Schema

const ArtistSchema = new Schema(
  {
    name: String,
    metalArchivesId: String,
    realName: String,
    age: String,
    placeOfOrigin: String,
    gender: String,
    picture: String,
    biography: String,
    activeBands: Array,
    pastBands: Array,
    dataExtracted: {
      type: Boolean,
      default: false,
    },
    lastExtractionDate: Date,
    roles: {}, // Maps bandId with roles for that band
  },
  {
    timestamps: true,
  },
)

export default mongoose.models.Artist || mongoose.model('Artist', ArtistSchema)
