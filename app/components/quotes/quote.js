import mongoose from 'mongoose'

const Schema = mongoose.Schema

const QuoteSchema = new Schema(
  {
    text: String,
    author: {
      type: String,
      default: 'Unknown',
    },
    status: {
      type: String,
      default: 'inactive',
    },
    quoteIndex: Number,
  },
  {
    timestamps: true,
  },
)

export default mongoose.models.Quote || mongoose.model('Quote', QuoteSchema)
