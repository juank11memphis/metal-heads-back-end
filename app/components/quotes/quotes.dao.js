import Quote from './quote'

export class QuotesDao {
  createQuote(data) {
    const newQuote = new Quote(data)
    return newQuote.save()
  }

  updateQuote(quote, data) {
    return quote.update(data)
  }

  findActiveQuote() {
    return Quote.findOne({ status: 'active' })
  }

  findDuplicate(quote) {
    return Quote.findOne({ text: quote.text })
  }

  findByQuoteIndex(quoteIndex) {
    return Quote.findOne({ quoteIndex })
  }

  async findMaxQuoteIndex() {
    const lastInsertedQuote = await Quote.find({})
      .sort({ quoteIndex: -1 })
      .limit(1)
    if (!lastInsertedQuote || lastInsertedQuote.length === 0) {
      return -1
    }
    return lastInsertedQuote[0].quoteIndex
  }
}

export default new QuotesDao()
