import AppError from '../../core/app.error'
import quotesDao from './quotes.dao'

export class QuotesService {
  loadActiveQuote() {
    return quotesDao.findActiveQuote()
  }

  async createQuote(quote) {
    const duplicateQuote = await quotesDao.findDuplicate(quote)
    if (duplicateQuote) {
      throw new AppError('Quote already exists')
    }

    const quoteData = { ...quote }
    const maxQuoteIndex = await quotesDao.findMaxQuoteIndex()
    if (maxQuoteIndex === -1) {
      quoteData.status = 'active'
    }
    return quotesDao.createQuote({
      ...quoteData,
      quoteIndex: maxQuoteIndex + 1,
    })
  }

  async setNextActiveQuote() {
    const activeQuote = await quotesDao.findActiveQuote()
    if (!activeQuote) {
      return null
    }
    await quotesDao.updateQuote(activeQuote, { status: 'inactive' })
    let nextActiveQuote = await quotesDao.findByQuoteIndex(
      activeQuote.quoteIndex + 1,
    )
    if (!nextActiveQuote) {
      nextActiveQuote = await quotesDao.findByQuoteIndex(0)
    }
    await quotesDao.updateQuote(nextActiveQuote, { status: 'active' })
    return true
  }
}

export default new QuotesService()
