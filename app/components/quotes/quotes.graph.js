import { graphHandler } from '../../core/middleware/graph-handler'

const CREATE_QUOTE_PERMISSION = 'create:quote'

export const quoteType = `
  type Quote {
    text: String,
    author: String,
  }

  input QuoteInput {
    text: String
    author: String
  }
`

export const quotesQueryTypes = `
  loadActiveQuote: Quote
`

export const quoteQueriesResolvers = {
  loadActiveQuote: (_, args, { quotesService }) => {
    return graphHandler({
      func: quotesService.loadActiveQuote,
      funcName: 'quotesService -> loadActiveQuote',
    })
  },
}

export const quotesMutationTypes = `
  createQuote(quote: QuoteInput): Quote
`

export const quoteMutationsResolvers = {
  createQuote: async (_, { quote }, { quotesService, token }) => {
    return graphHandler({
      func: quotesService.createQuote,
      funcName: 'quotesService -> createQuote',
      args: [quote],
      auth: {
        token,
        permission: CREATE_QUOTE_PERMISSION,
      },
      sendUserId: false,
    })
  },
}
