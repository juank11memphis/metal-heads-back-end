import bandsDao from '../components/bands/bands.dao'
import bandsService from '../components/bands/bands.service'
import { generalLogger, errorsLogger } from '../util/logs'
import { sleep } from '../util/general.util'

export default function(agenda) {
  agenda.define('prefill-bands', async function(job, done) {
    try {
      generalLogger.info('Prefill Bands Started')
      const begin = Date.now()

      const bandsNotExtracted = await bandsDao.find(
        { dataExtracted: false, extractionFailed: false },
        40,
      )

      for (const band of bandsNotExtracted) {
        try {
          await bandsService.loadBandDetails(band._id)
          await sleep(2000)
        } catch (error) {
          await bandsDao.updateBand(band, { extractionFailed: true })
          errorsLogger.error(
            `An error occurred pre filling band ${band._id} - ${band.name}`,
            error,
          )
        }
      }

      const end = Date.now()
      const timeSpent = (end - begin) / 1000 / 60
      generalLogger.info(`Prefill Bands Finished in ${timeSpent} minutes`)
    } catch (error) {
      errorsLogger.error(
        'Unexpected error occurred pre filling bands data',
        error,
      )
    }
    done()
  })

  agenda.on('ready', () => {
    agenda.every('5 minutes', 'prefill-bands')
  })
}
