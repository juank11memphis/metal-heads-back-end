import maintenanceController from '../components/maintenance/maintenance.service'
import { errorsLogger, generalLogger } from '../util/logs'

const run = async () => {
  try {
    await maintenanceController.fillElasticSearchData()
    generalLogger.info('Fill elasticsearch data job success!!')
  } catch (error) {
    errorsLogger.error(
      'Unexpected error occurred filling elasticsearch data',
      error,
    )
  }
}

export default run
