import dataLogger from '../util/logs/data.logger'
import imagesToProcessService from '../components/images/images-to-process.service'

export default function(agenda) {
  agenda.define('process-band-images', async function(job, done) {
    try {
      await imagesToProcessService.processImages()
    } catch (error) {
      dataLogger.error(
        'A critic error occurred processing images for band',
        error,
      )
    }
    done()
  })

  agenda.on('ready', () => {
    agenda.every('5 minutes', 'process-band-images')
  })
}
