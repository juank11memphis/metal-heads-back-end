process.env.NODE_ENV = process.env.NODE_ENV || 'development'

require('dotenv').config()

require('@babel/register')

const mongoose = require('mongoose')
const Constants = require('../core/constants').default
const run = require('./fill-elasticsearch-data.job').default

mongoose.connect(
  Constants.mongo.uri,
  {
    socketTimeoutMS: 10000,
    useNewUrlParser: true,
  },
)
mongoose.connection
  // eslint-disable-next-line no-console
  .once('open', function() {
    console.log('Successfully connected to MongDB!')
    run()
  })
  .on('error', function(error) {
    throw err
  })
