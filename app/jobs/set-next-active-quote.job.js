import dataLogger from '../util/logs/data.logger'
import quotesService from '../components/quotes/quotes.service'

export default function(agenda) {
  agenda.define('set-next-active-quote', async function(job, done) {
    try {
      await quotesService.setNextActiveQuote()
    } catch (error) {
      dataLogger.error(
        'A critic error occurred setting next active quote',
        error,
      )
    }
    done()
  })

  agenda.on('ready', () => {
    agenda.every('3 days', 'set-next-active-quote')
  })
}
