import getNewAdditionsETL from '../data/etl/new-additions/get-new-additions.etl'

export default function(agenda) {
  agenda.define('get-new-additions', async function(job, done) {
    try {
      // eslint-disable-next-line no-console
      console.log('job get-new-additions started...')

      await getNewAdditionsETL.start()

      // eslint-disable-next-line no-console
      console.log('job get-new-additions finished...')
    } catch (error) {
      // eslint-disable-next-line no-console
      console.log('A critic error occurred getting new additions', error)
    }
    done()
  })

  agenda.on('ready', () => {
    agenda.every('6 days', 'get-new-additions')
  })
}
