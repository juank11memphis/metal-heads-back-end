import albumsDao from '../components/albums/albums.dao'
import albumsService from '../components/albums/albums.service'
import { generalLogger, errorsLogger } from '../util/logs'
import { sleep } from '../util/general.util'

export default function(agenda) {
  agenda.define('prefill-albums', async function(job, done) {
    try {
      generalLogger.info('Prefill Albums Started')
      const begin = Date.now()

      const albumsNotExtracted = await albumsDao.find(
        { dataExtracted: false, extractionFailed: false },
        40,
      )

      for (const dbAlbum of albumsNotExtracted) {
        try {
          await albumsService.loadAlbumById(dbAlbum._id)
          await sleep(2000)
        } catch (error) {
          await albumsDao.update(dbAlbum, { extractionFailed: true })
          errorsLogger.error(
            `An error occurred pre filling album ${dbAlbum._id} - ${
              dbAlbum.name
            }`,
            error,
          )
        }
      }

      const end = Date.now()
      const timeSpent = (end - begin) / 1000 / 60
      generalLogger.info(`Prefill Albums Finished in ${timeSpent} minutes`)
    } catch (error) {
      errorsLogger.error(
        'Unexpected error occurred pre filling albums data',
        error,
      )
    }
    done()
  })

  agenda.on('ready', () => {
    agenda.every('5 minutes', 'prefill-albums')
  })
}
