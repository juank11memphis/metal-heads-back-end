import cheerio from 'cheerio'
import uuidv1 from 'uuid/v1'

import * as WebScraperUtil from './util.webscraper'
import doRequest from '../../util/request.util'

export class BlabbermouthWebScraper {
  NEWS_URL = 'http://www.blabbermouth.net/news'

  async scrapNews(providedHtml) {
    // eslint-disable-next-line no-console
    console.log('Scraping news from Blabbemouth...')

    const news = []
    const html = providedHtml
      ? providedHtml
      : await doRequest(WebScraperUtil.getRequestOptions(this.NEWS_URL))
    const $ = cheerio.load(html)
    $('#content article').each(function(index, element) {
      let newsLink = $(this).children('header')
      newsLink = newsLink.children('h1')
      newsLink = newsLink.children().first()
      news.push({
        id: uuidv1(),
        title: newsLink.text().trim(),
        link: `http://www.blabbermouth.net${newsLink.attr('href')}`,
      })
    })

    // eslint-disable-next-line no-console
    console.log('Scraping news from Blabbemouth finished...')
    return news
  }
}

export default new BlabbermouthWebScraper()
