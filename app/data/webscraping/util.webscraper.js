export const USER_AGENT_NAME = 'MetalHeadsBot (http://metalheads.fm/)'

const getHeaders = () => {
  return {
    'User-Agent': USER_AGENT_NAME,
  }
}

export function getRequestOptions(url) {
  return {
    url,
    headers: getHeaders(),
  }
}
