import cheerio from 'cheerio'

import doRequest from '../../util/request.util'
import * as WebScraperUtil from './util.webscraper'

export class MetalArchivesAlbumWebScraper {
  GET_ALBUM_URL = 'https://www.metal-archives.com/albums'

  async scrapAlbumFullData(
    metalArchivesAlbumId,
    albumName,
    bandName,
    providedHtml,
  ) {
    const urlBandName = bandName.split(' ').join('_')
    const urlAlbumName = albumName.split(' ').join('_')
    const url = `${
      this.GET_ALBUM_URL
    }/${urlBandName}/${urlAlbumName}/${metalArchivesAlbumId}`

    const html = providedHtml
      ? providedHtml
      : await doRequest(WebScraperUtil.getRequestOptions(url))

    const $ = cheerio.load(html)
    const albumData = {}
    Object.assign(
      albumData,
      this.scrapAlbumGeneralInfo($),
      this.scrapAlbumPicture($),
      this.scrapAlbumLineup($),
      this.scrapAlbumSongs($),
    )
    return albumData
  }

  scrapAlbumGeneralInfo($) {
    const leftInfo = $('#album_info .float_left dd')
      .map(function(index, el) {
        return $(this)
          .text()
          .trim()
      })
      .get()
    const rightInfo = $('#album_info .float_right dd')
      .map(function(index, el) {
        return $(this)
          .text()
          .trim()
      })
      .get()
    return {
      releaseDate: leftInfo[1],
      catalogId: leftInfo[2],
      label: rightInfo[0],
      format: rightInfo[1],
      reviews: rightInfo[2],
    }
  }

  scrapAlbumPicture($) {
    return {
      picture: $('#album_sidebar .album_img .image img').attr('src'),
    }
  }

  scrapAlbumLineup($) {
    const lineUp = $('#album_members_lineup .lineupRow')
      .map(function(index, el) {
        const nameEl = $(this).find('td a')
        const name = nameEl.text().trim()
        const artistLink = nameEl.attr('href')
        const metalArchivesId = artistLink.substring(
          artistLink.lastIndexOf('/') + 1,
          artistLink.length,
        )
        const info = $(this)
          .find('td')
          .map(function(index, el) {
            return $(this)
              .text()
              .trim()
          })
          .get()
        const roles = info[1]
        return {
          name,
          metalArchivesId,
          roles,
        }
      })
      .get()
    return {
      lineUp,
    }
  }

  scrapAlbumSongs($) {
    let discRowsCount = 0
    let sideRowsCount = 0
    const songsRows = $('#album_tabs_tracklist table tr')
    const songs = []
    for (let index = 0, length = songsRows.length; index < length; index++) {
      const songRowEl = songsRows[index]
      const rowClass = $(songRowEl).attr('class')
      if (rowClass === 'displayNone') {
        continue
      }
      if (rowClass === 'discRow') {
        discRowsCount++
        sideRowsCount = 0
        songs.push({
          title: `Disc ${discRowsCount}`,
        })
        continue
      }
      if (rowClass === 'sideRow') {
        sideRowsCount++
        songs.push({
          title: `Side ${sideRowsCount}`,
        })
        continue
      }
      const songData = this.scrapSongData($, songRowEl)
      songs.push(songData)
    }
    return {
      songs,
    }
  }

  scrapSongData($, songRowEl) {
    const songColumns = $(songRowEl).find('td')
    const indexColumn = $(songColumns[0])
      .text()
      .trim()
    const titleColumn = $(songColumns[1])
      .text()
      .trim()
    const lengthColumn = $(songColumns[2])
      .text()
      .trim()
    if (indexColumn === '') {
      return {
        length: titleColumn,
      }
    }
    return {
      title: `${indexColumn} ${titleColumn}`,
      length: lengthColumn,
    }
  }
}

export default new MetalArchivesAlbumWebScraper()
