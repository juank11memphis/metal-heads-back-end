import cheerio from 'cheerio'

import doRequest from '../../util/request.util'
import { logBandWebscrapingStart } from '../../util/logs/bands.logger'
import * as WebScraperUtil from './util.webscraper'

export class MetalArchivesBandWebScraper {
  GET_BAND_URL = 'https://www.metal-archives.com/bands/'
  GET_BAND_DISCOGRAPHY_URL =
    'https://www.metal-archives.com/band/discography/id/'

  async scrapBandFullData(metalArchivesBandId, bandName, providedHtmls) {
    const url = `${this.GET_BAND_URL}${bandName
      .split(' ')
      .join('_')}/${metalArchivesBandId}`
    const discographyUrl = `${
      this.GET_BAND_DISCOGRAPHY_URL
    }${metalArchivesBandId}/tab/all`

    logBandWebscrapingStart(bandName, url, discographyUrl)

    const [bandHtml, discographyHtml] = providedHtmls
      ? providedHtmls
      : await Promise.all([
          doRequest(WebScraperUtil.getRequestOptions(url)),
          doRequest(WebScraperUtil.getRequestOptions(discographyUrl)),
        ])
    const $ = cheerio.load(bandHtml)
    const bandData = {}
    Object.assign(
      bandData,
      this.scrapBandGeneralInfo($),
      this.scrapBandPicture($),
      this.scrapBandCurrentMembers($),
      this.scrapBandDiscography(discographyHtml),
    )
    return bandData
  }

  scrapBandGeneralInfo($) {
    const leftInfo = $('#band_stats .float_left dd')
      .map(function(index, el) {
        return $(this)
          .text()
          .trim()
      })
      .get()
    const rightInfo = $('#band_stats .float_right dd')
      .map(function(index, el) {
        return $(this)
          .text()
          .trim()
      })
      .get()
    const bottomInfo = $('#band_stats .clear dd')
      .map(function(index, el) {
        return $(this)
          .text()
          .trim()
      })
      .get()
    return {
      location: leftInfo[1],
      status: leftInfo[2],
      formedIn: leftInfo[3],
      genre: rightInfo[0],
      lyricalThemes: rightInfo[1],
      yearsActive: bottomInfo[0],
    }
  }

  scrapBandPicture($) {
    return {
      picture: $('#band_sidebar .band_img .image img').attr('src'),
    }
  }

  scrapBandCurrentMembers($) {
    const currentMembers = $('#band_tab_members_current .lineupRow')
      .map(function(index, el) {
        const nameEl = $(this).find('td a')
        const name = nameEl.text().trim()
        const artistLink = nameEl.attr('href')
        const metalArchivesId = artistLink.substring(
          artistLink.lastIndexOf('/') + 1,
          artistLink.length,
        )
        const info = $(this)
          .find('td')
          .map(function(index, el) {
            return $(this)
              .text()
              .trim()
          })
          .get()
        const roles = info[1]
        return {
          name,
          metalArchivesId,
          roles,
        }
      })
      .get()
    return {
      currentMembers,
    }
  }

  scrapBandDiscography(discographyHtml) {
    const $ = cheerio.load(discographyHtml)
    const albums = $('tbody tr')
      .map(function(index, el) {
        const rows = $(this)
          .find('td')
          .toArray()
        rows.pop()
        if (!rows || rows.length === 0) {
          return null
        }
        const nameEl = $(rows[0]).find('a')
        const name = nameEl.text().trim()
        const albumLink = nameEl.attr('href')
        const metalArchivesId = albumLink.substring(
          albumLink.lastIndexOf('/') + 1,
          albumLink.length,
        )
        const type = $(rows[1])
          .text()
          .trim()
        const year = $(rows[2])
          .text()
          .trim()
        return {
          name,
          metalArchivesId,
          type,
          year,
        }
      })
      .get()
    return {
      albums,
    }
  }
}

export default new MetalArchivesBandWebScraper()
