import cheerio from 'cheerio'

import * as WebScraperUtil from './util.webscraper'
import doRequest from '../../util/request.util'

export class MetalArchivesWebScraper {
  BAND_SIMILAR_BANDS_URL =
    'https://www.metal-archives.com/band/ajax-recommendations/id/'
  GET_ARTIST_URL = 'http://www.metal-archives.com/artists/'

  async scrapBandSimilarBandsIds(metalArchivesBandId, providedHtml) {
    const similarBandsIds = []
    const url = `${this.BAND_SIMILAR_BANDS_URL}${metalArchivesBandId}`
    const html = providedHtml
      ? providedHtml
      : await doRequest(WebScraperUtil.getRequestOptions(url))
    const $ = cheerio.load(html)
    $('#artist_list tr').each((index, element) => {
      let id = $(element).attr('id')
      if (id) {
        id = id.substring(id.indexOf('_') + 1, id.length)
        similarBandsIds.push(id)
      }
    })
    return similarBandsIds.slice(0, 10)
  }

  async scrapArtistFullData(name, metalArchivesId, providedHtml) {
    const url = `${this.GET_ARTIST_URL}${name
      .split(' ')
      .join('_')}/${metalArchivesId}`
    const html = providedHtml
      ? providedHtml
      : await doRequest(WebScraperUtil.getRequestOptions(url))
    const newArtist = {}
    const $ = cheerio.load(html)
    this.scrapArtistGeneralInfo(newArtist, $)
    newArtist.activeBands = this.scrapArtistBands($, 'artist_tab_active')
    newArtist.pastBands = this.scrapArtistBands($, 'artist_tab_past')
    return newArtist
  }

  scrapArtistGeneralInfo(newArtist, $) {
    const generalInfo = $('dd')
      .map(function(index, el) {
        return $(this)
          .text()
          .trim()
      })
      .get()
    newArtist.realName = generalInfo[0]
    newArtist.age = generalInfo[1]
    newArtist.placeOfOrigin = generalInfo[2]
    newArtist.gender = generalInfo[3]
    newArtist.picture = $('#artist').attr('href')
  }

  scrapArtistBands($, bandsContainerId) {
    const bands = []
    $(`#${bandsContainerId} .ui-tabs-panel-content .member_in_band`).map(
      function(index, el) {
        const band = {}
        const bandNameEl = $(this).children('.member_in_band_name')
        const bandLinkEl = bandNameEl
          .children()
          .first()
          .attr('href')
        band.name = bandNameEl.text().trim()
        if (bandLinkEl) {
          band.metalArchivesId = bandLinkEl.substring(
            bandLinkEl.lastIndexOf('/') + 1,
            bandLinkEl.lastIndexOf('#'),
          )
        }
        band.role = $(this)
          .children('.member_in_band_role')
          .text()
          .trim()
        bands.push(band)
      },
    )
    return bands
  }
}

export default new MetalArchivesWebScraper()
