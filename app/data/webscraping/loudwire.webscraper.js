import cheerio from 'cheerio'
import uuidv1 from 'uuid/v1'

import * as WebScraperUtil from './util.webscraper'
import doRequest from '../../util/request.util'

export class LoudwireWebScraper {
  NEWS_URL = 'http://loudwire.com/category/metal/'

  async scrapNews(providedHtml) {
    // eslint-disable-next-line no-console
    console.log('Scraping news from Loudwire...')

    const news = []
    const html = providedHtml
      ? providedHtml
      : await doRequest(WebScraperUtil.getRequestOptions(this.NEWS_URL))
    const $ = cheerio.load(html)
    $('.main-content article').each(function(index, element) {
      let newsLink = $(this).children('.content')
      newsLink = newsLink.children('.title')
      news.push({
        id: uuidv1(),
        title: newsLink.text().trim(),
        link: newsLink.attr('href'),
      })
    })

    // eslint-disable-next-line no-console
    console.log('Scraping news from Loudwire finished...')
    return news
  }
}

export default new LoudwireWebScraper()
