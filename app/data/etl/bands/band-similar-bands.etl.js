import metalArchivesWebScraper from '../../webscraping/metal-archives.webscraper'
import { logBandEvent } from '../../../util/logs/bands.logger'
import bandsDao from '../../../components/bands/bands.dao'

/* eslint-disable no-console */
export class BandSimilarBandsETL {
  async start(databaseBand) {
    logBandEvent('Similar Bands ETL Started', databaseBand)

    const similarBandsIds = await metalArchivesWebScraper.scrapBandSimilarBandsIds(
      databaseBand.metalArchivesId,
    )
    const similarBands = await bandsDao.find({
      metalArchivesId: { $in: similarBandsIds },
    })
    await databaseBand.update({
      similarBands,
      similarBandsExtracted: true,
    })

    logBandEvent('Similar Bands ETL Finished', databaseBand)
    return similarBands
  }
}

export default new BandSimilarBandsETL()
