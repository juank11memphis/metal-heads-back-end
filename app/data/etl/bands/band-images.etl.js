import awsS3Api from '../../api/aws-s3.api'
import dataLogger from '../../../util/logs/data.logger'
import * as DataUtil from '../../../util/data.util'
import bandsDao from '../../../components/bands/bands.dao'
import albumsDao from '../../../components/albums/albums.dao'
import artistsDao from '../../../components/artists/artists.dao'
import imagesToProcessDao from '../../../components/images/images-to-process.dao'

export class BandImagesETL {
  async start(bandId) {
    let databaseBand = await bandsDao.getFullDetail(bandId)
    dataLogger.info('Images ETL started', {
      bandId: databaseBand._id,
      bandName: databaseBand.name,
    })
    databaseBand = await this.extractBandImages(databaseBand)
    databaseBand = await this.extractAlbumsImages(databaseBand)
    databaseBand = await this.extractArtistsImages(databaseBand)
    databaseBand = await this.removeImageToProcessRecord(databaseBand)
    return true
  }

  async extractBandImages(databaseBand) {
    if (
      DataUtil.isImageAlreadyInS3(databaseBand.picture) ||
      this.isImageNotAvailable(databaseBand)
    ) {
      dataLogger.info('Band image already in S3', {
        artistName: databaseBand.name,
        picture: databaseBand.picture,
      })
      return databaseBand
    }
    databaseBand.picture = this.needToGenerateImage(databaseBand)
      ? DataUtil.generateBandPictureUrl(databaseBand)
      : databaseBand.picture
    const imagePath = awsS3Api.generateS3ImagePath(databaseBand, 'band')
    try {
      const s3ImageUrl = await awsS3Api.downloadAndUploadImage(
        databaseBand.picture,
        imagePath,
      )
      await bandsDao.updateBand(databaseBand, { picture: s3ImageUrl })
    } catch (error) {
      this.logError('Error downloading Band image', error, databaseBand, 'band')
    }
    return databaseBand
  }

  async extractAlbumsImages(databaseBand, imagesIndex = 0) {
    if (!databaseBand.albums || databaseBand.albums.length === 0) {
      return databaseBand
    }
    if (imagesIndex >= databaseBand.albums.length) {
      await this.updateAlbumsImagesUrls(databaseBand.albums)
      dataLogger.info('Albums images extracted', {
        bandId: databaseBand._id,
        bandName: databaseBand.name,
        albumsImagesCount: databaseBand.albums.length,
      })
      return databaseBand
    }
    const album = databaseBand.albums[imagesIndex]
    if (
      DataUtil.isImageAlreadyInS3(album.picture) ||
      this.isImageNotAvailable(album)
    ) {
      dataLogger.info('Album image already in S3', {
        artistName: album.name,
        picture: album.picture,
      })
      imagesIndex++
      return await this.extractAlbumsImages(databaseBand, imagesIndex)
    }
    album.picture = this.needToGenerateImage(album)
      ? DataUtil.generateAlbumPictureUrl(album)
      : album.picture
    const imagePath = awsS3Api.generateS3ImagePath(album, 'album')
    try {
      album.picture = await awsS3Api.downloadAndUploadImage(
        album.picture,
        imagePath,
      )
    } catch (error) {
      this.logError('Error downloading Album image', error, album, 'album')
    }
    imagesIndex++
    return await this.extractAlbumsImages(databaseBand, imagesIndex)
  }

  async updateAlbumsImagesUrls(albums) {
    const albumsToUpdate = albums.map(album => ({
      id: album._id,
      data: { picture: album.picture },
    }))
    await albumsDao.updateMany(albumsToUpdate)
  }

  async extractArtistsImages(databaseBand, imagesIndex = 0) {
    if (
      !databaseBand.currentMembers ||
      databaseBand.currentMembers.length === 0
    ) {
      return databaseBand
    }
    if (imagesIndex >= databaseBand.currentMembers.length) {
      await this.updateArtistsImagesUrls(databaseBand.currentMembers)
      dataLogger.info('Artists images extracted', {
        bandId: databaseBand._id,
        bandName: databaseBand.name,
        artistsImagesCount: databaseBand.currentMembers.length,
      })
      dataLogger.info('Images ETL finished', {
        bandId: databaseBand._id,
        bandName: databaseBand.name,
      })
      return databaseBand
    }
    const artist = databaseBand.currentMembers[imagesIndex]
    if (
      DataUtil.isImageAlreadyInS3(artist.picture) ||
      this.isImageNotAvailable(artist)
    ) {
      dataLogger.info('Artist image already in S3', {
        artistName: artist.name,
        picture: artist.picture,
      })
      imagesIndex++
      return await this.extractArtistsImages(databaseBand, imagesIndex)
    }
    artist.picture = this.needToGenerateImage(artist)
      ? DataUtil.generateArtistPictureUrl(artist)
      : artist.picture
    const imagePath = awsS3Api.generateS3ImagePath(artist, 'artist')
    try {
      artist.picture = await awsS3Api.downloadAndUploadImage(
        artist.picture,
        imagePath,
      )
    } catch (error) {
      this.logError('Error downloading Artist image', error, artist, 'artist')
    }
    imagesIndex++
    return await this.extractArtistsImages(databaseBand, imagesIndex)
  }

  async updateArtistsImagesUrls(artists) {
    const artistsToUpdate = artists.map(artist => ({
      id: artist._id,
      data: { picture: artist.picture },
    }))
    await artistsDao.updateMany(artistsToUpdate)
  }

  async removeImageToProcessRecord(databaseBand, callbacks) {
    await imagesToProcessDao.removeByBand(databaseBand)
    return databaseBand
  }

  isImageNotAvailable(item) {
    if (item.dataExtracted && !item.picture) {
      return true
    }
    return false
  }

  needToGenerateImage(item) {
    if (!item.picture || !item.dataExtracted) {
      return true
    }
    return false
  }

  logError(message, error, item, type) {
    const errorData = {
      id: item._id,
      name: item.name,
      type: type,
      imageUrl: item.picture,
      error: error,
    }
    dataLogger.error(message, errorData)
    return { message, errorData }
  }
}

export default new BandImagesETL()
