import metalArchivesBandWebScraper from '../../webscraping/metal-archives-band.webscraper'
import * as DataUtil from '../../../util/data.util'
import { logBandEvent } from '../../../util/logs/bands.logger'
import bandsDao from '../../../components/bands/bands.dao'
import artistsDao from '../../../components/artists/artists.dao'
import albumsDao from '../../../components/albums/albums.dao'

/* eslint-disable no-console */
export class BandETL {
  async start(databaseBand) {
    logBandEvent('Band ETL Started', databaseBand)

    let bandData = await this.extractBandData(databaseBand)

    bandData = await this.transformBandData(databaseBand, bandData)

    await this.loadBandData(databaseBand, bandData)

    const responseBand = await bandsDao.getDetail(databaseBand._id)

    logBandEvent('Band ETL Finished', databaseBand)
    return responseBand
  }

  async extractBandData(databaseBand) {
    const bandData = await metalArchivesBandWebScraper.scrapBandFullData(
      databaseBand.metalArchivesId,
      databaseBand.name,
    )
    return bandData
  }

  async transformBandData(databaseBand, metalArchivesBand) {
    const newBandData = {
      dataExtracted: true,
      lastExtractionDate: new Date(),
    }
    Object.assign(
      newBandData,
      this.transformBandGeneralInfo(metalArchivesBand),
      this.transformBandAlbums(metalArchivesBand, databaseBand),
      await this.transformBandMembers(databaseBand, metalArchivesBand),
    )
    return newBandData
  }

  transformBandGeneralInfo(metalArchivesBand) {
    return {
      formedIn: metalArchivesBand.formedIn,
      yearsActive: metalArchivesBand.yearsActive,
      lyricalThemes: metalArchivesBand.lyricalThemes,
      status: metalArchivesBand.status,
      location: metalArchivesBand.location,
      genre: metalArchivesBand.genre,
      picture: metalArchivesBand.picture,
    }
  }

  transformBandAlbums(metalArchivesBand, databaseBand) {
    let bandData = {
      albums: [...databaseBand.albums],
      albumsNotInDB: [],
    }
    const databaseAlbumsMap = {}
    for (const databaseAlbum of databaseBand.albums) {
      databaseAlbumsMap[databaseAlbum.metalArchivesId] = true
    }
    for (const metalArchivesAlbum of metalArchivesBand.albums) {
      const metalArchivesAlbumId = metalArchivesAlbum.metalArchivesId
      if (!databaseAlbumsMap[metalArchivesAlbumId]) {
        const newAlbum = this.transformBandAlbum(
          metalArchivesAlbum,
          databaseBand,
        )
        bandData.albums.push(newAlbum)
        bandData.albumsNotInDB.push(newAlbum)
      }
    }
    return bandData
  }

  transformBandAlbum(metalArchivesAlbum, databaseBand) {
    let albumData = {}
    Object.assign(albumData, metalArchivesAlbum)
    albumData.band = databaseBand
    const newAlbum = albumsDao.getNewAlbumInstance(albumData)
    newAlbum.picture = DataUtil.generateAlbumPictureUrl(newAlbum)
    return newAlbum
  }

  async transformBandMembers(databaseBand, metalArchivesBand) {
    let bandData = { currentMembers: [], membersNotInDB: [] }
    metalArchivesBand.currentMembers = metalArchivesBand.currentMembers
      ? metalArchivesBand.currentMembers
      : []
    for (const metalArchivesArtist of metalArchivesBand.currentMembers) {
      const databaseArtist = await artistsDao.findOne({
        metalArchivesId: metalArchivesArtist.metalArchivesId,
      })
      let artistData = {}
      let artistRolesMap
      Object.assign(artistData, metalArchivesArtist)
      if (databaseArtist) {
        artistRolesMap = databaseArtist.roles || {}
        artistRolesMap =
          typeof artistRolesMap === 'object' ? artistRolesMap : {}
        artistRolesMap[databaseBand._id] = metalArchivesArtist.roles
        artistData.roles = artistRolesMap
        artistData.picture = DataUtil.generateArtistPictureUrl(databaseArtist)
        databaseArtist.picture = artistData.picture
        await artistsDao.update(databaseArtist, artistData)
        bandData.currentMembers.push(databaseArtist)
      } else {
        artistData.roles = {
          [databaseBand._id]: artistData.roles,
        }
        const newArtist = artistsDao.getNewArtistInstance(artistData)
        newArtist.picture = DataUtil.generateArtistPictureUrl(newArtist)
        bandData.currentMembers.push(newArtist)
        bandData.membersNotInDB.push(newArtist)
      }
    }
    return bandData
  }

  loadBandData = async (databaseBand, bandData) => {
    const membersNotInDB = [...bandData.membersNotInDB]
    const albumsNotInDB = [...bandData.albumsNotInDB]
    delete bandData.membersNotInDB
    delete bandData.albumsNotInDB

    await Promise.all([
      albumsDao.inserMany(albumsNotInDB),
      artistsDao.inserMany(membersNotInDB),
      bandsDao.updateBand(databaseBand, bandData),
    ])

    return true
  }
}

export default new BandETL()
