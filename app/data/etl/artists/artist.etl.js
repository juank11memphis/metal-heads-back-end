import metalArchivesWebScraper from '../../webscraping/metal-archives.webscraper'
import awsS3Api from '../../api/aws-s3.api'
import artistsDao from '../../../components/artists/artists.dao'
import bandsDao from '../../../components/bands/bands.dao'

/* eslint-disable no-console */
export class ArtistETL {
  async start(databaseArtist) {
    this.logStart(databaseArtist)

    let artistData = await this.extractArtistData(databaseArtist)
    artistData = await this.transformArtistData(databaseArtist, artistData)
    await this.loadArtistData(databaseArtist, artistData)

    this.logEnd(databaseArtist)
    return await artistsDao.getDetail(databaseArtist._id)
  }

  logStart(databaseArtist) {
    console.log(`
        Artist ETL Started.
        id: ${databaseArtist._id}
        metalArchivesId: ${databaseArtist.metalArchivesId}
        name: ${databaseArtist.name}
      `)
  }

  logEnd(databaseArtist) {
    console.log(`
        Artist ETL Finished.
        id: ${databaseArtist._id}
        metalArchivesId: ${databaseArtist.metalArchivesId}
        name: ${databaseArtist.name}
      `)
  }

  async extractArtistData(databaseArtist) {
    const artistData = await metalArchivesWebScraper.scrapArtistFullData(
      databaseArtist.name,
      databaseArtist.metalArchivesId,
    )
    return artistData
  }

  async transformArtistData(databaseArtist, artistData) {
    artistData.activeBands = await this.transformBands(
      databaseArtist,
      artistData.activeBands,
    )
    artistData.pastBands = await this.transformBands(
      databaseArtist,
      artistData.pastBands,
    )
    const imagePath = awsS3Api.generateS3ImagePath(databaseArtist, 'artist')
    if (artistData.picture) {
      artistData.picture = await awsS3Api.downloadAndUploadImage(
        artistData.picture,
        imagePath,
      )
    }
    artistData.dataExtracted = true
    artistData.lastExtractionDate = new Date()
    return artistData
  }

  async transformBands(databaseArtist, bands) {
    if (!bands) {
      return bands
    }
    for (const band of bands) {
      let databaseBand
      if (band.metalArchivesId) {
        databaseBand = await bandsDao.findOne({
          metalArchivesId: band.metalArchivesId,
        })
      }
      if (databaseBand) {
        band.id = databaseBand._id
      }
    }
    return bands
  }

  async loadArtistData(databaseArtist, artistData) {
    await artistsDao.update(databaseArtist, artistData)
    return true
  }
}

export default new ArtistETL()
