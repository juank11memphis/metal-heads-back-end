import { ArtistETL } from './artist.etl'

/* eslint-disable no-console */
class ArtistSyncETL extends ArtistETL {
  logStart(databaseArtist) {
    console.log(`
        Artist Sync ETL Started. 
        id: ${databaseArtist._id} 
        metalArchivesId: ${databaseArtist.metalArchivesId}
        name: ${databaseArtist.name}
      `)
  }

  logEnd(databaseArtist) {
    console.log(`
        Artist Sync ETL Finished. 
        id: ${databaseArtist._id} 
        metalArchivesId: ${databaseArtist.metalArchivesId}
        name: ${databaseArtist.name}
      `)
  }

  async transformArtistData(databaseArtist, artistData) {
    return {
      activeBands: await this.transformBands(
        databaseArtist,
        artistData.activeBands,
      ),
      pastBands: await this.transformBands(
        databaseArtist,
        artistData.pastBands,
      ),
      dataExtracted: true,
      lastExtractionDate: new Date(),
    }
  }
}

export default new ArtistSyncETL()
