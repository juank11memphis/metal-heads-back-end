import metalArchivesApi from '../../api/metal-archives.api'
import bandsDao from '../../../components/bands/bands.dao'

class GetNewAdditionsETL {
  yearParam
  monthParam

  async start() {
    // eslint-disable-next-line no-console
    console.log(`
      New additions ETL Started.
      Date: ${new Date()}
    `)

    let newAdditionsData = await this.extractNewAdditionsData()
    newAdditionsData = this.transformNewAdditionsData(newAdditionsData)
    await this.loadNewAdditionsData(newAdditionsData)

    // eslint-disable-next-line no-console
    console.log(`
      New additions ETL Finished.
      Date: ${new Date()}
    `)
    return newAdditionsData
  }

  async extractNewAdditionsData() {
    let dateParam = this.getDateParam()
    // eslint-disable-next-line no-console
    console.log(`extracting new additions data for: ${dateParam}`)
    const newAdditionsData = await metalArchivesApi.getNewAdditionsData(
      dateParam,
    )
    return newAdditionsData
  }

  getDateParam() {
    if (!this.yearParam && !this.monthParam) {
      this.initDateParams()
    }
    return this.formatDateParams()
  }

  initDateParams() {
    const now = new Date()
    this.yearParam = now.getFullYear()
    this.monthParam = now.getMonth() + 1
  }

  formatDateParams() {
    let monthParamStr = this.monthParam + ''
    if (this.monthParam < 10) {
      monthParamStr = `0${this.monthParam}`
    }
    return `${this.yearParam}-${monthParamStr}`
  }

  transformNewAdditionsData(data) {
    let newList = []
    data.forEach(bandDataArr => {
      newList.push(this.getBandObjectFromMetalArchivesData(bandDataArr))
    })
    return newList
  }

  getBandObjectFromMetalArchivesData(bandDataArr) {
    let metalArchivesId
    let name = bandDataArr[1]
    let country = bandDataArr[2]
    let genre = bandDataArr[3]
    metalArchivesId = name.substring(name.indexOf("href='"), name.indexOf('>'))
    metalArchivesId = metalArchivesId.substring(
      metalArchivesId.lastIndexOf('/') + 1,
      metalArchivesId.length - 1,
    )
    name = name.substring(name.indexOf('>') + 1, name.indexOf('</a>'))
    country = country.substring(
      country.indexOf('>') + 1,
      country.indexOf('</a>'),
    )

    return { name, country, genre, metalArchivesId }
  }

  async loadNewAdditionsData(bands) {
    const bandsToAdd = []
    for (const band of bands) {
      const databaseBand = await bandsDao.findOne({
        metalArchivesId: band.metalArchivesId,
      })
      if (databaseBand) {
        break
      } else {
        bandsToAdd.push(band)
      }
    }
    if (bandsToAdd.length === 0) {
      // eslint-disable-next-line no-console
      console.log('no new bands to add this time')
      return true
    } else {
      // eslint-disable-next-line no-console
      console.log(`inserting new bands in database: ${bandsToAdd.length}`)
      await bandsDao.inserMany(bandsToAdd)
      return true
    }
  }
}

export default new GetNewAdditionsETL()
