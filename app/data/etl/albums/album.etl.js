import metalArchivesAlbumWebScraper from '../../webscraping/metal-archives-album.webscraper'
import awsS3Api from '../../api/aws-s3.api'
import { sortBasedOnArray } from '../../../util/array.util'
import albumsDao from '../../../components/albums/albums.dao'
import artistsDao from '../../../components/artists/artists.dao'
import songsDao from '../../../components/songs/songs.dao'

export class AlbumETL {
  async start(dbAlbumId) {
    const databaseAlbum = await albumsDao.getFullDetail(dbAlbumId)
    this.log('Start', databaseAlbum)

    let albumData = await this.extractAlbumData(databaseAlbum)
    albumData = await this.transformAlbumData(databaseAlbum, albumData)
    await this.loadAlbumData(databaseAlbum, albumData)

    this.log('Finish', databaseAlbum)
    return await albumsDao.getDetail(databaseAlbum._id)
  }

  async extractAlbumData(databaseAlbum) {
    const albumData = await metalArchivesAlbumWebScraper.scrapAlbumFullData(
      databaseAlbum.metalArchivesId,
      databaseAlbum.name,
      databaseAlbum.band.name,
    )
    return albumData
  }

  async transformAlbumData(databaseAlbum, albumData) {
    let data = {}
    Object.assign(
      data,
      this.transformAlbumGeneralInfo(albumData),
      await this.transformAlbumLineUp(albumData),
      await this.transformAlbumPicture(databaseAlbum, albumData),
      this.transformAlbumSongs(albumData),
    )
    return data
  }

  transformAlbumGeneralInfo(albumData) {
    return {
      dataExtracted: true,
      releaseDate: albumData.releaseDate,
      catalogId: albumData.catalogId,
      label: albumData.label,
      format: albumData.format,
      reviews: albumData.reviews,
    }
  }

  async transformAlbumLineUp(albumData) {
    const lineUp = albumData.lineUp
    const lineUpTransformed = []
    for (const artist of lineUp) {
      let dbArtist = await artistsDao.findOne({
        metalArchivesId: artist.metalArchivesId,
      })
      if (!dbArtist) {
        dbArtist = await artistsDao.create({
          name: artist.name,
          metalArchivesId: artist.metalArchivesId,
        })
      }
      lineUpTransformed.push({
        name: artist.name,
        roles: artist.roles,
        metalArchivesId: artist.metalArchivesId,
        id: dbArtist._id,
      })
    }
    return { lineUp: lineUpTransformed }
  }

  async transformAlbumPicture(databaseAlbum, albumData) {
    const albumCover = albumData.picture
    const imagePath = awsS3Api.generateS3ImagePath(databaseAlbum, 'album')
    const picture = await awsS3Api.downloadAndUploadImage(albumCover, imagePath)
    return { picture }
  }

  transformAlbumSongs(albumData) {
    let songs = albumData.songs.filter(song => song.title && song.length)
    songs = songs.map(song => ({
      title: song.title.substring(3, song.title.length),
      length: song.length,
    }))
    return {
      tracks: songs,
    }
  }

  async loadAlbumData(dbAlbum, albumData) {
    const { tracks, ...albumDataRest } = albumData
    const dbTracks = await this.loadTracksData(dbAlbum, tracks)
    await albumsDao.update(dbAlbum, {
      ...albumDataRest,
      tracks: dbTracks,
    })
    return true
  }

  async loadTracksData(dbAlbum, sourceTracks) {
    const { tracks: dbTracks } = dbAlbum
    const { songsToAdd, songsToUpdate } = this.getSongsToAddAndUpdate(
      dbTracks,
      sourceTracks,
    )
    const { songsToDelete } = this.getSongsToDelete(dbTracks, sourceTracks)

    const [songsAdded, songsUpdated] = await Promise.all([
      songsDao.inserMany(songsToAdd),
      songsDao.updateMany(songsToUpdate),
      songsDao.removeMany(songsToDelete),
    ])

    const unorderedSongs = [...songsAdded, ...songsUpdated]
    const songsOrdered = sortBasedOnArray({
      unordered: unorderedSongs,
      ordered: sourceTracks,
      unorderedField: 'name',
      orderedField: 'title',
    })
    return songsOrdered
  }

  getSongsToAddAndUpdate(dbTracks, sourceTracks) {
    const songsToAdd = []
    const songsToUpdate = []
    sourceTracks.forEach(song => {
      const dbTrack = dbTracks.find(track => track.name === song.title)
      if (dbTrack) {
        songsToUpdate.push({ id: dbTrack._id, data: song })
      } else {
        songsToAdd.push({ name: song.title, length: song.length })
      }
    })
    return { songsToAdd, songsToUpdate }
  }

  getSongsToDelete(dbTracks, sourceTracks) {
    const songsToDelete = []
    dbTracks.forEach(dbTrack => {
      const songFound = sourceTracks.find(song => song.title === dbTrack.name)
      if (!songFound) {
        songsToDelete.push(dbTrack._id)
      }
    })
    return { songsToDelete }
  }

  log(text, databaseAlbum) {
    // eslint-disable-next-line no-console
    console.log(`
        Album ETL ${text}
        id: ${databaseAlbum._id}
        metalArchivesId: ${databaseAlbum.metalArchivesId}
        name: ${databaseAlbum.name}
      `)
  }
}

export default new AlbumETL()
