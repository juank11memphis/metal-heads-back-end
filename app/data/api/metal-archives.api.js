import Constants from '../../core/constants'
import request from 'request'
import doRequest from '../../util/request.util'

export class MetalArchivesApi {
  GET_LIST_PREFIX_URL = 'http://www.metal-archives.com/browse/ajax-letter/l/'
  GET_LIST_SUFFIX_URL =
    '/json/1?sEcho=1&iDisplayLength=500&iSortCol_0=0&sSortDir_0=asc&iSortingCols=1&iDisplayStart='

  GET_NEW_ADDITIONS_PREFIX_URL =
    'https://www.metal-archives.com/archives/ajax-band-list/by/created/selection/'
  GET_NEW_ADDITIONS_SUFFIX_URL =
    '?iDisplayLength=200&iSortCol_0=4&sSortDir_0=desc&iDisplayStart='

  GET_ALBUM_URL = 'http://em.wemakesites.net/album/'

  getListData(listItem, callback, start = 0, dataAcc = []) {
    const url = `${this.GET_LIST_PREFIX_URL}${listItem}${
      this.GET_LIST_SUFFIX_URL
    }${start}`
    request.get(url, (err, response, body) => {
      const data = JSON.parse(body).aaData
      if (data.length === 0) {
        callback(dataAcc)
        return
      }
      dataAcc = [...dataAcc, ...data]
      start += 500
      setTimeout(
        () => this.getListData(listItem, callback, start, dataAcc),
        500,
      )
    })
  }

  async getNewAdditionsData(dateParam, start = 0, dataAcc = []) {
    const url = `${this.GET_NEW_ADDITIONS_PREFIX_URL}${dateParam}${
      this.GET_NEW_ADDITIONS_SUFFIX_URL
    }${start}`
    let response = await doRequest(url)
    response = response.substring(response.indexOf('"aaData"'), response.length)
    response = '{' + response
    const data = JSON.parse(response).aaData
    if (data.length === 0) {
      return dataAcc
    }
    dataAcc = [...dataAcc, ...data]
    start += 200
    return await this.getNewAdditionsData(dateParam, start, dataAcc)
  }

  getAlbumData(metalArchivesId) {
    const promise = async (resolve, reject) => {
      try {
        const url = `${this.GET_ALBUM_URL}${metalArchivesId}?api_key=${
          Constants.metalArchivesApiKey
        }`
        const body = await doRequest(url)
        const bodyParsed = JSON.parse(body)
        resolve(bodyParsed.data.album)
      } catch (error) {
        reject(error)
      }
    }
    return new Promise(promise)
  }
}

export default new MetalArchivesApi()
