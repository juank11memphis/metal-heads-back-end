import elasticsearch from 'elasticsearch'
import Constants from '../../core/constants'

export class ElasticSearchApi {
  constructor() {
    this.client = new elasticsearch.Client({
      host: Constants.elasticsearch.url,
    })
  }

  createCurrentIndex() {
    return new Promise((resolve, reject) => {
      const indexName = Constants.elasticsearch.index
      this.client.indices.create({ index: indexName }, (err, response) => {
        resolve()
      })
    })
  }

  deleteCurrentIndex() {
    return new Promise((resolve, reject) => {
      const indexName = Constants.elasticsearch.index
      this.client.indices.delete({ index: indexName }, (err, response) => {
        resolve()
      })
    })
  }

  searchByEntityType(entityType, size = 10) {
    return new Promise((resolve, reject) => {
      const indexName = Constants.elasticsearch.index
      this.client.search(
        {
          index: indexName,
          q: `entityType:${entityType}`,
          size,
        },
        (err, response) => {
          if (err) {
            reject(err)
          } else {
            const { hits } = response
            resolve(hits.hits)
          }
        },
      )
    })
  }

  async searchByName(name) {
    const indexName = Constants.elasticsearch.index
    const result = await this.client.search({
      index: indexName,
      body: {
        query: {
          bool: {
            must: {
              multi_match: {
                type: 'most_fields',
                fields: ['name.folded', 'name'],
                query: name,
                operator: 'and',
              },
            },
            filter: {
              terms: {
                entityType: ['band', 'album', 'artist'],
              },
            },
          },
        },
      },
    })
    return result
  }

  deleteAllDocs() {
    return new Promise((resolve, reject) => {
      const indexName = Constants.elasticsearch.index
      this.client.deleteByQuery(
        {
          index: indexName,
          body: {
            query: {
              match_all: {},
            },
          },
        },
        (err, response) => {
          if (err) {
            reject(err)
          } else {
            resolve()
          }
        },
      )
    })
  }

  bulkBands(bands = [], action = 'index') {
    return new Promise((resolve, reject) => {
      if (bands.length === 0) {
        return resolve()
      }
      const indexName = Constants.elasticsearch.index
      const bulkBody = []
      for (const band of bands) {
        if (!band._id) {
          continue
        }
        bulkBody.push({
          [action]: {
            _index: indexName,
            _type: 'doc',
            _id: `band-${band._id}`,
          },
        })
        if (action === 'index') {
          bulkBody.push(this.getBandObject(band))
        } else if (action === 'update') {
          bulkBody.push({ doc: this.getBandObject(band) })
        }
      }
      this.client.bulk({ body: bulkBody, refresh: true }, (err, response) => {
        if (err) {
          reject(err)
        } else {
          resolve()
        }
      })
    })
  }

  getBandObject(band) {
    return {
      name: band.name,
      entityType: 'band',
      metadata: {
        id: band._id,
        status: band.status,
        genre: band.genre,
        country: band.country,
        picture: band.picture,
        lyricalThemes: band.lyricalThemes,
        location: band.location,
        formedIn: band.formedIn,
        globalRating: band.ratingsData
          ? band.ratingsData.globalRating
          : undefined,
      },
    }
  }

  bulkArtists(artists = [], action = 'index') {
    return new Promise((resolve, reject) => {
      if (artists.length === 0) {
        return resolve()
      }
      const indexName = Constants.elasticsearch.index
      const bulkBody = []
      for (const artist of artists) {
        if (!artist._id) {
          continue
        }
        bulkBody.push({
          [action]: {
            _index: indexName,
            _type: 'doc',
            _id: `artist-${artist._id}`,
          },
        })
        if (action === 'index') {
          bulkBody.push(this.getArtistObject(artist))
        } else if (action === 'update') {
          bulkBody.push({ doc: this.getArtistObject(artist) })
        }
      }
      this.client.bulk({ body: bulkBody, refresh: true }, (err, response) => {
        if (err) {
          reject(err)
        } else {
          resolve()
        }
      })
    })
  }

  getArtistObject(artist) {
    return {
      name: artist.name,
      entityType: 'artist',
      metadata: {
        id: artist._id,
        gender: artist.gender,
        picture: artist.picture,
      },
    }
  }

  bulkAlbums(albums = [], action = 'index') {
    return new Promise((resolve, reject) => {
      if (albums.length === 0) {
        return resolve()
      }
      const indexName = Constants.elasticsearch.index
      const bulkBody = []
      for (const album of albums) {
        if (!album._id) {
          continue
        }
        bulkBody.push({
          [action]: {
            _index: indexName,
            _type: 'doc',
            _id: `album-${album._id}`,
          },
        })
        if (action === 'index') {
          bulkBody.push(this.getAlbumObject(album))
        } else if (action === 'update') {
          bulkBody.push({ doc: this.getAlbumObject(album) })
        }
      }
      this.client.bulk({ body: bulkBody, refresh: true }, (err, response) => {
        if (err) {
          reject(err)
        } else {
          resolve()
        }
      })
    })
  }

  getAlbumObject(album) {
    return {
      name: album.name,
      entityType: 'album',
      metadata: {
        id: album._id,
        year: album.year,
        picture: album.picture,
        type: album.type,
        globalRating: album.ratingsData
          ? album.ratingsData.globalRating
          : undefined,
      },
    }
  }

  bulkSongs(songs = [], action = 'index') {
    return new Promise((resolve, reject) => {
      if (songs.length === 0) {
        return resolve()
      }
      const indexName = Constants.elasticsearch.index
      const bulkBody = []
      for (const song of songs) {
        if (!song._id) {
          continue
        }
        bulkBody.push({
          [action]: {
            _index: indexName,
            _type: 'doc',
            _id: `song-${song._id}`,
          },
        })
        if (action === 'index') {
          bulkBody.push(this.getSongObject(song))
        } else if (action === 'update') {
          bulkBody.push({ doc: this.getSongObject(song) })
        }
      }
      this.client.bulk({ body: bulkBody, refresh: true }, (err, response) => {
        if (err) {
          reject(err)
        } else {
          resolve()
        }
      })
    })
  }

  getSongObject(song) {
    return {
      name: song.name,
      entityType: 'song',
      metadata: {
        id: song._id,
        length: song.year,
      },
    }
  }

  getDocumentById(id) {
    return new Promise((resolve, reject) => {
      const indexName = Constants.elasticsearch.index
      this.client.get(
        {
          index: indexName,
          type: '_all',
          id,
        },
        (err, response) => {
          if (err) {
            reject(err)
          } else {
            resolve(response._source)
          }
        },
      )
    })
  }
}

export default new ElasticSearchApi()
