import { get } from 'lodash'

import Constants from '../../core/constants'
import doRequest from '../../util/request.util'
import * as StringUtil from '../../util/string.util'
import { generalLogger, errorsLogger } from '../../util/logs'

export class SpotifyApi {
  BASE_AUTH_API_URL = 'https://accounts.spotify.com/api'
  BASE_API_URL = 'https://api.spotify.com/v1'

  getTokenRequestOptions(params) {
    const {
      grantType,
      code = null,
      refreshToken = null,
      spotifyRedirectUrl = Constants.spotifyRedirectUrl,
    } = params
    const clientId = Constants.spotifyClientId
    const clientSecret = Constants.spotifyClientSecret
    const authBuffer = new Buffer(`${clientId}:${clientSecret}`).toString(
      'base64',
    )
    let optionsForm = { grant_type: grantType }
    if (code) {
      optionsForm = {
        ...optionsForm,
        redirect_uri: spotifyRedirectUrl,
        code,
      }
    }
    if (refreshToken) {
      optionsForm = {
        ...optionsForm,
        refresh_token: refreshToken,
      }
    }

    const options = {
      url: `${this.BASE_AUTH_API_URL}/token`,
      form: optionsForm,
      headers: {
        Authorization: `Basic ${authBuffer}`,
      },
      json: true,
    }
    return options
  }

  async getToken(code, spotifyRedirectUrl = undefined) {
    const requestOptions = this.getTokenRequestOptions({
      grantType: 'authorization_code',
      code,
      spotifyRedirectUrl,
    })
    const response = await doRequest(requestOptions, 'post')
    return {
      accessToken: response.access_token,
      refreshToken: response.refresh_token,
      expiresIn: response.expires_in,
    }
  }

  async refreshAccessToken(refreshToken) {
    const requestOptions = this.getTokenRequestOptions({
      grantType: 'refresh_token',
      refreshToken,
    })
    const response = await doRequest(requestOptions, 'post')
    return response.access_token
  }

  async getUserData(accessToken) {
    const options = {
      url: `${this.BASE_API_URL}/me`,
      headers: { Authorization: 'Bearer ' + accessToken },
      json: true,
    }
    generalLogger.info('SpotifyApi -> getUserData', options.url)
    const userData = await doRequest(options)
    return {
      country: userData.country,
      firstname: userData.id,
      email: userData.email,
    }
  }

  async validateAccessToken(accessToken) {
    try {
      await this.getUserData(accessToken)
      return true
    } catch (error) {
      return false
    }
  }

  async getPlayerDevices(accessToken) {
    const options = {
      url: `${this.BASE_API_URL}/me/player/devices`,
      headers: { Authorization: 'Bearer ' + accessToken },
      json: true,
    }
    generalLogger.info('SpotifyApi -> getPlayerDevices', options.url)
    const playerDevices = await doRequest(options)
    return playerDevices
  }

  async searchAlbum(accessToken, albumName, bandName) {
    const queryAlbumName = StringUtil.removeDiacritics(
      albumName
        .replace('&', '')
        .split(' ')
        .join('+'),
    )
    const queryBandName = StringUtil.removeDiacritics(
      bandName
        .replace('&', '')
        .split(' ')
        .join('+'),
    )
    const query = `album:${queryAlbumName}+artist:${queryBandName}`
    const options = {
      url: `${this.BASE_API_URL}/search?q=${query}&type=album`,
      headers: { Authorization: 'Bearer ' + accessToken },
      json: true,
    }
    generalLogger.info('SpotifyApi -> searchAlbum', options.url)
    const searchResult = await doRequest(options)
    const album = get(searchResult, 'albums.items.0')
    return album
  }

  async searchBand(accessToken, bandName) {
    const queryBandName = StringUtil.removeDiacritics(
      bandName.split(' ').join('+'),
    )
    const query = `artist:${queryBandName}`
    const options = {
      url: `${this.BASE_API_URL}/search?q=${query}&type=artist`,
      headers: { Authorization: 'Bearer ' + accessToken },
      json: true,
    }
    generalLogger.info('SpotifyApi -> searchBand', options.url)
    const searchResult = await doRequest(options)
    const items = get(searchResult, 'artists.items', [])
    return items
  }

  async playOnDevice(accessToken, deviceId, contextUri) {
    const options = {
      headers: { Authorization: 'Bearer ' + accessToken },
      url: `${this.BASE_API_URL}/me/player/play?device_id=${deviceId}`,
      body: {
        context_uri: contextUri,
      },
      json: true,
    }
    generalLogger.info('SpotifyApi -> playOnDevice', options.url, options.body)
    await doRequest(options, 'put')
    return true
  }

  async toggleShuffle({ accessToken, state = false, deviceId } = {}) {
    try {
      const options = {
        headers: { Authorization: 'Bearer ' + accessToken },
        url: `${
          this.BASE_API_URL
        }/me/player/shuffle?state=${state}&device_id=${deviceId}`,
        json: true,
      }
      generalLogger.info('SpotifyApi -> toggleShuffle', options.url)
      await doRequest(options, 'put')
    } catch (error) {
      errorsLogger.error('Error on: SpotifyApi -> toggleShuffle', error)
    }
  }
}

export default new SpotifyApi()
