import aws from 'aws-sdk'
import doRequest from '../../util/request.util'
import Constants from '../../core/constants'
import errorsLogger from '../../util/logs/errors.logger'

export class AWSS3Api {
  setup() {
    aws.config.update({
      accessKeyId: Constants.awsS3Key,
      secretAccessKey: Constants.awsS3Secret,
      region: Constants.awsS3Region,
    })
  }

  async downloadAndUploadImage(imageUrl, imagePath) {
    const reqOptions = this.getReqOptions(imageUrl)
    const imageBuffer = await doRequest(reqOptions)
    let s3ImageUrl
    if (!Constants.isProduction()) {
      s3ImageUrl = this.getImageUrl(imagePath)
      console.log(`success fake uploading ${imagePath} to s3`) // eslint-disable-line no-console
      return s3ImageUrl
    }
    s3ImageUrl = await this.uploadImage(imageBuffer, imagePath)
    return s3ImageUrl
  }

  uploadImage(imageBuffer, imagePath) {
    const promise = new Promise((resolve, reject) => {
      this.setup()
      const s3 = new aws.S3()
      s3.putObject(
        {
          Body: imageBuffer,
          Key: imagePath,
          Bucket: 'metal-pedia',
        },
        (error, data) => {
          if (error) {
            errorsLogger.error(`error uploading ${imagePath} to s3`, error)
            reject(error)
          } else {
            console.log(`success uploading ${imagePath} to s3`) // eslint-disable-line no-console
            resolve(this.getImageUrl(imagePath))
          }
        },
      )
    })
    return promise
  }

  getImageUrl(imagePath) {
    return `http://metalheads.imgix.net/${imagePath}`
  }

  getReqOptions(url) {
    return {
      uri: url,
      encoding: null,
    }
  }

  generateS3ImagePath(item, type) {
    if (type === 'band') {
      return `bands/${item._id}_picture.jpg`
    }
    if (type === 'album') {
      return `albums/${item._id}_picture.jpg`
    }
    return `artists/${item._id}_picture.jpg`
  }
}

export default new AWSS3Api()
