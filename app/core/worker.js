import Agenda from 'agenda'

import Constants from './constants'

const agenda = new Agenda({ db: { address: Constants.mongo.uri } })

const jobTypes = Constants.jobTypes

jobTypes.forEach(function(type) {
  require('../jobs/' + type + '.job').default(agenda)
})

agenda.on('ready', () => {
  if (jobTypes.length && !Constants.envs.test) {
    agenda.start()
  }
})

module.exports = agenda
