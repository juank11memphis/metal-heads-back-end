import express from 'express'
import cors from 'cors'
import bodyParser from 'body-parser'
import methodOverride from 'method-override'
import helmet from 'helmet'
import compress from 'compression'

import routes from './routes'
import Constants from './constants'

const app = express()

app.use(helmet())
app.use(cors())

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

app.use(compress())

app.use(methodOverride())

app.use(Constants.apiPrefix, routes)

export default app
