import path from 'path'
import merge from 'lodash/merge'

// Default configurations applied to all environments
const defaultConfig = {
  env: process.env.NODE_ENV,
  version: require('../../package.json').version,
  root: path.normalize(__dirname + '/../../..'),
  port: process.env.PORT || 4567,
  ip: process.env.IP || '0.0.0.0',

  apiPrefix: '/api',
  supportEmail: process.env.SUPPORT_EMAIL,
  logsDir: process.env.LOGS_DIR
    ? process.env.LOGS_DIR
    : '/tmp/metal-heads-logs.log',

  googleClientId: process.env.GOOGLE_CLIENT_ID || 'test_google_client_id',
  spotifyClientId: process.env.SPOTIFY_CLIENT_ID || 'test_spotify_client_id',
  spotifyClientSecret:
    process.env.SPOTIFY_CLIENT_SECRET || 'test_spotify_secret',
  spotifyRedirectUrl:
    process.env.SPOTIFY_REDIRECT_URL || 'test_spotify_redirect',
  spotifyConnectRedirectUrl:
    process.env.SPOTIFY_CONNECT_REDIRECT_URL || 'test_spotify_connect_redirect',
  spotifyScopes: process.env.SPOTIFY_SCOPES || 'test_spotify_scopes',

  mailgunApiKey: process.env.MAILGUN_API_KEY || 'test-mailgun-api-key',
  mailgunDomain: process.env.MAILGUN_DOMAIN || 'test-mailgun-domain',

  awsS3Key: process.env.S3_KEY || 'test-s3-key',
  awsS3Secret: process.env.S3_SECRET || 'test-s3-secret',
  awsS3Region: process.env.S3_REGION || 'test-s3-region',

  jobTypes: process.env.JOB_TYPES ? process.env.JOB_TYPES.split(',') : [],
  daysBeforeSyncBandData: process.env.DAYS_BEFORE_SYNC_BAND_DATA || 5,

  metalArchivesApiKey:
    process.env.METAL_ARCHIVES_API_KEY || 'test-metalarchives-key',

  mongo: {
    seed: true,
    options: {
      db: {
        safe: true,
      },
    },
  },

  elasticsearch: {
    url: process.env.ELASTIC_SEARCH_URL,
    index: process.env.ELASTIC_SEARCH_INDEX,
  },

  security: {
    sessionSecret: process.env.SESSION_SECRET || 'i-am-the-secret-key',
    sessionExpiration: process.env.SESSION_EXPIRATION || 60 * 60 * 24 * 7, // 1 week
    resetPasswordExpiration:
      process.env.RESET_PASSWORD_EXPIRATION || 60 * 60 * 24, // 1 day
    resetPasswordEmailLink:
      process.env.RESET_PASSWORD_EMAIL_LINK || 'test-link',
    saltRounds: process.env.SALT_ROUNDS || 12,
  },

  get envs() {
    return {
      test: process.env.NODE_ENV === 'test',
      development: process.env.NODE_ENV === 'development',
      production: process.env.NODE_ENV === 'production',
    }
  },

  isProduction: () => {
    return process.env.NODE_ENV === 'production'
  },
}

// Environment specific overrides
const environmentConfigs = {
  development: {
    mongo: {
      uri: process.env.MONGO_URI,
    },
    security: {
      saltRounds: 4,
    },
  },
  production: {
    mongo: {
      seed: false,
      uri: process.env.MONGO_URI,
    },
  },
  test: {
    port: 5678,
    mongo: {
      uri: 'mongodb://localhost:27017/test',
    },
    security: {
      saltRounds: 4,
      sessionSecret: 'the-session-secret',
    },
    googleClientId: 'test-google-client-id',
    elasticsearch: {
      index: 'metalheadstest',
      url: 'localhost:9200',
    },
  },
}

export default merge(
  defaultConfig,
  environmentConfigs[process.env.NODE_ENV] || {},
)
