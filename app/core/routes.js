import { Router } from 'express'
import cookieParser from 'cookie-parser'

import maintenanceRest from '../components/maintenance/maintenance.rest'
import authSpotifyRest from '../components/auth/auth-spotify.rest'
import {
  authenticate,
  authenticateQueryString,
} from './middleware/authenticate'
import { authorize } from './middleware/authorize'
import { restHandler } from './middleware/rest-handler'
import * as permissions from './permissions'

const routes = new Router()

// Authentication
routes.get(
  '/auth/signin/spotify',
  restHandler({
    func: authSpotifyRest.signinWithSpotify,
    funcName: 'signinWithSpotify',
  }),
)
routes.get(
  '/auth/signin/spotify/callback',
  cookieParser(),
  restHandler({
    func: authSpotifyRest.signinWithSpotifyCallback,
    funcName: 'signinWithSpotifyCallback',
  }),
)

// Users
routes.get(
  '/users/me/connect/spotify',
  authenticateQueryString,
  restHandler({
    func: authSpotifyRest.connectToSpotify,
    funcName: 'connectToSpotify',
  }),
)
routes.get(
  '/users/me/connect/spotify/callback',
  cookieParser(),
  restHandler({
    func: authSpotifyRest.connectToSpotifyCallback,
    funcName: 'connectToSpotifyCallback',
  }),
)

// Maintenance
routes.get(
  '/maintenance/elasticsearch/fill',
  authenticate,
  authorize(permissions.FILL_ELASTIC_SEARCH),
  restHandler({
    func: maintenanceRest.fillElasticSearchData,
    funcName: 'fillElasticSearchData',
  }),
)

export default routes
