import HttpStatus from 'http-status-codes'

import { errorsLogger } from '../../util/logs'

export const restHandler = ({ func, funcName = '' } = {}) => {
  return async (req, res, next) => {
    try {
      return await func(req, res, next)
    } catch (error) {
      errorsLogger.error(`Error on ${funcName}`, error)
      return res.sendStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    }
  }
}
