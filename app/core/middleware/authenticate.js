import HttpStatus from 'http-status-codes'

import { getTokenData } from '../../util/auth.util'

export const authenticate = async (req, res, next) => {
  let { authorization } = req.headers
  if (!authorization) {
    return res.sendStatus(HttpStatus.UNAUTHORIZED)
  }
  const tokenData = await getTokenData(authorization)
  if (!tokenData) {
    return res.sendStatus(HttpStatus.UNAUTHORIZED)
  }
  req.userId = tokenData.userId
  next()
}

export const authenticateQueryString = async (req, res, next) => {
  let { authorization } = req.query
  if (!authorization) {
    return res.sendStatus(HttpStatus.UNAUTHORIZED)
  }
  const tokenData = await getTokenData(authorization)
  if (!tokenData) {
    return res.sendStatus(HttpStatus.UNAUTHORIZED)
  }
  req.userId = tokenData.userId
  next()
}
