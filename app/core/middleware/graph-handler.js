import { has } from 'lodash'

import { errorAsGraphError } from '../../util/errors.util'
import { validateToken, validatePermissions } from '../../util/auth.util'
import { errorsLogger } from '../../util/logs'

const runHandler = async ({
  func,
  funcName = '',
  args = [],
  auth = {},
  sendUserId = true,
} = {}) => {
  const { token, permission } = auth
  let tokenData = {}
  if (has(auth, 'token')) {
    tokenData = await validateToken(token)
  }
  if (has(auth, 'token') && has(auth, 'permission')) {
    await validatePermissions(tokenData.userRoles, permission)
  }
  if (sendUserId && tokenData.userId) {
    return await func(tokenData.userId, ...args)
  }
  return await func(...args)
}

export const graphHandlerSilent = async handlerParams => {
  try {
    return await runHandler(handlerParams)
  } catch (error) {
    const { funcName } = handlerParams
    errorsLogger.error(`Silent error on ${funcName}`, error)
    return null
  }
}

export const graphHandler = async handlerParams => {
  try {
    return await runHandler(handlerParams)
  } catch (error) {
    const { funcName } = handlerParams
    errorsLogger.error(`Error on ${funcName}`, error)
    throw errorAsGraphError(error)
  }
}
