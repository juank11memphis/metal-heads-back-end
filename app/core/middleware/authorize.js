import HttpStatus from 'http-status-codes'

import { validatePermissionsNoException } from '../../util/auth.util'

export const authorize = permission => {
  return async (req, res, next) => {
    const { tokenData } = req
    const isAuthorized = validatePermissionsNoException(
      tokenData.userRoles,
      permission,
    )
    if (!isAuthorized) {
      return res.sendStatus(HttpStatus.UNAUTHORIZED)
    }
    next()
  }
}
