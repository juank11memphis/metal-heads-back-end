import app from './app'
import Constants from './constants'
import graphServer from '../graphql/graphql.server'

import './worker'

graphServer.applyMiddleware({ app })

const server = app.listen(Constants.port, () => {
  // eslint-disable-next-line no-console
  console.log(`
    Port: ${Constants.port}
    Env: ${app.get('env')}
  `)
})

export default server
