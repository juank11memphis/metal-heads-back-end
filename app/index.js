// Load environment variables
require('dotenv').config()

// Initialize Database
require('./core/database')

// Initialize Server
require('./core/server')
