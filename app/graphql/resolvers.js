import { newsQueriesResolvers } from '../components/news/news.graph'
import {
  quoteQueriesResolvers,
  quoteMutationsResolvers,
} from '../components/quotes/quotes.graph'
import {
  bandTypeResolver,
  bandQueriesResolvers,
} from '../components/bands/bands.graph'
import {
  artistTypeResolver,
  artistQueriesResolvers,
} from '../components/artists/artists.graph'
import {
  albumTypeResolver,
  albumQueriesResolvers,
} from '../components/albums/albums.graph'
import {
  userQueriesResolvers,
  userMutationsResolvers,
} from '../components/users/users.graph'
import {
  userBandTypeResolver,
  userBandsQueriesResolvers,
  userBandsMutationsResolvers,
} from '../components/user-bands/user-bands.graph'
import { authMutationsResolvers } from '../components/auth/auth.graph'
import {
  userAlbumTypeResolver,
  userAlbumsQueriesResolvers,
  userAlbumsMutationsResolvers,
} from '../components/user-albums/user-albums.graph'
import {
  spotifyQueriesResolvers,
  spotifyMutationsResolvers,
} from '../components/spotify/spotify.graph'
import { searchQueriesResolvers } from '../components/search/search.graph'
import { songTypeResolver } from '../components/songs/songs.graph'
import {
  activitiesMutationsResolvers,
  activityTypeResolver,
  activitiesQueriesResolvers,
} from '../components/activities/activities.graph'
import { configQueriesResolvers } from '../components/config/config.graph'

export default {
  Band: bandTypeResolver,
  Album: albumTypeResolver,
  Artist: artistTypeResolver,
  UserBand: userBandTypeResolver,
  UserAlbum: userAlbumTypeResolver,
  Song: songTypeResolver,
  Activity: activityTypeResolver,
  Query: {
    ...configQueriesResolvers,
    ...newsQueriesResolvers,
    ...bandQueriesResolvers,
    ...artistQueriesResolvers,
    ...albumQueriesResolvers,
    ...userQueriesResolvers,
    ...quoteQueriesResolvers,
    ...userBandsQueriesResolvers,
    ...userAlbumsQueriesResolvers,
    ...spotifyQueriesResolvers,
    ...searchQueriesResolvers,
    ...activitiesQueriesResolvers,
  },
  Mutation: {
    ...authMutationsResolvers,
    ...userMutationsResolvers,
    ...quoteMutationsResolvers,
    ...userBandsMutationsResolvers,
    ...userAlbumsMutationsResolvers,
    ...spotifyMutationsResolvers,
    ...activitiesMutationsResolvers,
  },
}
