import { spotifyQueryTypes } from '../../components/spotify/spotify.graph'
import { usersQueryTypes } from '../../components/users/users.graph'
import { bandsQueryTypes } from '../../components/bands/bands.graph'
import { configQueryTypes } from '../../components/config/config.graph'
import { userBandsQueryTypes } from '../../components/user-bands/user-bands.graph'
import { userAlbumsQueryTypes } from '../../components/user-albums/user-albums.graph'
import { newsQueryTypes } from '../../components/news/news.graph'
import { searchQueryTypes } from '../../components/search/search.graph'
import { artistsQueryTypes } from '../../components/artists/artists.graph'
import { albumsQueryTypes } from '../../components/albums/albums.graph'
import { quotesQueryTypes } from '../../components/quotes/quotes.graph'
import { activitiesQueryTypes } from '../../components/activities/activities.graph'

export default `
  type Query {
    ${newsQueryTypes}
    ${userBandsQueryTypes}
    ${userAlbumsQueryTypes}
    ${searchQueryTypes}
    ${artistsQueryTypes}
    ${albumsQueryTypes}
    ${configQueryTypes}
    ${bandsQueryTypes}
    ${usersQueryTypes}
    ${spotifyQueryTypes}
    ${quotesQueryTypes}
    ${activitiesQueryTypes}
  }
`
