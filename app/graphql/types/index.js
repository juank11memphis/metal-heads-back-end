import queryTypes from './queries'
import mutationsTypes from './mutations'
import { newsType } from '../../components/news/news.graph'
import { bandType } from '../../components/bands/bands.graph'
import { albumType } from '../../components/albums/albums.graph'
import { artistType } from '../../components/artists/artists.graph'
import { configType } from '../../components/config/config.graph'
import { userType } from '../../components/users/users.graph'
import { authType } from '../../components/auth/auth.graph'
import { quoteType } from '../../components/quotes/quotes.graph'
import { userBandType } from '../../components/user-bands/user-bands.graph'
import { userAlbumType } from '../../components/user-albums/user-albums.graph'
import { spotifyTypes } from '../../components/spotify/spotify.graph'
import { searchTypes } from '../../components/search/search.graph'
import { songType } from '../../components/songs/songs.graph'
import { activityType } from '../../components/activities/activities.graph'

export default `
  ${queryTypes}
  ${mutationsTypes}
  ${newsType}
  ${bandType}
  ${albumType}
  ${artistType}
  ${configType}
  ${userType}
  ${authType}
  ${quoteType}
  ${userBandType}
  ${userAlbumType}
  ${spotifyTypes}
  ${searchTypes}
  ${songType}
  ${activityType}
`
