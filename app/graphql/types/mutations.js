import { spotifyMutationTypes } from '../../components/spotify/spotify.graph'
import { activitiesMutationTypes } from '../../components/activities/activities.graph'
import { authMutationTypes } from '../../components/auth/auth.graph'
import { usersMutationTypes } from '../../components/users/users.graph'
import { userBandsMutationTypes } from '../../components/user-bands/user-bands.graph'
import { userAlbumsMutationTypes } from '../../components/user-albums/user-albums.graph'
import { quotesMutationTypes } from '../../components/quotes/quotes.graph'

export default `
  type Mutation {
    ${authMutationTypes}
    ${usersMutationTypes}
    ${userBandsMutationTypes}
    ${userAlbumsMutationTypes}
    ${quotesMutationTypes}
    ${spotifyMutationTypes}
    ${activitiesMutationTypes}
  }
`
