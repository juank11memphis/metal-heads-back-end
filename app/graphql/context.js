import newsService from '../components/news/news.service'
import bandsService from '../components/bands/bands.service'
import albumsService from '../components/albums/albums.service'
import artistsService from '../components/artists/artists.service'
import songsService from '../components/songs/songs.service'
import configService from '../components/config/config.service'
import usersService from '../components/users/users.service'
import userBandsService from '../components/user-bands/user-bands.service'
import userAlbumsService from '../components/user-albums/user-albums.service'
import authService from '../components/auth/auth.service'
import quotesService from '../components/quotes/quotes.service'
import spotifyService from '../components/spotify/spotify.service'
import searchService from '../components/search/search.service'
import activitiesService from '../components/activities/activities.service'

import { createDataLoader } from '../graphql/loader.factory'

export default function({ req, res }) {
  return {
    token: req.headers['authorization'],
    newsService,
    bandsService,
    albumsService,
    artistsService,
    configService,
    usersService,
    userBandsService,
    authService,
    quotesService,
    userAlbumsService,
    spotifyService,
    searchService,
    songsService,
    activitiesService,
    loaders: {
      albums: createDataLoader(albumsService),
      artists: createDataLoader(artistsService),
      bands: createDataLoader(bandsService),
      songs: createDataLoader(songsService),
      users: createDataLoader(usersService),
    },
  }
}
