import { maskErrors } from 'graphql-errors'
import { makeExecutableSchema } from 'graphql-tools'

import typeDefs from './types'
import resolvers from './resolvers'

const getSchema = () =>
  makeExecutableSchema({
    typeDefs,
    resolvers,
  })

const schema = getSchema()
maskErrors(schema)

export default schema
