import { UserError } from 'graphql-errors'

export const errorAsGraphError = error => {
  if (error.name === 'ValidationError') {
    const message = error.message.substring(
      error.message.lastIndexOf(':') + 2,
      error.message.length,
    )
    return new UserError(message)
  }
  if (error.name === 'AppError') {
    return new UserError(error.message)
  }
  return error
}
