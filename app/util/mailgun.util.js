import Mailgun from 'mailgun-js'
import fs from 'fs'
import path from 'path'
import MailComposer from 'nodemailer/lib/mail-composer'

import Constants from '../core/constants'
import errorsLogger from './logs/errors.logger'

const emailsData = {
  welcome: {
    getData: user => {
      let fullName = user.firstname
      if (user.lastname) {
        fullName = `${user.firstname} ${user.lastname}`
      }
      return {
        to: [user.email],
        recipientVars: {
          [user.email]: { name: fullName },
        },
        subject: 'Hey, %recipient.name%! Welcome to MetalHeads!',
        html: fs.readFileSync(
          path.join(__dirname, './email-templates/welcome-email.html'),
        ),
      }
    },
  },
  forgotPassword: {
    getData: (user, link) => {
      return {
        to: [user.email],
        recipientVars: {
          [user.email]: { link },
        },
        subject: 'MetalHeads! Password reset',
        html: fs.readFileSync(
          path.join(__dirname, './email-templates/reset-password-email.html'),
        ),
      }
    },
  },
}

export const sendWelcomeEmail = async user => {
  try {
    await composeEmail(emailsData.welcome.getData(user))
    return true
  } catch (error) {
    errorsLogger.error('Error on: MailgunUtil -> sendWelcomeEmail', error)
    return false
  }
}

export const sendResetPasswordEmail = async (user, link) => {
  try {
    await composeEmail(emailsData.forgotPassword.getData(user, link))
    return true
  } catch (error) {
    errorsLogger.error('Error on: MailgunUtil -> sendResetPasswordEmail', error)
    return false
  }
}

const composeEmail = emailData => {
  const promise = async (resolve, reject) => {
    try {
      const mail = getNewMailComposerInstance(emailData)
      mail.compile().build(async (mailBuildError, message) => {
        if (mailBuildError) {
          return reject(mailBuildError)
        }
        const dataToSend = {
          ['to']: emailData.to,
          ['message']: message.toString('ascii'),
          ['recipient-variables']: emailData.recipientVars,
        }
        sendEmail(dataToSend)
          .then(() => resolve())
          .catch(error => reject(error))
      })
    } catch (error) {
      reject(error)
    }
  }
  return new Promise(promise)
}

const sendEmail = dataToSend => {
  const promise = async (resolve, reject) => {
    try {
      console.log('Sending mail with Mailgun', dataToSend.to) // eslint-disable-line no-console
      const mailgun = getNewMailgunInstance()
      mailgun.messages().sendMime(dataToSend, function(sendError, body) {
        if (sendError) {
          reject(sendError)
        } else {
          console.log('Email sent ! ', body) // eslint-disable-line no-console
          resolve()
        }
      })
    } catch (error) {
      reject(error)
    }
  }
  return new Promise(promise)
}

const getNewMailgunInstance = () => {
  // eslint-disable-next-line no-console
  console.log(
    'Creating Mailgun Instance!',
    Constants.mailgunDomain,
    Constants.mailgunApiKey,
  )
  const mailgun = new Mailgun({
    apiKey: Constants.mailgunApiKey,
    domain: Constants.mailgunDomain,
  })
  return mailgun
}

const getNewMailComposerInstance = emailData => {
  return new MailComposer({
    from: Constants.supportEmail,
    to: emailData.to,
    subject: emailData.subject,
    html: emailData.html,
    headers: {
      'X-Mailgun-Recipient-Variables': JSON.stringify(emailData.recipientVars),
    },
  })
}
