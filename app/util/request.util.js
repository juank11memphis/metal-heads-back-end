import request from 'request'

export default function doRequest(optionsOrUrl, method = 'get') {
  const promise = (resolve, reject) => {
    request[method](optionsOrUrl, (err, res, body) => {
      if (!err && res && (res.statusCode == 200 || res.statusCode == 204)) {
        resolve(body)
      } else if (res && res.statusCode === 404) {
        reject(new Error('Resource not found'))
      } else {
        const url = optionsOrUrl.url || optionsOrUrl
        reject(err || new Error(`Unknown error on request: ${url}`))
      }
    })
  }
  return new Promise(promise)
}
