import logger from './logger.util'

const dataLogger = logger.child({ type: 'data' })

export default dataLogger
