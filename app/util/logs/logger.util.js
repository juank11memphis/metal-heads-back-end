import bunyan from 'bunyan'
import consoleStream from 'bunyan-console-stream'

import Constants from '../../core/constants'

const logsDir = Constants.logsDir
const streamOptions = {
  stderrThreshold: 40, // log warning, error and fatal messages on STDERR
}

const logger = bunyan.createLogger({
  name: 'metal-heads',
  streams: [
    {
      path: logsDir,
    },
    {
      type: 'raw',
      stream: consoleStream.createStream(streamOptions),
    },
  ],
})

export default logger
