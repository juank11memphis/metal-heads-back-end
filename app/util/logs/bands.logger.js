import generalLogger from './general.logger'
import errorsLogger from './errors.logger'

export const logBandEvent = (eventName, databaseBand) => {
  generalLogger.info(eventName, {
    bandId: databaseBand._id,
    bandName: databaseBand.name,
    bandMetalArchivesId: databaseBand.metalArchivesId,
  })
}

export const logBandWebscrapingStart = (
  bandName,
  bandUrl,
  bandDiscographyUrl,
) => {
  generalLogger.info('Webscraping band data', {
    bandName,
    bandUrl,
    bandDiscographyUrl,
  })
}

export const logBandError = (errorDesciption, databaseBand) => {
  errorsLogger.info(errorDesciption, {
    bandId: databaseBand._id,
    bandName: databaseBand.name,
    bandMetalArchivesId: databaseBand.metalArchivesId,
  })
}
