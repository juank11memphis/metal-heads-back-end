import logger from './logger.util'

const errorsLogger = logger.child({ type: 'errors' })

export default errorsLogger
