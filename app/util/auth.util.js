import jwt from 'jsonwebtoken'
import bcrypt from 'bcryptjs'

import Constants from '../core/constants'
import AppError from '../core/app.error'

export const generateToken = user => {
  return jwt.sign(
    { id: user._id, roles: user.roles },
    Constants.security.sessionSecret,
    {
      expiresIn: Constants.security.sessionExpiration,
    },
  )
}

export const generateResetPasswordToken = user => {
  return jwt.sign({ id: user._id }, Constants.security.sessionSecret, {
    expiresIn: Constants.security.resetPasswordExpiration,
  })
}

export const validateToken = async authorization => {
  if (!authorization) {
    throw new AppError('Invalid token')
  }
  const [jwtType, token] = authorization.split(' ')
  if (jwtType.toLowerCase() !== 'bearer' || !token) {
    throw new AppError('Invalid token')
  }
  let decoded
  try {
    decoded = await jwt.verify(token, Constants.security.sessionSecret)
  } catch (error) {
    throw new AppError('Invalid token')
  }
  return { userId: decoded.id, userRoles: decoded.roles }
}

export const getTokenData = async token => {
  let tokenData
  try {
    tokenData = await validateToken(token)
  } catch (error) {
    tokenData = null
  }
  return tokenData
}

export const validatePermissions = (roles, permissionToFind) => {
  if (!roles || roles.length === 0) {
    throw new AppError('Not enough permissions')
  }
  let permissionFound = false
  roles.forEach(({ permissions }) => {
    if (!permissions) {
      return
    }
    permissions.forEach(permission => {
      if (permission === permissionToFind) {
        permissionFound = true
      }
    })
  })
  if (!permissionFound) {
    throw new AppError('Not enough permissions')
  }
  return true
}

export const validatePermissionsNoException = (roles, permissionToFind) => {
  try {
    return validatePermissions(roles, permissionToFind)
  } catch (error) {
    return false
  }
}

export const comparePasswords = (password, encryptedPassword) => {
  return bcrypt.compareSync(password, encryptedPassword)
}

export const hashPassword = password => {
  const { saltRounds } = Constants.security
  return bcrypt.hashSync(password, saltRounds)
}
