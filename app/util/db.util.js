export const updateMany = async (modelClass, updates = []) => {
  if (updates.length === 0) {
    return []
  }
  const bulkUpdates = []
  const ids = updates.map(update => update.id)
  updates.forEach(({ id, data }) => {
    bulkUpdates.push({
      updateOne: {
        filter: { _id: id },
        update: { $set: { ...data } },
      },
    })
  })
  await modelClass.collection.bulkWrite(bulkUpdates, { ordered: true, w: 1 })
  return modelClass.find({ _id: { $in: ids } })
}

export const removeMany = (modelClass, deletes = []) => {
  if (deletes.length === 0) {
    return []
  }
  return modelClass.remove({ _id: { $in: deletes } })
}
