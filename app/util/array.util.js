export const sortBasedOnArray = ({
  unordered = [],
  ordered = [],
  unorderedField = 'id',
  orderedField = 'id',
} = {}) => {
  const result = []
  ordered.forEach(orderedItem => {
    let unorderedItemFound = unordered.find(
      unorderedItem =>
        unorderedItem[unorderedField] === orderedItem[orderedField],
    )
    if (unorderedItemFound) {
      result.push(unorderedItemFound)
    }
  })
  return result
}
