import moment from 'moment'
import { get } from 'lodash'

import Constants from '../core/constants'

export const isImageAlreadyInS3 = imageUrl => {
  if (imageUrl === undefined || imageUrl === null) {
    return false
  }
  return imageUrl.includes('metalheads.imgix.net')
}

const getBasePictureUrl = modelInstance => {
  const { metalArchivesId } = modelInstance
  const baseUrl = 'https://www.metal-archives.com/images/'
  let prefix = metalArchivesId.substring(0, 4)
  prefix = prefix.split('').join('/')
  return `${baseUrl}${prefix}`
}

const addPictureUrlSuffix = pictureUrl => {
  let newPictureUrl = pictureUrl
  if (newPictureUrl.indexOf('https') < 0) {
    newPictureUrl = pictureUrl.replace('http', 'https')
  }
  return `${newPictureUrl}?auto=compress,enhance,redeye`
}

export function generateBandPictureUrl(band) {
  const { metalArchivesId } = band
  if (!metalArchivesId) {
    return ''
  }
  const basePictureUrl = getBasePictureUrl(band)
  return `${basePictureUrl}/${metalArchivesId}_photo.jpg`
}

export function getBandPictureUrl(band) {
  const { metalArchivesId, picture } = band
  let pictureUrl = picture
  if (!pictureUrl && !metalArchivesId) {
    return ''
  }
  if (!pictureUrl) {
    pictureUrl = generateBandPictureUrl(band)
  }
  return addPictureUrlSuffix(pictureUrl)
}

export function generateAlbumPictureUrl(album) {
  const { metalArchivesId } = album
  if (!metalArchivesId) {
    return ''
  }
  const basePictureUrl = getBasePictureUrl(album)
  return `${basePictureUrl}/${metalArchivesId}.jpg`
}

export function getAlbumPictureUrl(album) {
  const { metalArchivesId, picture } = album
  let pictureUrl = picture
  if (!pictureUrl && !metalArchivesId) {
    return ''
  }
  if (!pictureUrl) {
    pictureUrl = generateAlbumPictureUrl(album)
  }
  return addPictureUrlSuffix(pictureUrl)
}

export function generateArtistPictureUrl(artist) {
  const { metalArchivesId } = artist
  if (!metalArchivesId) {
    return ''
  }
  const basePictureUrl = getBasePictureUrl(artist)
  return `${basePictureUrl}/${metalArchivesId}_artist.jpg`
}

export function getArtistPictureUrl(artist) {
  const { metalArchivesId, picture } = artist
  let pictureUrl = picture
  if (!pictureUrl && !metalArchivesId) {
    return ''
  }
  if (!pictureUrl) {
    pictureUrl = generateArtistPictureUrl(artist)
  }
  return addPictureUrlSuffix(pictureUrl)
}

export function needsSync(item) {
  if (!item.dataExtracted) {
    return false
  }
  if (item.dataExtracted && !item.lastExtractionDate) {
    return true
  }
  const now = moment()
  const lastExtractionDate = moment(item.lastExtractionDate)
  const diff = now.diff(lastExtractionDate, 'days')
  return diff > Constants.daysBeforeSyncBandData
}

export const recalculateRatingsData = (
  item,
  currentUserRating,
  newUserRating,
) => {
  const isNewRating = !currentUserRating ? true : false
  const finalCurrentRating = isNewRating ? 0 : currentUserRating

  const { ratingsCount, ratingsSum } = get(item, 'ratingsData', {
    globalRating: 0,
    ratingsCount: 0,
    ratingsSum: 0,
  })
  const newRatingsSum = ratingsSum - finalCurrentRating + newUserRating
  const newRatingsCount = isNewRating ? ratingsCount + 1 : ratingsCount
  return {
    ratingsSum: newRatingsSum,
    ratingsCount: newRatingsCount,
    globalRating: newRatingsSum / newRatingsCount,
  }
}

export const getValidRatingValue = rating => {
  // we dont take ratings lower than 0
  if (rating < 0) {
    return 0
  }
  // we dont take ratings greater than 10
  if (rating > 10) {
    return 10
  }
  return rating
}

export const recalculateRatingsDataOnRemoveRating = (item, currentRating) => {
  const { ratingsCount, ratingsSum } = get(item, 'ratingsData')
  const newRatingsSum = ratingsSum - currentRating
  const newRatingsCount = ratingsCount - 1
  return {
    ratingsSum: newRatingsSum,
    ratingsCount: newRatingsCount,
    globalRating: newRatingsSum / newRatingsCount,
  }
}
