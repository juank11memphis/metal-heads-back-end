# ----------------------------
# DOCKER COMMANDS
# ----------------------------

docker-compose-down:
	docker stop $(shell docker ps -a -q)
	docker rm mongodb
	docker rm mongo-seed
	docker rm elasticsearch
	docker rm es-seed
	docker volume prune -f
	docker-compose down --remove-orphans

docker-compose-up:
	docker-compose up --build -d

# ----------------------------
# RUNNING THE APP LOCALLY
# ----------------------------

local: docker-compose-down docker-compose-up
	yarn
	yarn start

# ----------------------------
# RUNNING TESTS
# ----------------------------

test: docker-compose-down docker-compose-up
	yarn
	yarn test

test-watch: docker-compose-down docker-compose-up
	yarn
	yarn test:watch

# ----------------------------
# PRODUCTION BUILD
# ----------------------------

prod:
	yarn
	yarn build

# ----------------------------
# MAKE TARGETS
# ----------------------------

.PHONY: docker-compose-up docker-compose-down
